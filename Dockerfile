#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:5.0-buster-slim AS base
RUN apt-get update && apt-get install -y libgdiplus
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:5.0-buster-slim AS build
WORKDIR /src
COPY ["mirleCloudCore/mirleCloudCore.csproj", "mirleCloudCore/"]
COPY ["dalmirleCloudCore/dalmirleCloudCore.csproj", "dalmirleCloudCore/"]
COPY ["bllmirleCloudCore/bllmirleCloudCore.csproj", "bllmirleCloudCore/"]
RUN dotnet restore "mirleCloudCore/mirleCloudCore.csproj"
COPY . .
WORKDIR "/src/mirleCloudCore"
RUN dotnet build "mirleCloudCore.csproj"  -c Release -o /app/build 

FROM build AS publish
RUN dotnet publish "mirleCloudCore.csproj" -c Release -o /app/publish 

FROM base AS final
RUN sed -i 's/MinProtocol = TLSv1.2/MinProtocol = TLSv1/g' /etc/ssl/openssl.cnf
RUN sed -i 's/MinProtocol = TLSv1.2/MinProtocol = TLSv1/g' /usr/lib/ssl/openssl.cnf
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "mirleCloudCore.dll"]
