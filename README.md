# CloudDisk 雲端硬碟
 #### 這是一個 使用dotnet core實作的 web base的檔案管理系統
<img src="https://upload.cc/i1/2022/02/22/bRKCYV.png" alt="test image size" height="50%" width="50%">

**** 開發環境 .net5 + sql server 2019 ****

### 步驟一
<pre>
docker-compose build && \
docker-compose up -d
</pre>

###### 因資料庫需要時間初始化,執行指令後請等待約30秒~1分鐘

### 步驟二
進入登入畫面
http://127.0.0.1

管理者帳號:lin/lin <br/>
選擇管理者身份登入

<img src="https://upload.cc/i1/2022/02/21/khQRUd.png" alt="test image size" height="50%" width="50%">

### 步驟三 (紫色為管理者模式)

<img src="https://upload.cc/i1/2022/02/21/lftAF7.png" alt="test image size" height="75%" width="150%">
 
###### 功能說明
<img src="https://upload.cc/i1/2022/02/21/JPgnst.png" alt="test image size" height="75%" width="150%">

###### 設定權限(share 給 chi 這位user) 帳密:chi/chi
<img src="https://upload.cc/i1/2022/02/21/pQo7at.png" alt="test image size" height="75%" width="150%">

### 步驟四
一般使用者登入

<img src="https://upload.cc/i1/2022/02/21/ChI6B7.png" alt="test image size" height="75%" width="150%">


### 步驟五
###### 功能說明  (藍色為使用者模式)

<img src="https://upload.cc/i1/2022/02/21/sdMW0C.png" alt="test image size" height="75%" width="150%">

###### 上傳

<img src="https://upload.cc/i1/2022/02/21/PQFs6b.png" alt="test image size" height="75%" width="150%">

###### 上傳後
<img src="https://upload.cc/i1/2022/02/21/9GFwWX.png" alt="test image size" height="75%" width="150%">

###### 搬動檔案
<img src="https://upload.cc/i1/2022/02/21/Xbiemp.png" alt="test image size" height="75%" width="150%">


### DB Schema 說明
###### ERD
<img src="https://upload.cc/i1/2022/02/21/NpZMou.png" alt="test image size" height="75%" width="150%">
一個User可以創建多個資料夾(AccountDocument)
<br/>
一個資料夾可以分享多個User（DocumentAccount）

##### Table 欄位說明請參照 <a href="https://gitlab.com/eggeggss/CloudWeb/-/tree/main/document" target="_blank"> document/table-desc.pdf </a>

##### 備註 ######
1.excel / word 原本的架構是靠另一台主機的devexpress套件,但此版沒有,未來有時間會整合這部分..
<br/>
2.如果要直接在vscode 或 visual studio 上面run ... appsetting的Root 請設定www/share 在你專案下載的地方的絕對路徑 ex: c:/cloudweb/app/wwww/share
