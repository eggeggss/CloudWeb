USE [master]
GO
USE [master]
GO
/****** Object:  Database [hangfiredb]    Script Date: 2022/2/21 下午3:01:55 ******/
CREATE DATABASE [hangfiredb]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'hangfiredb', FILENAME = N'/var/opt/mssql/data/hangfiredb.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'hangfiredb_log', FILENAME = N'/var/opt/mssql/data/hangfiredb_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [hangfiredb].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [hangfiredb] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [hangfiredb] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [hangfiredb] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [hangfiredb] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [hangfiredb] SET ARITHABORT OFF 
GO
ALTER DATABASE [hangfiredb] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [hangfiredb] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [hangfiredb] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [hangfiredb] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [hangfiredb] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [hangfiredb] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [hangfiredb] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [hangfiredb] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [hangfiredb] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [hangfiredb] SET  DISABLE_BROKER 
GO
ALTER DATABASE [hangfiredb] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [hangfiredb] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [hangfiredb] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [hangfiredb] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [hangfiredb] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [hangfiredb] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [hangfiredb] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [hangfiredb] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [hangfiredb] SET  MULTI_USER 
GO
ALTER DATABASE [hangfiredb] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [hangfiredb] SET DB_CHAINING OFF 
GO
ALTER DATABASE [hangfiredb] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [hangfiredb] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [hangfiredb] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [hangfiredb] SET QUERY_STORE = OFF
GO
USE [hangfiredb]
GO
/****** Object:  Schema [HangFire]    Script Date: 2022/2/21 下午3:01:55 ******/
CREATE SCHEMA [HangFire]
GO
/****** Object:  Table [HangFire].[AggregatedCounter]    Script Date: 2022/2/21 下午3:01:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[AggregatedCounter](
	[Key] [nvarchar](100) NOT NULL,
	[Value] [bigint] NOT NULL,
	[ExpireAt] [datetime] NULL,
 CONSTRAINT [PK_HangFire_CounterAggregated] PRIMARY KEY CLUSTERED 
(
	[Key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[Counter]    Script Date: 2022/2/21 下午3:01:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[Counter](
	[Key] [nvarchar](100) NOT NULL,
	[Value] [int] NOT NULL,
	[ExpireAt] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [CX_HangFire_Counter]    Script Date: 2022/2/21 下午3:01:55 ******/
CREATE CLUSTERED INDEX [CX_HangFire_Counter] ON [HangFire].[Counter]
(
	[Key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[Hash]    Script Date: 2022/2/21 下午3:01:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[Hash](
	[Key] [nvarchar](100) NOT NULL,
	[Field] [nvarchar](100) NOT NULL,
	[Value] [nvarchar](max) NULL,
	[ExpireAt] [datetime2](7) NULL,
 CONSTRAINT [PK_HangFire_Hash] PRIMARY KEY CLUSTERED 
(
	[Key] ASC,
	[Field] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[Job]    Script Date: 2022/2/21 下午3:01:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[Job](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[StateId] [bigint] NULL,
	[StateName] [nvarchar](20) NULL,
	[InvocationData] [nvarchar](max) NOT NULL,
	[Arguments] [nvarchar](max) NOT NULL,
	[CreatedAt] [datetime] NOT NULL,
	[ExpireAt] [datetime] NULL,
 CONSTRAINT [PK_HangFire_Job] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[JobParameter]    Script Date: 2022/2/21 下午3:01:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[JobParameter](
	[JobId] [bigint] NOT NULL,
	[Name] [nvarchar](40) NOT NULL,
	[Value] [nvarchar](max) NULL,
 CONSTRAINT [PK_HangFire_JobParameter] PRIMARY KEY CLUSTERED 
(
	[JobId] ASC,
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[JobQueue]    Script Date: 2022/2/21 下午3:01:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[JobQueue](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[JobId] [bigint] NOT NULL,
	[Queue] [nvarchar](50) NOT NULL,
	[FetchedAt] [datetime] NULL,
 CONSTRAINT [PK_HangFire_JobQueue] PRIMARY KEY CLUSTERED 
(
	[Queue] ASC,
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[List]    Script Date: 2022/2/21 下午3:01:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[List](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](100) NOT NULL,
	[Value] [nvarchar](max) NULL,
	[ExpireAt] [datetime] NULL,
 CONSTRAINT [PK_HangFire_List] PRIMARY KEY CLUSTERED 
(
	[Key] ASC,
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[Schema]    Script Date: 2022/2/21 下午3:01:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[Schema](
	[Version] [int] NOT NULL,
 CONSTRAINT [PK_HangFire_Schema] PRIMARY KEY CLUSTERED 
(
	[Version] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[Server]    Script Date: 2022/2/21 下午3:01:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[Server](
	[Id] [nvarchar](200) NOT NULL,
	[Data] [nvarchar](max) NULL,
	[LastHeartbeat] [datetime] NOT NULL,
 CONSTRAINT [PK_HangFire_Server] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[Set]    Script Date: 2022/2/21 下午3:01:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[Set](
	[Key] [nvarchar](100) NOT NULL,
	[Score] [float] NOT NULL,
	[Value] [nvarchar](256) NOT NULL,
	[ExpireAt] [datetime] NULL,
 CONSTRAINT [PK_HangFire_Set] PRIMARY KEY CLUSTERED 
(
	[Key] ASC,
	[Value] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[State]    Script Date: 2022/2/21 下午3:01:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[State](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[JobId] [bigint] NOT NULL,
	[Name] [nvarchar](20) NOT NULL,
	[Reason] [nvarchar](100) NULL,
	[CreatedAt] [datetime] NOT NULL,
	[Data] [nvarchar](max) NULL,
 CONSTRAINT [PK_HangFire_State] PRIMARY KEY CLUSTERED 
(
	[JobId] ASC,
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Index [IX_HangFire_AggregatedCounter_ExpireAt]    Script Date: 2022/2/21 下午3:01:56 ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_AggregatedCounter_ExpireAt] ON [HangFire].[AggregatedCounter]
(
	[ExpireAt] ASC
)
WHERE ([ExpireAt] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_HangFire_Hash_ExpireAt]    Script Date: 2022/2/21 下午3:01:56 ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_Hash_ExpireAt] ON [HangFire].[Hash]
(
	[ExpireAt] ASC
)
WHERE ([ExpireAt] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_HangFire_Job_ExpireAt]    Script Date: 2022/2/21 下午3:01:56 ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_Job_ExpireAt] ON [HangFire].[Job]
(
	[ExpireAt] ASC
)
INCLUDE ( 	[StateName]) 
WHERE ([ExpireAt] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_HangFire_Job_StateName]    Script Date: 2022/2/21 下午3:01:56 ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_Job_StateName] ON [HangFire].[Job]
(
	[StateName] ASC
)
WHERE ([StateName] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_HangFire_List_ExpireAt]    Script Date: 2022/2/21 下午3:01:56 ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_List_ExpireAt] ON [HangFire].[List]
(
	[ExpireAt] ASC
)
WHERE ([ExpireAt] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_HangFire_Server_LastHeartbeat]    Script Date: 2022/2/21 下午3:01:56 ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_Server_LastHeartbeat] ON [HangFire].[Server]
(
	[LastHeartbeat] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_HangFire_Set_ExpireAt]    Script Date: 2022/2/21 下午3:01:56 ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_Set_ExpireAt] ON [HangFire].[Set]
(
	[ExpireAt] ASC
)
WHERE ([ExpireAt] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_HangFire_Set_Score]    Script Date: 2022/2/21 下午3:01:56 ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_Set_Score] ON [HangFire].[Set]
(
	[Key] ASC,
	[Score] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [HangFire].[JobParameter]  WITH CHECK ADD  CONSTRAINT [FK_HangFire_JobParameter_Job] FOREIGN KEY([JobId])
REFERENCES [HangFire].[Job] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [HangFire].[JobParameter] CHECK CONSTRAINT [FK_HangFire_JobParameter_Job]
GO
ALTER TABLE [HangFire].[State]  WITH CHECK ADD  CONSTRAINT [FK_HangFire_State_Job] FOREIGN KEY([JobId])
REFERENCES [HangFire].[Job] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [HangFire].[State] CHECK CONSTRAINT [FK_HangFire_State_Job]
GO
USE [master]
GO
ALTER DATABASE [hangfiredb] SET  READ_WRITE 
GO





/****** Object:  Database [clouddb]    Script Date: 2022/2/20 下午10:10:06 ******/
CREATE DATABASE [clouddb]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'clouddb', FILENAME = N'/var/opt/mssql/data/clouddb.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'clouddb_log', FILENAME = N'/var/opt/mssql/data/clouddb_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [clouddb].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [clouddb] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [clouddb] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [clouddb] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [clouddb] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [clouddb] SET ARITHABORT OFF 
GO
ALTER DATABASE [clouddb] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [clouddb] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [clouddb] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [clouddb] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [clouddb] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [clouddb] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [clouddb] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [clouddb] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [clouddb] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [clouddb] SET  DISABLE_BROKER 
GO
ALTER DATABASE [clouddb] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [clouddb] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [clouddb] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [clouddb] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [clouddb] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [clouddb] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [clouddb] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [clouddb] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [clouddb] SET  MULTI_USER 
GO
ALTER DATABASE [clouddb] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [clouddb] SET DB_CHAINING OFF 
GO
ALTER DATABASE [clouddb] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [clouddb] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [clouddb] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'clouddb', N'ON'
GO
ALTER DATABASE [clouddb] SET QUERY_STORE = OFF
GO
USE [clouddb]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_get_auth]    Script Date: 2022/2/20 下午10:10:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[UserInfo]    Script Date: 2022/2/20 下午10:10:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON

GO
CREATE TABLE [dbo].[UserInfo](
	[emp_no] [varchar](30) NULL,
	[emp_ename] [varchar](30) NULL,
	[emp_cname] [nvarchar](100) NULL,
	[email] [varchar](1024) NULL,
	[dept_no] [varchar](100) NULL
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[vw_AppEmp]    Script Date: 2022/2/20 下午10:10:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


GO
/****** Object:  Table [dbo].[Dept]    Script Date: 2022/2/20 下午10:10:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Dept](
	[dept_no] [varchar](100) NULL,
	[dept_name] [nvarchar](max) NULL,
	[key] [nvarchar](max) NULL,
	[parent_dept_no] [varchar](100) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  View [dbo].[vw_dept]    Script Date: 2022/2/20 下午10:10:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/****** Object:  Table [dbo].[AccountDocument]    Script Date: 2022/2/20 下午10:10:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountDocument](
	[id_AccountDocument] [bigint] IDENTITY(-999999999,1) NOT NULL,
	[empname] [varchar](100) NULL,
	[id_Document] [bigint] NULL,
	[dt_create] [datetime] NULL,
	[dt_update] [datetime] NULL,
	[update_by] [varchar](100) NULL,
	[create_by] [varchar](100) NULL,
	[stat_void] [int] NULL,
 CONSTRAINT [PK_AccountDocument] PRIMARY KEY CLUSTERED 
(
	[id_AccountDocument] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[dept_level]    Script Date: 2022/2/20 下午10:10:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dept_level](
	[id_dept_level] [bigint] IDENTITY(-999999999,1) NOT NULL,
	[level_string] [nvarchar](max) NULL,
	[dept_no] [varchar](100) NULL,
	[dt_create] [datetime] NULL,
	[stat_void] [int] NULL,
 CONSTRAINT [PK_dept_level] PRIMARY KEY CLUSTERED 
(
	[id_dept_level] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Document]    Script Date: 2022/2/20 下午10:10:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Document](
	[id_Document] [bigint] IDENTITY(-999999999,1) NOT NULL,
	[relativepath] [nvarchar](max) NULL,
	[absolutepath] [nvarchar](max) NULL,
	[docname] [nvarchar](max) NULL,
	[dt_create] [datetime] NULL,
	[dt_update] [datetime] NULL,
	[update_by] [varchar](100) NULL,
	[create_by] [varchar](100) NULL,
	[stat_void] [int] NULL,
	[autoday] [int] NULL,
	[autoversion] [int] NULL,
 CONSTRAINT [PK_Document] PRIMARY KEY CLUSTERED 
(
	[id_Document] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DocumentAccount]    Script Date: 2022/2/20 下午10:10:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DocumentAccount](
	[id_DocumentAccount] [bigint] IDENTITY(-999999999,1) NOT NULL,
	[empname] [varchar](100) NULL,
	[id_Document] [bigint] NULL,
	[dt_create] [datetime] NULL,
	[create_by] [varchar](100) NULL,
	[update_by] [varchar](100) NULL,
	[dt_update] [datetime] NULL,
	[stat_void] [int] NULL,
	[allow_upload] [int] NULL,
	[allow_download] [int] NULL,
	[allow_createfolder] [int] NULL,
	[allow_delfolder] [int] NULL,
	[allow_delfile] [int] NULL,
	[allow_share] [int] NULL,
 CONSTRAINT [PK_DocumentAccount] PRIMARY KEY CLUSTERED 
(
	[id_DocumentAccount] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DocumentDept]    Script Date: 2022/2/20 下午10:10:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DocumentDept](
	[id_DocumentDept] [bigint] IDENTITY(-999999999,1) NOT NULL,
	[dept_no] [varchar](1000) NULL,
	[id_Document] [bigint] NULL,
	[dt_create] [datetime] NULL,
	[create_by] [varchar](100) NULL,
	[update_by] [varchar](100) NULL,
	[dt_update] [datetime] NULL,
	[stat_void] [int] NULL,
	[allow_upload] [int] NULL,
	[allow_download] [int] NULL,
	[allow_createfolder] [int] NULL,
	[allow_delfolder] [int] NULL,
	[allow_delfile] [int] NULL,
	[allow_share] [int] NULL,
 CONSTRAINT [PK_DocumentDept] PRIMARY KEY CLUSTERED 
(
	[id_DocumentDept] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DocumentFileVersion]    Script Date: 2022/2/20 下午10:10:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DocumentFileVersion](
	[id_DocumentFileVersion] [bigint] IDENTITY(-999999999,1) NOT NULL,
	[id_Document] [bigint] NULL,
	[filename] [nvarchar](max) NULL,
	[version] [int] NULL,
	[dt_create] [datetime] NULL,
	[dt_update] [datetime] NULL,
	[update_by] [varchar](100) NULL,
	[create_by] [varchar](100) NULL,
	[stat_void] [int] NULL,
 CONSTRAINT [PK_DocumentFileVersion] PRIMARY KEY CLUSTERED 
(
	[id_DocumentFileVersion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[File]    Script Date: 2022/2/20 下午10:10:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[File](
	[id_File] [bigint] IDENTITY(-999999999,1) NOT NULL,
	[id_Document] [bigint] NULL,
	[filename] [nvarchar](max) NULL,
	[relativepath] [nvarchar](max) NULL,
	[absolutepath] [nvarchar](max) NULL,
	[filesize] [nvarchar](max) NULL,
	[dt_create] [datetime] NULL,
	[dt_update] [datetime] NULL,
	[update_by] [varchar](100) NULL,
	[create_by] [varchar](100) NULL,
	[stat_void] [int] NULL,
	[comment] [nvarchar](max) NULL,
	[preview] [varchar](max) NULL,
 CONSTRAINT [PK_File] PRIMARY KEY CLUSTERED 
(
	[id_File] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserAccount]    Script Date: 2022/2/20 下午10:10:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserAccount](
	[id_UserAccount] [bigint] IDENTITY(-999999999,1) NOT NULL,
	[empname] [varchar](100) NULL,
	[dt_create] [datetime] NULL,
	[dt_update] [datetime] NULL,
	[create_by] [varchar](100) NULL,
	[update_by] [varchar](100) NULL,
	[stat_void] [int] NULL,
	[is_manager] [int] NULL,
	[is_superuser] [int] NULL,
 CONSTRAINT [PK_UserAccount] PRIMARY KEY CLUSTERED 
(
	[id_UserAccount] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserRelDept]    Script Date: 2022/2/20 下午10:10:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRelDept](
	[emp_cname] [nvarchar](100) NULL,
	[emp_ename] [varchar](30) NULL,
	[emp_no] [varchar](30) NULL,
	[company] [nvarchar](3) NOT NULL,
	[dept_no] [varchar](100) NOT NULL,
	[key] [nvarchar](263) NULL
) ON [PRIMARY]
GO

if not exists(select 1 from [Dept])
begin

INSERT [dbo].[Dept] ([dept_no], [dept_name], [key], [parent_dept_no]) VALUES (N'FA0', N'資訊管理處', N'FA0-資訊管理處', N'F90')

INSERT [dbo].[Dept] ([dept_no], [dept_name], [key], [parent_dept_no]) VALUES (N'FA1', N'營運資訊管理A部', N'FA1-營運資訊管理部', N'FA0')

INSERT [dbo].[Dept] ([dept_no], [dept_name], [key], [parent_dept_no]) VALUES (N'FA2', N'資訊系統整合A部', N'FA2-資訊系統整合部', N'FA0')


end


SET IDENTITY_INSERT [dbo].[dept_level] ON 
GO


if not exists(select 1 from [dept_level])
begin

INSERT [dbo].[dept_level] ([id_dept_level], [level_string], [dept_no], [dt_create], [stat_void]) VALUES (-999999999, N'F90-FA0', N'FA0', CAST(N'2022-02-20T13:58:16.743' AS DateTime), 0)

INSERT [dbo].[dept_level] ([id_dept_level], [level_string], [dept_no], [dt_create], [stat_void]) VALUES (-999999998, N'F90-FA0-FA1', N'FA1', CAST(N'2022-02-20T13:58:16.750' AS DateTime), 0)

INSERT [dbo].[dept_level] ([id_dept_level], [level_string], [dept_no], [dt_create], [stat_void]) VALUES (-999999997, N'F90-FA0-FA2', N'FA2', CAST(N'2022-02-20T13:58:16.753' AS DateTime), 0)

end

SET IDENTITY_INSERT [dbo].[dept_level] OFF
GO

if not exists(select 1 from [UserInfo])
begin

INSERT [dbo].[UserInfo] ([emp_no], [emp_ename], [emp_cname], [email], [dept_no]) VALUES (N'10505', N'chi', N'chi', N'', N'FA0')

INSERT [dbo].[UserInfo] ([emp_no], [emp_ename], [emp_cname], [email], [dept_no]) VALUES (N'10610', N'iec', N'iec', N'', N'FA2')

INSERT [dbo].[UserInfo] ([emp_no], [emp_ename], [emp_cname], [email], [dept_no]) VALUES (N'10615', N'oan', N'oan', N'', N'FA2')

INSERT [dbo].[UserInfo] ([emp_no], [emp_ename], [emp_cname], [email], [dept_no]) VALUES (N'79002', N'lcj', N'lcj', N'', N'FA2')

INSERT [dbo].[UserInfo] ([emp_no], [emp_ename], [emp_cname], [email], [dept_no]) VALUES (N'89002', N'ang', N'ang', N'', N'FA2')

INSERT [dbo].[UserInfo] ([emp_no], [emp_ename], [emp_cname], [email], [dept_no]) VALUES (N'90003', N'ylu', N'ylu', N'', N'FA2')

INSERT [dbo].[UserInfo] ([emp_no], [emp_ename], [emp_cname], [email], [dept_no]) VALUES (N'90007', N'Alin', N'Alin', N'', N'FA2')

INSERT [dbo].[UserInfo] ([emp_no], [emp_ename], [emp_cname], [email], [dept_no]) VALUES (N'92010', N'ang', N'ang', N'', N'FA2')

INSERT [dbo].[UserInfo] ([emp_no], [emp_ename], [emp_cname], [email], [dept_no]) VALUES (N'10509', N'iwu', N'iwu', N'', N'FA1')

INSERT [dbo].[UserInfo] ([emp_no], [emp_ename], [emp_cname], [email], [dept_no]) VALUES (N'11002', N'ark', N'ark', N'', N'FA0')

INSERT [dbo].[UserInfo] ([emp_no], [emp_ename], [emp_cname], [email], [dept_no]) VALUES (N'11011', N'iao', N'iao', N'', N'FA1')

INSERT [dbo].[UserInfo] ([emp_no], [emp_ename], [emp_cname], [email], [dept_no]) VALUES (N'10404', N'ewu', N'ewu', N'', N'FA1')

INSERT [dbo].[UserInfo] ([emp_no], [emp_ename], [emp_cname], [email], [dept_no]) VALUES (N'11020', N'lin', N'lin', N'', N'FA2')

INSERT [dbo].[UserInfo] ([emp_no], [emp_ename], [emp_cname], [email], [dept_no]) VALUES (N'11027', N'hen', N'hen', N'', N'FA2')

INSERT [dbo].[UserInfo] ([emp_no], [emp_ename], [emp_cname], [email], [dept_no]) VALUES (N'11028', N'Bwho', N'Bwho', N'', N'FA1')

end

go


ALTER TABLE [dbo].[AccountDocument] ADD  CONSTRAINT [DF__AccountDo__dt_cr__4BAC3F29]  DEFAULT (getdate()) FOR [dt_create]
GO
ALTER TABLE [dbo].[AccountDocument] ADD  CONSTRAINT [DF__AccountDo__stat___4CA06362]  DEFAULT ((0)) FOR [stat_void]
GO
ALTER TABLE [dbo].[dept_level] ADD  DEFAULT (getdate()) FOR [dt_create]
GO
ALTER TABLE [dbo].[dept_level] ADD  DEFAULT ((0)) FOR [stat_void]
GO
ALTER TABLE [dbo].[Document] ADD  CONSTRAINT [DF__Document__dt_cre__571DF1D5]  DEFAULT (getdate()) FOR [dt_create]
GO
ALTER TABLE [dbo].[Document] ADD  CONSTRAINT [DF__Document__stat_v__5812160E]  DEFAULT ((0)) FOR [stat_void]
GO
ALTER TABLE [dbo].[Document] ADD  CONSTRAINT [DF__Document__autoda__0F624AF8]  DEFAULT ((1)) FOR [autoday]
GO
ALTER TABLE [dbo].[Document] ADD  DEFAULT ((0)) FOR [autoversion]
GO
ALTER TABLE [dbo].[DocumentAccount] ADD  CONSTRAINT [DF__DocumentA__dt_cr__4F7CD00D]  DEFAULT (getdate()) FOR [dt_create]
GO
ALTER TABLE [dbo].[DocumentAccount] ADD  CONSTRAINT [DF__DocumentA__stat___5070F446]  DEFAULT ((0)) FOR [stat_void]
GO
ALTER TABLE [dbo].[DocumentAccount] ADD  DEFAULT ((0)) FOR [allow_upload]
GO
ALTER TABLE [dbo].[DocumentAccount] ADD  DEFAULT ((0)) FOR [allow_download]
GO
ALTER TABLE [dbo].[DocumentAccount] ADD  DEFAULT ((0)) FOR [allow_createfolder]
GO
ALTER TABLE [dbo].[DocumentAccount] ADD  DEFAULT ((0)) FOR [allow_delfolder]
GO
ALTER TABLE [dbo].[DocumentAccount] ADD  DEFAULT ((0)) FOR [allow_delfile]
GO
ALTER TABLE [dbo].[DocumentAccount] ADD  DEFAULT ((0)) FOR [allow_share]
GO
ALTER TABLE [dbo].[DocumentDept] ADD  CONSTRAINT [DF__DocumentD__dt_cr__534D60F1]  DEFAULT (getdate()) FOR [dt_create]
GO
ALTER TABLE [dbo].[DocumentDept] ADD  CONSTRAINT [DF__DocumentD__stat___5441852A]  DEFAULT ((0)) FOR [stat_void]
GO
ALTER TABLE [dbo].[DocumentDept] ADD  DEFAULT ((0)) FOR [allow_upload]
GO
ALTER TABLE [dbo].[DocumentDept] ADD  DEFAULT ((0)) FOR [allow_download]
GO
ALTER TABLE [dbo].[DocumentDept] ADD  DEFAULT ((0)) FOR [allow_createfolder]
GO
ALTER TABLE [dbo].[DocumentDept] ADD  DEFAULT ((0)) FOR [allow_delfolder]
GO
ALTER TABLE [dbo].[DocumentDept] ADD  DEFAULT ((0)) FOR [allow_delfile]
GO
ALTER TABLE [dbo].[DocumentDept] ADD  DEFAULT ((0)) FOR [allow_share]
GO
ALTER TABLE [dbo].[DocumentFileVersion] ADD  DEFAULT ((1)) FOR [version]
GO
ALTER TABLE [dbo].[DocumentFileVersion] ADD  DEFAULT (getdate()) FOR [dt_create]
GO
ALTER TABLE [dbo].[DocumentFileVersion] ADD  DEFAULT ((0)) FOR [stat_void]
GO
ALTER TABLE [dbo].[File] ADD  CONSTRAINT [DF__File__dt_create__5BE2A6F2]  DEFAULT (getdate()) FOR [dt_create]
GO
ALTER TABLE [dbo].[File] ADD  CONSTRAINT [DF__File__stat_void__5CD6CB2B]  DEFAULT ((0)) FOR [stat_void]
GO
ALTER TABLE [dbo].[File] ADD  DEFAULT ('') FOR [comment]
GO
ALTER TABLE [dbo].[File] ADD  DEFAULT ('') FOR [preview]
GO
ALTER TABLE [dbo].[UserAccount] ADD  CONSTRAINT [DF__UserAccou__dt_cr__5FB337D6]  DEFAULT (getdate()) FOR [dt_create]
GO
ALTER TABLE [dbo].[UserAccount] ADD  CONSTRAINT [DF__UserAccou__stat___60A75C0F]  DEFAULT ((0)) FOR [stat_void]
GO
ALTER TABLE [dbo].[UserAccount] ADD  DEFAULT ((0)) FOR [is_manager]
GO
ALTER TABLE [dbo].[UserAccount] ADD  DEFAULT ((0)) FOR [is_superuser]
GO
/****** Object:  StoredProcedure [dbo].[zp_check_autoversion]    Script Date: 2022/2/20 下午10:10:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/**************View****************/
  
create view [dbo].[vw_AppEmp]  
as   
  
select top 5000 emp_no,emp_ename,emp_cname,email,dept_no
from UserInfo
  
go


CREATE view [dbo].[vw_dept]
  as
  select distinct dept_no,dept_name,[key]=dept_no+'-'+dept_name
  from Dept
GO
/****** Object:  View [dbo].[vw_member]    Script Date: 2022/2/20 下午10:10:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


  CREATE view [dbo].[vw_member]
  as
  select emp_cname,emp_ename,emp_no,
  company='HeadQuarter',dept_no=isnull(dept_no,''),
	[key]=emp_cname+'-'+emp_ename+'-'+emp_no+'-'+isnull(dept_no,'')
  from UserInfo
GO


/******************************/




/*************Function*******************/

create function [dbo].[fn_get_auth] (@empname varchar(100))
returns  @result table(id_Document bigint,empname varchar(100) null,dept_no varchar(100) null,
create_by varchar(100) null,[share_type] varchar(100) null)  
begin
declare @deptno varchar(100)
/*

declare @result table(id_Document bigint,empname varchar(100) null,dept_no varchar(100) null,
create_by varchar(100) null)


declare @empname varchar(100) 
select @empname='rogerroan',@deptno='FA0'
*/

 select @deptno=b.dept_no  
 from [vw_AppEmp] b  
 where emp_ename=@empname   



--if object_id('tempdb.dbo.#byuser') is not null drop table #byuser

declare  @byuser table(id_Document bigint ,empname varchar(100) null,dept_no varchar(100) null,create_by varchar(100) null)

insert into @byuser(id_Document,empname,dept_no,create_by)
select a.id_Document,a.empname,b.dept_no,create_by=a.create_by
--into #byuser
from DocumentAccount a,dbo.vw_AppEmp b  
where a.stat_void=0  
and a.empname=b.emp_ename  
and a.empname=@empname 

--if object_id('tempdb.dbo.#deptno') is not null drop table #deptno

declare @deptno1 table(dept_no varchar(100) null,child_dept_no varchar(100) null)

insert into @deptno1(dept_no,child_dept_no)
select a.dept_no,child_dept_no=b.dept_no
--into #deptno
from DocumentDept a cross apply [fn_get_level](a.dept_no) b
where b.dept_no=@deptno


--if object_id('tempdb.dbo.#bydept') is not null drop table #bydept

declare @bydept table(id_Document bigint,empname varchar(100) null,dept_no varchar(100) null,create_by varchar(100) null)

--select  d.id_Document,empname=c.emp_ename,a.dept_no
--into #bydept
insert into @bydept(id_Document,empname,dept_no,create_by)
select b.id_Document,empname=@empname,dept_no=a.child_dept_no,create_by=b.create_by
--into #bydept
from @deptno1 a,DocumentDept b
where a.dept_no=b.dept_no
and b.stat_void=0


--if object_id('tempdb.dbo.#result') is not null drop table #result


insert into @result(id_Document,empname,dept_no,create_by,share_type)
select id_Document,empname,dept_no,create_by,'byuser'
--into #result
from @byuser
union 
select id_Document,empname,dept_no,create_by,'bydept'
from @bydept 

/*
select * 
from @result
*/
return
end

GO
/****** Object:  UserDefinedFunction [dbo].[fn_get_level]    Script Date: 2022/2/20 下午10:10:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create FUNCTION [dbo].[fn_get_level] (@dept_no varchar(100))               
returns @result table(dept_no varchar(100) null)            
--*/   
begin
/*
	declare @result table(dept_no varchar(100) null)

	declare @dept_no varchar(100)

	select @dept_no='F90'
*/
	insert into @result(dept_no)
	select dept_no
	from [dbo].[dept_level]
	where level_string like '%'+@dept_no+'%'

	return
end
GO
/****** Object:  UserDefinedFunction [dbo].[udf_Split]    Script Date: 2022/2/20 下午10:10:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create FUNCTION [dbo].[udf_Split] 
( @Words nvarchar(MAX)/*原始字串*/
, @splitStr varchar(50) /*分割字元*/
)
RETURNS @Result_Table TABLE
	   (
		 [word] nvarchar(max) NULL
	   )
BEGIN 
    Declare @TempStr nvarchar(MAX)
    
    WHILE (CHARINDEX(@splitStr,@Words)>0)/*@Words有包含分割字元就一直執行迴圈*/
	BEGIN
		Set @TempStr=SUBSTRING(@Words,1,CHARINDEX(@splitStr,@Words)-1)/*取出最前面的word*/
		Insert into @Result_Table (word) Values (@TempStr)
		
		Set @Words = REPLACE(@Words,@TempStr+@splitStr,'')/*把最前面的word加上分割字元後，取代為空字串再指派回給@Words*/
	END/*End While*/
	
	IF(LEN(RTRIM(LTRIM(@Words)))>0 And CHARINDEX(@splitStr,RTRIM(LTRIM(@Words)))=0) /*@Words有值但沒有分割字元，表示此為最後一個word*/
	Begin
	    Set @TempStr=@Words /*取出word*/
	    
		Insert into @Result_Table (word) Values (@TempStr)
	    
	End /*End IF*/

   RETURN /*回傳table變數*/
END
GO

/*************Function*******************/





/***************SP***************/


create proc [dbo].[zp_check_autoversion] @rpath nvarchar(max)
as 
set nocount on
/*
declare @rpath nvarchaR(max)
select @rpath=N'share/rogerroan/clouddata/FA0/ERP進度/a/b/c'
*/
if object_id('tempdb.dbo.#autoversion') is not null drop table #autoversion

select distinct relativepath,id_Document
into #autoversion
from Document
where stat_void=0
and autoversion=1


if exists(select 1
from #autoversion
where @rpath like relativepath+'%')
begin
    
	select [result]=1
	
end
else
begin
   
	select [result]=0
end

set nocount off
return
GO
/****** Object:  StoredProcedure [dbo].[zp_checkversion_give_filename]    Script Date: 2022/2/20 下午10:10:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--/*
CREATE proc [dbo].[zp_checkversion_give_filename] @rpath nvarchar(max),@filename nvarchaR(max)
as 
set nocount on
--*/

--truncate table DocumentFileVersion
--select * from DocumentFileVersion

/*
declare  @rpath nvarchar(max),@filename nvarchaR(max)

select @rpath=N'share/rogerroan/clouddata/FA0/ERP進度/20210722',@filename='hello.txt'
*/

declare @purfilename nvarchar(max),@extention nvarchar(max)

select @purfilename=@filename

if object_id('tempdb.dbo.#word') is not null drop table #word

select id=identity(int,1,1), [word]
into #word              
from  [dbo].[udf_Split](@filename,'.')  

select top 1 @extention=word
from #word
order by id desc

if (select count(*) from #word)=1
begin
    select  @extention=''
end

select @purfilename=replace(@filename,'.'+@extention,'')

--select @purfilename,@extention

if object_id('tempdb.dbo.#versionflag') is not null drop table #versionflag

create table #versionflag(result int null)

insert into #versionflag
exec dbo.zp_check_autoversion @rpath

declare @versionflag int,@id_document bigint,@version int

select top 1 @versionflag= result
from #versionflag


if (@versionflag=1)
begin

    select @id_document=id_Document
	from Document a
	where stat_void=0
	and relativepath=@rpath
	and stat_void=0

	if object_id('tempdb.dbo.#dcv') is not null drop table #dcv

	select id_DocumentFileVersion,id_Document,[filename],
	[version]
	into #dcv
	from [DocumentFileVersion]
	where id_Document=@id_document
	and [filename]=@filename
	and stat_void=0


	if not exists(select 1 from #dcv)
	begin
	      insert into [DocumentFileVersion](id_Document,[filename])
		  values(@id_document,@filename)

		  select @version=1
	end
	else
	begin
	     select top 1 @version= [version]
		 from #dcv

		  select @version=@version+1

		 update a
		 set [version]=@version
		 from #dcv a

		 update a
		 set [version]=b.[version]
		 from [DocumentFileVersion] a,#dcv b
		 where a.id_DocumentFileVersion=b.id_DocumentFileVersion

	end

	select [filename]=@purfilename+'_v'+convert(varchar(100),@version)+'.'+@extention

end
else

begin
    select [filename]=@filename
end

set nocount off
return
GO
/****** Object:  StoredProcedure [dbo].[zp_find_keyword]    Script Date: 2022/2/20 下午10:10:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create proc [dbo].[zp_find_keyword] @keyword nvarchar(max)
as
set nocount on
SELECT DISTINCT o.name, c.* FROM syscomments c
INNER JOIN sysobjects o ON c.id=o.id
WHERE (o.xtype = 'P'           --查SP
OR o.xtype = 'V')              --查View
And (o.name LIKE '%'+@keyword+'%'  --查SP或View名稱
OR c.text LIKE '%'+@keyword+'%')   --查SP或View內含文字

set nocount off
return 
GO
/****** Object:  StoredProcedure [dbo].[zp_gen_dept_level]    Script Date: 2022/2/20 下午10:10:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--/*    
create proc [dbo].[zp_gen_dept_level] @input_dept_no varchar(100)    
as     
set nocount on    
--*/
/*    
 declare @input_dept_no varchar(100)    
 select @input_dept_no='MC7'    
*/ 
--MC7   
    
if object_id('tempdb.dbo.#tmp') is not null drop table #tmp    
if OBJECT_ID('tempdb.dbo.#resal') is not null drop table #resal    
     
 select distinct     
 dept_no,    
 parent_dept_no=dept_no, 
 origin_parent_dept_no=parent_dept_no,   
 level=0    
 into #resal    
 from dbo.Dept    
 where dept_no=@input_dept_no   
 

 if exists(
 select 1
 from #resal
 where dept_no=origin_parent_dept_no)
 begin
      --select @string=substring(@input_dept_no,0,len(@input_dept_no))    
      
	  insert into dept_level(level_string,dept_no)    
	  values( @input_dept_no,@input_dept_no) 
	  
	  return   

 end

 begin                               
                
    WITH WUSE_CTE (parent_dept_no,dept_no,level) AS(       
    --根       
    select     
    b.dept_no,    
    a.parent_dept_no,    
    a.level    
    from #resal a,dbo.Dept b    
    where a.dept_no=b.dept_no    
    union all    
    --以根為基底往下重複做一件事    
    select dept_no=b.parent_dept_no,    
    parent_dept_no=a.dept_no,level=a.level+1    
    from  WUSE_CTE  a,dbo.Dept b    
    where a.parent_dept_no=b.dept_no    
    )    
        
    select *     
    into #tmp    
    from WUSE_CTE     
    order by [level]    
        
      
  end    
    
  if object_id('tempdb.dbo.#tmp1') is not null drop table #tmp1    
    
  select parent_dept_no,dept_no,[level]    
  into #tmp1    
  from #tmp    
  where isnull(parent_dept_no,'') not in ('22099478','')    
     
  declare @row int,@rowcount int    
    
  select @row=0    
    
  select @rowcount=count(*)    
  from #tmp1    
    
  select @rowcount=@rowcount-1    
    
  declare @string nvarchar(max),@parent_dept_no varchar(max)    
    
  select @string=''    
    
  while @row<>@rowcount+1    
  begin    
       select @parent_dept_no=parent_dept_no    
    from #tmp1     
    where [level]=@row    
            
   select @string=@parent_dept_no+'-'+@string    
    
       select @row=@row+1    
  end    
    
  select @string=substring(@string,0,len(@string))    
    
      
  insert into dept_level(level_string,dept_no)    
  values( @string,@input_dept_no)    
    
set nocount off    
return
GO
/****** Object:  StoredProcedure [dbo].[zp_gen_dept_level_all]    Script Date: 2022/2/20 下午10:10:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE proc [dbo].[zp_gen_dept_level_all]
as
set nocount on

truncate table dept_level

if object_id('tempdb.dbo.#dept_no') is not null drop table #dept_no

select distinct id=identity(int,1,1), dept_no
into #dept_no
from Dept

declare @row int,@rowcount int
select @row=1
select @rowcount=count(*) 
from #dept_no

declare @dept_no varchar(100)


while @row<>@rowcount+1
begin

	select @dept_no=dept_no
	from #dept_no
	where id=@row

	print @dept_no

	exec zp_gen_dept_level @dept_no
	
	set @row=@row+1
end
/*


truncate table dept_level

insert into dept_level(	level_string,	dept_no,	dt_create	,stat_void)
select level_string,	dept_no,	dt_create	,stat_void
from dept_level_1115

*/

set nocount off
return
GO
/****** Object:  StoredProcedure [dbo].[zp_get_dashboard_get_files]    Script Date: 2022/2/20 下午10:10:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE proc [dbo].[zp_get_dashboard_get_files] @rpath nvarchar(max)
as
set nocount on
--取得資料匣的detail在dashboard
/*
declare @rpath nvarchar(max)
select @rpath=N'share/rogerroan/clouddata/FA0/週會'
*/
select b.[filename],b.create_by,b.dt_create,a.absolutepath
from Document a,[File] b
where a.relativepath=@rpath
and a.stat_void=0
and a.id_Document=b.id_Document
and b.stat_void=0
order by b.dt_create desc

set nocount off
return
GO
/****** Object:  StoredProcedure [dbo].[zp_get_disk_space_report]    Script Date: 2022/2/20 下午10:10:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--/*
CREATE proc [dbo].[zp_get_disk_space_report] @empname varchar(100)
as 
set nocount on
--*/
/*
declare @empname varchar(100)
select @empname='yuriwu'
*/

declare @fileziestring varchaR(100)

select @fileziestring='0 MB'

--/*

declare @maxlimt int

set @maxlimt=5

select @fileziestring=convert(int,round(sum(convert(bigint,filesize))/1024/convert(decimal(14,1),1024),0))
from AccountDocument a,[File] b
where a.empname=@empname
and a.stat_void=0
and a.id_Document=b.id_Document
and b.stat_void=0
--*/
/*
select @fileziestring=convert(int,round(sum(convert(bigint,filesize))/1024/convert(decimal(14,1),1024),0))
from  [File] b
where b.stat_void=0
*/

--select @fileziestring

--select @fileziestring='0'

if isnull(@fileziestring,'')=''
begin
   select @fileziestring='0'
end

declare @files bigint,@percen  numeric(14,2),@percenstring varchar(100)

select @files=convert(bigint,@fileziestring)


if  len(@fileziestring)<4
begin
   select @fileziestring=@fileziestring+' MB'
   select @percen=(@files/convert(decimal(14,2),@maxlimt*1000))*100

end 
else 
begin
   select @files=@files/1000
   select @fileziestring=convert(bigint,@fileziestring)/1000
   select @fileziestring=@fileziestring+' GB'
   select @percen=(@files/convert(decimal(14,2),@maxlimt))*100
end

select @percenstring=convert(varchar(max),@percen)+'%'
--select @fileziestring, @percenstring

if object_id('tempdb.dbo.#result') is not null drop table #result
create table #result(used varchar(100) null,percen varchar(100) null)

insert into #result(used,percen)
values( @fileziestring,@percenstring)

select used,percen,maxlimit=@maxlimt
from #result

set nocount off
return
GO
/****** Object:  StoredProcedure [dbo].[zp_get_doc_size_report]    Script Date: 2022/2/20 下午10:10:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--/*
CREATE proc [dbo].[zp_get_doc_size_report] @empname varchar(100)  
as  
set nocount on  
--*/

/*  
declare @empname varchar(100)  
select @empname='rogerroan'  
*/  
  
if object_id('tempdb.dbo.#result') is not null drop table #result  

create table #result(id_Document bigint null,docname nvarchar(max) null, absolutepath nvarchar(max) null,
relativepath nvarchar(max) null,filesize bigint null,percen bigint null) 

insert into #result(id_Document,docname,absolutepath,relativepath,filesize,percen)
select b.id_Document,b.docname,b.absolutepath,b.relativepath,  
filesize=convert(bigint,0),--sum(convert(bigint,c.filesize))/1024/1024,  
percen=convert(bigint,0)  
--into #result  
from AccountDocument a,Document b--,[File] c  
where a.empname=@empname  
and a.stat_void=0  
and a.id_Document=b.id_Document  
and b.stat_void=0  
--and b.id_Document=c.id_Document  
--and c.stat_void=0  
group by b.id_Document, b.docname,b.absolutepath,b.relativepath  

if object_id('tempdb.dbo.#size') is not null drop table #size 

select a.id_Document,filesize=sum(convert(bigint,b.filesize))/1024/1024
into #size
from #result a,[File] b
where a.id_Document=b.id_Document
and b.stat_void=0
group by a.id_Document


update a
set filesize=b.filesize
from #result a,#size b
where a.id_Document=b.id_Document


if not exists (select 1 from #result)
begin
    insert into #result(docname,relativepath,filesize,percen)
	values('clouddata','',0,1)
end
  
if object_id('tempdb.dbo.#result1') is not null drop table #result1
  
select top 10 id_Document,docname,absolutepath,relativepath,filesize,percen
into #result1
from #result
order by filesize desc

declare @total numeric(14,1)  
  
select @total=sum(filesize)  
from #result1  
  
--update a  
--select     
update a  
set percen= case when @total=0   
        then 1  
     else  
             convert(int,(filesize/@total)*100)  
     end  
from #result1 a  

  
select docname,relativepath,filesize,percen  
from #result1 
   

set nocount off  
return  
GO
/****** Object:  StoredProcedure [dbo].[zp_get_file_datatable]    Script Date: 2022/2/20 下午10:10:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--/*    
CREATE proc [dbo].[zp_get_file_datatable] @empname varchar(100)      
as      
set nocount on      
--*/    
/*      
declare @empname varchar(100)      
set @empname='yuriwu'      
*/      
--/*      
if object_id('tempdb.dbo.#byuser1') is not null drop table #byuser1      
      
--select  c.[filename],b.docname,      
--filesize=convert(numeric(3,1),(convert(int,c.filesize)/(1024*1024.0))),b.relativepath,b.absolutepath,a.create_by,a.empname,      
--c.dt_create,category=N'我的雲端硬碟'      
--into #byuser1      
--from dbo.AccountDocument a,Document b,[File] c      
--where a.stat_void=0      
--and b.stat_void=0      
--and a.id_Document=b.id_Document      
--and c.stat_void=0      
--and b.id_Document=c.id_Document      
--and c.stat_void=0      
--and a.empname=@empname      
--union      
--*/      
select distinct  b.id_Document    
into #byuser1    
from dbo.DocumentAccount a,Document b      
where a.stat_void=0      
and b.stat_void=0      
and a.id_Document=b.id_Document      
and a.empname=@empname      
      
      
--select * from DocumentAccount      
declare @deptno varchar(100)      
/*      
declare @empname varchar(100)       
select @empname='humark',@deptno='FA0'      
*/      

 select @deptno=b.dept_no  
 from [vw_AppEmp] b  
 where emp_ename=@empname   


     
      
if object_id('tempdb.dbo.#deptno') is not null drop table #deptno      
      
select a.dept_no,child_dept_no=b.dept_no      
into #deptno      
from DocumentDept a cross apply [fn_get_level](a.dept_no) b      
where b.dept_no=@deptno      
      
if object_id('tempdb.dbo.#bydept') is not null drop table #bydept      
      
select distinct  b.id_Document     
into #bydept      
from #deptno a,DocumentDept b      
where a.dept_no=b.dept_no      
and b.stat_void=0      
    
    
if object_id('tempdb.dbo.#result') is not null drop table #result    
      
select id_Document    
into #result    
from #byuser1    
union    
select id_Document    
from #bydept    
    
    
if object_id('tempdb.dbo.#parent')is not null drop table #parent    
    
select  a.id_Document,b.relativepath,[level]=0    
into #parent    
from #result a,Document b    
where a.id_Document=b.id_Document    
and b.stat_void=0    
  /*  
  select*  
  from #parent a,Document b  
  where b.relativepath like a.relativepath+'%'  
  */  
if object_id('tempdb.dbo.#doc') is not null drop table #doc    
    
 begin                               
                
    WITH WUSE_CTE (id_Document,relativepath,[level]) AS(       
    --根       
    select     
    distinct a.id_Document,a.relativepath,a.[level]    
    from #parent a, Document b    
    where a.relativepath like b.relativepath+'%'    
    and b.stat_void=0    
    union all    
    --以根為基底往下重複做一件事    
    select b.id_Document,b.relativepath,level=c.level+1    
    from Document b,WUSE_CTE c    
    where b.stat_void=0    
    and b.relativepath like c.relativepath+'%'    
    and isnull(b.relativepath,'')<>isnull(c.relativepath,'')    
    --and c.level<10    
    )    
        
    select id_Document,relativepath,[level]    
    into #doc    
    from WUSE_CTE     
          OPTION(MAXRECURSION 99);    
  end   
    
    
select distinct c.filename,b.docname,    
filesize=convert(numeric(13,1),(convert(int,c.filesize)/(1024*1024.0))),    
b.relativepath,b.absolutepath,c.create_by,empname=convert(varchar(100),''),c.dt_create,    
category=N'我的資料夾',keyword=c.comment    
from #doc a,Document b,[File] c    
where a.id_Document=b.id_Document    
and b.stat_void=0    
and b.id_Document=c.id_Document    
and c.stat_void=0    
    
/*      
select [filename],docname,filesize, relativepath, absolutepath, create_by ,empname,dt_create,      
category      
from #result      
 */    
set nocount off      
return
GO
/****** Object:  StoredProcedure [dbo].[zp_get_file_datatable_bk]    Script Date: 2022/2/20 下午10:10:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--/*
CREATE proc [dbo].[zp_get_file_datatable_bk] @empname varchar(100)    
as    
set nocount on    
--*/
/*    
declare @empname varchar(100)    
set @empname='rogerroan'    
*/    
--/*    
if object_id('tempdb.dbo.#byuser1') is not null drop table #byuser1    
    
select  c.[filename],b.docname,    
filesize=convert(numeric(3,1),(convert(int,c.filesize)/(1024*1024.0))),b.relativepath,b.absolutepath,a.create_by,a.empname,    
c.dt_create,category=N'我的雲端硬碟'    
into #byuser1    
from dbo.AccountDocument a,Document b,[File] c    
where a.stat_void=0    
and b.stat_void=0    
and a.id_Document=b.id_Document    
and c.stat_void=0    
and b.id_Document=c.id_Document    
and c.stat_void=0    
and a.empname=@empname    
union     
select distinct c.[filename],b.docname,    
filesize=convert(numeric(3,1),(convert(int,c.filesize)/(1024*1024.0))),b.relativepath,b.absolutepath,a.create_by,a.empname,    
c.dt_create,category=N'我的資料夾'    
from dbo.DocumentAccount a,Document b,[File] c    
where a.stat_void=0    
and b.stat_void=0    
and a.id_Document=b.id_Document    
and b.id_Document=c.id_Document    
and c.stat_void=0    
and a.empname=@empname    
    
    
--select * from DocumentAccount    
declare @deptno varchar(100)    
/*    
declare @empname varchar(100)     
select @empname='humark',@deptno='FA0'    
*/    

 select @deptno=b.dept_no  
 from [vw_AppEmp] b  
 where emp_ename=@empname   


 
    
if object_id('tempdb.dbo.#deptno') is not null drop table #deptno    
    
select a.dept_no,child_dept_no=b.dept_no    
into #deptno    
from DocumentDept a cross apply [fn_get_level](a.dept_no) b    
where b.dept_no=@deptno    
    
if object_id('tempdb.dbo.#bydept') is not null drop table #bydept    
    
select b.id_Document,empname=@empname,dept_no=a.child_dept_no,create_by=b.create_by    
into #bydept    
from #deptno a,DocumentDept b    
where a.dept_no=b.dept_no    
and b.stat_void=0    
    
if object_id('tempdb.dbo.#bydept1') is not null drop table #bydept1    
    
select distinct c.[filename],b.docname,    
filesize=convert(numeric(3,1),(convert(int,c.filesize)/(1024*1024.0))),b.relativepath,b.absolutepath,a.create_by,a.empname,    
c.dt_create,category=N'我的資料夾'    
into #bydept1    
from #bydept a,Document b,[File] c    
where a.id_Document=b.id_Document    
and b.stat_void=0    
and b.id_Document=c.id_Document    
and c.stat_void=0    
    
if object_id('tempdb.dbo.#result') is not null drop table #result    
    
select [filename],docname,filesize, relativepath, absolutepath, create_by ,empname,dt_create,    
category    
into #result    
from #bydept1    
union    
select [filename],docname,filesize, relativepath, absolutepath, create_by ,empname,dt_create,    
category    
from #byuser1     
    
    
select [filename],docname,filesize, relativepath, absolutepath, create_by ,empname,dt_create,    
category    
from #result    
    
set nocount off    
return
GO
/****** Object:  StoredProcedure [dbo].[zp_get_file_datatable_for_manager]    Script Date: 2022/2/20 下午10:10:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

  
--/*    
CREATE proc [dbo].[zp_get_file_datatable_for_manager] @empname varchar(100)      
as      
set nocount on      
--*/    
/*      
declare @empname varchar(100)      
set @empname='yuriwu'      
*/      
--/*      
if object_id('tempdb.dbo.#byuser1') is not null drop table #byuser1      
      
select distinct b.id_Document     
into #byuser1      
from dbo.AccountDocument a,Document b,[File] c      
where a.stat_void=0      
and b.stat_void=0      
and a.id_Document=b.id_Document      
and c.stat_void=0      
and b.id_Document=c.id_Document      
and c.stat_void=0      
and a.empname=@empname      
union      
select distinct  b.id_Document    
from dbo.DocumentAccount a,Document b      
where a.stat_void=0      
and b.stat_void=0      
and a.id_Document=b.id_Document      
and a.empname=@empname      
      
      
--select * from DocumentAccount      
declare @deptno varchar(100)      
/*      
declare @empname varchar(100)       
select @empname='humark',@deptno='FA0'      
*/      

 select @deptno=b.dept_no  
 from [vw_AppEmp] b  
 where emp_ename=@empname   


    
      
if object_id('tempdb.dbo.#deptno') is not null drop table #deptno      
      
select a.dept_no,child_dept_no=b.dept_no      
into #deptno      
from DocumentDept a cross apply [fn_get_level](a.dept_no) b      
where b.dept_no=@deptno      
      
if object_id('tempdb.dbo.#bydept') is not null drop table #bydept      
      
select distinct  b.id_Document     
into #bydept      
from #deptno a,DocumentDept b      
where a.dept_no=b.dept_no      
and b.stat_void=0      
    
    
if object_id('tempdb.dbo.#result') is not null drop table #result    
      
select id_Document    
into #result    
from #byuser1    
union    
select id_Document    
from #bydept    
    
    
if object_id('tempdb.dbo.#parent')is not null drop table #parent    
    
select a.id_Document,b.relativepath,[level]=0    
into #parent    
from #result a,Document b    
where a.id_Document=b.id_Document    
and b.stat_void=0    
    
if object_id('tempdb.dbo.#doc') is not null drop table #doc    
    
 begin                               
                
    WITH WUSE_CTE (id_Document,relativepath,[level]) AS(       
    --根       
    select     
    distinct a.id_Document,a.relativepath,a.[level]    
    from #parent a, Document b    
    where a.relativepath like b.relativepath+'%'    
    and b.stat_void=0    
    union all    
    --以根為基底往下重複做一件事    
    select b.id_Document,b.relativepath,level=c.level+1    
    from Document b,WUSE_CTE c    
    where b.stat_void=0    
    and b.relativepath like c.relativepath+'%'    
    and isnull(b.relativepath,'')<>isnull(c.relativepath,'')    
    --and c.level<10    
    )    
        
    select id_Document,relativepath,[level]    
    into #doc    
    from WUSE_CTE     
          OPTION(MAXRECURSION 99);    
  end    
    
select distinct c.filename,b.docname,    
filesize=convert(numeric(3,1),(convert(int,c.filesize)/(1024*1024.0))),    
b.relativepath,b.absolutepath,c.create_by,empname=convert(varchar(100),''),c.dt_create,    
category=N'我的資料夾',keyword=c.comment    
from #doc a,Document b,[File] c    
where a.id_Document=b.id_Document    
and b.stat_void=0    
and b.id_Document=c.id_Document    
and c.stat_void=0    
    
/*      
select [filename],docname,filesize, relativepath, absolutepath, create_by ,empname,dt_create,      
category      
from #result      
 */    
set nocount off      
return
GO
/****** Object:  StoredProcedure [dbo].[zp_get_mangers_list]    Script Date: 2022/2/20 下午10:10:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE proc [dbo].[zp_get_mangers_list]  
as   
set nocount on  
select b.emp_cname,b.emp_ename,b.emp_no ,a.dt_create,a.id_UserAccount 
from [UserAccount] a ,vw_AppEmp b  
where a.empname=b.emp_ename  
and a.stat_void=0  
and a.is_manager=1  
order by a.dt_create desc
  
set nocount off  
return  
GO
/****** Object:  StoredProcedure [dbo].[zp_get_member_auth_detail]    Script Date: 2022/2/20 下午10:10:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--/*    
CREATE proc [dbo].[zp_get_member_auth_detail] @empname varchar(100),@rpath nvarchar(max)    
as     
set nocount on    
--*/    
/*    
declare @empname varchar(100)    
select @empname='yuriwu'    
declare @rpath nvarchar(max)    
select @rpath=N'share/yuriwu/clouddata/M00智慧製造中心'     
*/    

declare @deptno varchar(100)


 select @deptno=b.dept_no  
 from [vw_AppEmp] b  
 where emp_ename=@empname   


 
if object_id('tempdb.dbo.#auth_list') is not null drop table #auth_list    
    
select  a.id_Document,a.empname,a.dept_no,a.create_by,b.docname,b.relativepath,    
c.allow_upload,c.allow_download,c.allow_createfolder,c.allow_delfolder,    
c.allow_delfile,c.allow_share,c.dt_create    
into #auth_list    
from fn_get_auth(@empname) a,Document b,DocumentAccount c    
where a.share_type='byuser'    
and a.id_Document=b.id_Document    
and b.stat_void=0    
and b.id_Document=c.id_Document    
and c.stat_void=0    
and c.empname=@empname  
and @rpath like b.relativepath+'%'    
--and b.relativepath like @rpath+'%'    
union    
select  a.id_Document,a.empname,a.dept_no,a.create_by,b.docname,b.relativepath,    
c.allow_upload,c.allow_download,c.allow_createfolder,c.allow_delfolder,    
c.allow_delfile,c.allow_share,c.dt_create    
from fn_get_auth(@empname) a,Document b,DocumentDept c    
where a.share_type='bydept'    
and a.id_Document=b.id_Document    
and b.stat_void=0    
and b.id_Document=c.id_Document    
and c.stat_void=0 
and a.dept_no=@deptno   
and @rpath like b.relativepath+'%'    
--and b.relativepath like @rpath+'%'    
    
if object_id('tempdb.dbo.#result') is not null drop table #result    
    
select id_Document,empname,dept_no,create_by,docname,relativepath,    
allow_upload=max(allow_upload),    
allow_download=max(allow_download),    
allow_createfolder=max(allow_createfolder),    
allow_delfolder=max(allow_delfolder),    
allow_delfile=max(allow_delfile),    
allow_share=max(allow_share),    
dt_create=max(dt_create)    
into #result    
from #auth_list    
group by id_Document,empname,dept_no,create_by,docname,relativepath    
    
    
select     
id_Document, empname, dept_no, create_by,    
docname, relativepath, allow_upload, allow_download,     
allow_createfolder,    
allow_delfolder, allow_delfile, allow_share,dt_create    
from #result    
    
set nocount off    
return 
GO
/****** Object:  StoredProcedure [dbo].[zp_get_recent_upload_report]    Script Date: 2022/2/20 下午10:10:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



--/*
CREATE proc [dbo].[zp_get_recent_upload_report] @empname varchar(100)
as
set nocount on
--*/
/*
declare @empname varchar(100)
set @empname='rogerroan'
*/
if object_id('tempdb.dbo.#result') is not null drop table #result

select  b.docname,c.filename,
filesize=convert(varchar(100),convert(numeric(13,2),convert(bigint,c.filesize)/1024/convert(decimal(14,2),1024)))
,b.absolutepath,b.relativepath,c.dt_create
into #result
from AccountDocument a,Document b,[File] c
where a.id_Document=b.id_Document
and a.stat_void=0
and b.stat_void=0
and b.id_Document=c.id_Document
and c.stat_void=0
and a.empname=@empname

select top 6
docname,[filename],	filesize,	absolutepath,	relativepath,	dt_create
from #result
order by dt_create desc

set nocount off
return
GO
/****** Object:  StoredProcedure [dbo].[zp_get_share_folder_dept_list]    Script Date: 2022/2/20 下午10:10:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 CREATE proc [dbo].[zp_get_share_folder_dept_list] @rpath nvarchar(max)      
  as      
  set nocount on     
  --*/   
  /*      
  declare @rpath nvarchar(max)      
      
  select @rpath=N'share/rogerroan/clouddata/cd1'      
  */      
  if object_id('tempdb.dbo.#result') is not null drop table #result  
  select b.id_DocumentDept,b.id_Document,b.dept_no,dept_name=convert(nvarchar(max),'') ,
  allow_upload,
  allow_download,allow_createfolder,allow_delfolder,
  allow_delfile,allow_share
  into #result   
  from Document a,DocumentDept b   
  where relativepath=@rpath      
  and a.id_Document=b.id_Document      
  and b.stat_void=0      
  
  

  update a  
  set dept_name=b.dept_name  
  --select *  
  from #result a,Dept b  
  where a.dept_no=b.dept_no  
    


  select id_DocumentDept,id_Document,dept_no,dept_name  ,
  allow_upload,
  allow_download,allow_createfolder,allow_delfolder,
  allow_delfile,allow_share
  from #result  
  order by 1 desc  
      
  set nocount off      
  return   
GO
/****** Object:  StoredProcedure [dbo].[zp_get_share_folder_user_list]    Script Date: 2022/2/20 下午10:10:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

  
  CREATE proc [dbo].[zp_get_share_folder_user_list] @rpath nvarchar(max)    
  as    
  set nocount on    
  /*    
  declare @rpath nvarchar(max)    
    
  select @rpath=N'share/rogerroan/clouddata/share data2'    
  */    
  select b.id_DocumentAccount,b.id_Document,d.emp_cname,d.emp_ename,d.emp_no ,
  allow_upload,
  allow_download,allow_createfolder,allow_delfolder,
  allow_delfile,allow_share
  from Document a,DocumentAccount b,vw_AppEmp d    
  where relativepath=@rpath    
  and a.id_Document=b.id_Document    
  and b.stat_void=0    
  and a.stat_void=0    
  and b.empname=d.emp_ename 
  order by b.dt_create desc  
    
  set nocount off    
  return  
GO
/****** Object:  StoredProcedure [dbo].[zp_get_sharetome_list]    Script Date: 2022/2/20 下午10:10:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

  
--/*  
CREATE proc [dbo].[zp_get_sharetome_list] @empname varchar(100)  
as  
set nocount on  
--*/  
--select * from DocumentAccount  
declare @deptno varchar(100)  
/*  
declare @empname varchar(100)   
select @empname='rogerroan',@deptno='FA0'  
*/  

 select @deptno=b.dept_no  
 from [vw_AppEmp] b  
 where emp_ename=@empname   



if object_id('tempdb.dbo.#byuser') is not null drop table #byuser  
  
select a.id_Document,a.empname,b.dept_no,create_by=a.create_by  
into #byuser  
from DocumentAccount a,dbo.vw_AppEmp b  
where a.stat_void=0  
and a.empname=b.emp_ename  
and a.empname=@empname 
  
if object_id('tempdb.dbo.#deptno') is not null drop table #deptno  
  
select a.dept_no,child_dept_no=b.dept_no  
into #deptno  
from DocumentDept a cross apply [fn_get_level](a.dept_no) b  
where b.dept_no=@deptno  
  
  
if object_id('tempdb.dbo.#bydept') is not null drop table #bydept  
  
--select  d.id_Document,empname=c.emp_ename,a.dept_no  
--into #bydept  
select b.id_Document,empname=@empname,dept_no=a.child_dept_no,create_by=b.create_by  
into #bydept  
from #deptno a,DocumentDept b  
where a.dept_no=b.dept_no  
and b.stat_void=0  
  
  
if object_id('tempdb.dbo.#result') is not null drop table #result  
  
select id_Document,empname,dept_no,create_by  
into #result  
from #byuser  
union   
select id_Document,empname,dept_no,create_by  
from #bydept  
  
  
select distinct a.id_Document,a.empname,  
a.dept_no,  
docname=docname,  
c.relativepath,c.absolutepath,a.create_by  ,
docname2=replace
   (substring(c.relativepath,charindex('clouddata',c.relativepath),len(
   c.relativepath)),'clouddata/','')
from #result a,Document c  
where a.id_Document=c.id_Document  
and c.stat_void=0  
order by a.id_Document desc  
  
set nocount off  
return
GO
/****** Object:  StoredProcedure [dbo].[zp_get_sharetome_list_for_dashboard]    Script Date: 2022/2/20 下午10:10:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--/*  
CREATE proc [dbo].[zp_get_sharetome_list_for_dashboard] @empname varchar(100),@top int  
as  
set nocount on  
--*/  
--select * from DocumentAccount  
declare @deptno varchar(100)  
/*  
declare @empname varchar(100)   
select @empname='rogerroan',@deptno='FA0'  
  
declare @top int  
select @top=4  
*/  


 select @deptno=b.dept_no  
 from [vw_AppEmp] b  
 where emp_ename=@empname   


  
if object_id('tempdb.dbo.#byuser') is not null drop table #byuser  
  
select a.id_Document,a.empname,b.dept_no,create_by=a.create_by  
into #byuser  
from DocumentAccount a,dbo.vw_AppEmp b  
where a.stat_void=0  
and a.empname=b.emp_ename  
and a.empname=@empname   
  
if object_id('tempdb.dbo.#deptno') is not null drop table #deptno  
  
select a.dept_no,child_dept_no=b.dept_no  
into #deptno  
from DocumentDept a cross apply [fn_get_level](a.dept_no) b  
where b.dept_no=@deptno  
  
  
if object_id('tempdb.dbo.#bydept') is not null drop table #bydept  
  
--select  d.id_Document,empname=c.emp_ename,a.dept_no  
--into #bydept  
select b.id_Document,empname=@empname,dept_no=a.child_dept_no,create_by=b.create_by  
into #bydept  
from #deptno a,DocumentDept b  
where a.dept_no=b.dept_no  
and b.stat_void=0  
  
  
if object_id('tempdb.dbo.#result') is not null drop table #result  
  
select id_Document,empname,dept_no,create_by  
into #result  
from #byuser  
union   
select id_Document,empname,dept_no,create_by  
from #bydept  
  
declare @autodayfolder varchar(max)  
select @autodayfolder=convert(varchar(10),getdate(),112)  
  
if object_id('tempdb.dbo.#final') is not null drop table #final  
  
select  distinct id=identity(int,1,1),  
 a.id_Document,a.empname,  
a.dept_no,  
docname=case when c.autoday=0 then docname else docname+'/'+@autodayfolder end,  
relativepath=case when c.autoday=0 then c.relativepath else c.relativepath+'/'+@autodayfolder end,  
absolutepath=case when c.autoday=0 then c.absolutepath else c.absolutepath+'/'+@autodayfolder end,  
a.create_by,c.dt_create ,
abpath=absolutepath 
into #final  
from #result a,Document c  
where a.id_Document=c.id_Document  
and c.stat_void=0  
order by a.id_Document asc  
  
declare @limit int  
  
select @limit=count(*)  
from #final  
  
select top(@top) id ,id_Document, empname,  
dept_no, docname, relativepath,   
absolutepath, create_by ,limit=@limit,dt_create,  
docname2=replace  
   (substring(relativepath,charindex('clouddata',relativepath),len(  
   relativepath)),'clouddata/','')  ,
   abpath
from #final  
  
  
set nocount off  
return  
GO
/****** Object:  StoredProcedure [dbo].[zp_get_sharetome_list_manager_for_dashboard]    Script Date: 2022/2/20 下午10:10:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

  
--/*    
CREATE proc [dbo].[zp_get_sharetome_list_manager_for_dashboard] @empname varchar(100),@top int    
as    
set nocount on    
--*/    
--select * from DocumentAccount    
declare @deptno varchar(100)    
/*    
declare @empname varchar(100)     
select @empname='rogerroan',@deptno='FA0'    
    
declare @top int    
select @top=10
*/    

 select @deptno=b.dept_no  
 from [vw_AppEmp] b  
 where emp_ename=@empname   



  
  
 if object_id('tempdb.dbo.#deptno') is not null drop table #deptno  
  
 select a.dept_no,child_dept_no=b.dept_no  
 into #deptno  
 from DocumentDept a cross apply [fn_get_level](a.dept_no) b  
 where b.dept_no=@deptno  
  
 if object_id('tempdb.dbo.#bydept') is not null drop table #bydept  
  
 --select  d.id_Document,empname=c.emp_ename,a.dept_no  
 --into #bydept  
 select b.id_Document,empname=@empname,dept_no=a.child_dept_no,create_by=b.create_by  
 into #bydept  
 from #deptno a,DocumentDept b  
 where a.dept_no=b.dept_no  
 and b.stat_void=0  
  
 if object_id('tempdb.dbo.#result') is not null drop table #result  
  
 select distinct b.id_Document,b.docname,b.relativepath,b.absolutepath 
 ,empname=@empname,b.create_by
 into #result  
 from #bydept a,Document b  
 where a.id_Document=b.id_Document  
 and b.stat_void=0  
 union  
 select distinct d.id_Document,c.docname,c.relativepath,c.absolutepath  
 ,empname=@empname,c.create_by
 from AccountDocument a,AccountDocument b,Document c,DocumentAccount d  
 where a.empname=@empname  
 and a.stat_void=0  
 and a.id_Document=b.id_Document  
 and b.stat_void=0  
 and b.id_Document=c.id_Document  
 and c.stat_void=0  
 and c.id_Document=d.id_Document  
 and d.stat_void=0  


 
declare @autodayfolder varchar(max)    
select @autodayfolder=convert(varchar(10),getdate(),112)    
    
if object_id('tempdb.dbo.#final') is not null drop table #final    
    
select  distinct id=identity(int,1,1),    
 a.id_Document,a.empname,    
dept_no=@deptno,    
docname=case when c.autoday=0 then a.docname else a.docname+'/'+@autodayfolder end,    
relativepath=case when c.autoday=0 then c.relativepath else c.relativepath+'/'+@autodayfolder end,    
absolutepath=case when c.autoday=0 then c.absolutepath else c.absolutepath+'/'+@autodayfolder end,    
a.create_by,c.dt_create ,  
abpath=c.absolutepath ,
c.autoversion
into #final    
from #result a,Document c    
where a.id_Document=c.id_Document    
and c.stat_void=0    
order by a.id_Document asc    
    
declare @limit int    
    
select @limit=count(*)    
from #final    
    
select top(@top) id ,id_Document, empname,    
dept_no, docname, relativepath,     
absolutepath, create_by ,limit=@limit,dt_create,    
docname2=replace    
   (substring(relativepath,charindex('clouddata',relativepath),len(    
   relativepath)),'clouddata/','')  ,  
   abpath ,autoversion 
from #final    
  
    
set nocount off    
return    
GO
/****** Object:  StoredProcedure [dbo].[zp_get_sharetometop10_report]    Script Date: 2022/2/20 下午10:10:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

      
--/*      
CREATE proc [dbo].[zp_get_sharetometop10_report] @empname varchar(100)      
as      
set nocount on      
--*/      
--select * from DocumentAccount      
declare @deptno varchar(100)      
/*      
declare @empname varchar(100)       
select @empname='yuriwu',@deptno='FA0'      
*/      

 select @deptno=b.dept_no  
 from [vw_AppEmp] b  
 where emp_ename=@empname   



if object_id('tempdb.dbo.#byuser') is not null drop table #byuser      
      
select a.id_Document,a.empname,b.dept_no,create_by=a.create_by      
into #byuser  
from DocumentAccount a,dbo.vw_AppEmp b  
where a.stat_void=0  
and a.empname=b.emp_ename  
and a.empname=@empname   
  
      
if object_id('tempdb.dbo.#deptno') is not null drop table #deptno      
      
select a.dept_no,child_dept_no=b.dept_no      
into #deptno      
from DocumentDept a cross apply [fn_get_level](a.dept_no) b      
where b.dept_no=@deptno      
      
      
if object_id('tempdb.dbo.#bydept') is not null drop table #bydept      
      
--select  d.id_Document,empname=c.emp_ename,a.dept_no      
--into #bydept      
select b.id_Document,empname=@empname,dept_no=a.child_dept_no,create_by=b.create_by      
into #bydept      
from #deptno a,DocumentDept b      
where a.dept_no=b.dept_no      
and b.stat_void=0      
      
      
if object_id('tempdb.dbo.#result') is not null drop table #result      
      
select id_Document,empname,dept_no,create_by      
into #result      
from #byuser      
union       
select id_Document,empname,dept_no,create_by      
from #bydept      
      
      
if object_id('tempdb.dbo.#parent')is not null drop table #parent      
      
select  a.id_Document,b.relativepath,[level]=0      
into #parent      
from #result a,Document b      
where a.id_Document=b.id_Document      
and b.stat_void=0      
      
    
if object_id('tempdb.dbo.#doc') is not null drop table #doc      
      
 begin                                 
                  
    WITH WUSE_CTE (id_Document,relativepath,[level]) AS(         
    --根         
    select       
    distinct a.id_Document,a.relativepath,a.[level]      
    from #parent a, Document b      
    where a.relativepath like b.relativepath+'%'      
    and b.stat_void=0      
    union all      
    --以根為基底往下重複做一件事      
    select b.id_Document,b.relativepath,level=c.level+1      
    from Document b,WUSE_CTE c      
    where b.stat_void=0      
    and b.relativepath like c.relativepath+'%'      
    and isnull(b.relativepath,'')<>isnull(c.relativepath,'')      
    --and c.level<10      
    )      
          
    select id_Document,relativepath,[level]      
    into #doc      
    from WUSE_CTE       
    OPTION(MAXRECURSION 10);      
  end      
      
        
if object_id('tempdb.dbo.#final') is not null drop table #final      
      
select   c.docname,d.filename,      
filesize=convert(varchar(1000),convert(numeric(13,2),convert(bigint,d.filesize)/1024/convert(decimal(14,2),1024))),      
dt_create = case when d.dt_create > isnull(d.dt_update,'1900-01-01')       
                 then d.dt_create else d.dt_update end,     
d.create_by,d.absolutepath,c.relativepath --doc path      
into #final      
from #doc a,Document c,[File] d      
where a.id_Document=c.id_Document      
and c.stat_void=0      
and c.id_Document=d.id_Document      
and d.stat_void=0     

      
select distinct  top 20    
docname=replace      
   (substring(relativepath,charindex('clouddata',relativepath),len(      
   relativepath)),'clouddata/','') ,[filename],filesize,dt_create,create_by,absolutepath,relativepath      
from #final      
order by dt_create desc      
      
set nocount off      
return 
GO
/****** Object:  StoredProcedure [dbo].[zp_get_top10_file_size_report]    Script Date: 2022/2/20 下午10:10:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE proc [dbo].[zp_get_top10_file_size_report] @empname varchar(100)
as
set nocount on
/*
declare @empname varchar(100)
set @empname='rogerroan'
*/
if object_id('tempdb.dbo.#result') is not null drop table #result

select top 10  b.docname,c.filename,
filesize=convert(varchar(100),convert(numeric(13,2),convert(bigint,c.filesize)/1024/convert(decimal(14,2),1024)))
,b.absolutepath,b.relativepath,c.dt_create
into #result
from AccountDocument a,Document b,[File] c
where a.id_Document=b.id_Document
and a.stat_void=0
and b.stat_void=0
and b.id_Document=c.id_Document
and c.stat_void=0
and a.empname=@empname

select 
docname,[filename],	filesize,	absolutepath,	relativepath,	dt_create
from #result
order by filesize desc

set nocount off
return
GO
/****** Object:  StoredProcedure [dbo].[zp_inital_user]    Script Date: 2022/2/20 下午10:10:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


  create proc [dbo].[zp_inital_user] @empname varchar(100)
  as 
  set nocount on
  /*
  declare @empname varchar(100)
  select @empname='rogerroan'
  */
  if not exists(select 1 from UserAccount 
  where empname=@empname and stat_void=0)
  begin 
     INSERT INTO [dbo].[UserAccount]
           ([empname]
           ,[dt_create]
           ,[create_by])
	values(@empname,getdate(),@empname)
  end
  else
  begin
      update a
	  set dt_update=getdate()
	  from [UserAccount] a
	  where empname=@empname
	  and stat_void=0
  end

  
  select [result]='OK'


  set nocount off
  return
GO
/****** Object:  StoredProcedure [dbo].[zp_remove_directory]    Script Date: 2022/2/20 下午10:10:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--/*
CREATE proc [dbo].[zp_remove_directory] @id_Document bigint ,
@rpathold nvarchar(max),@rpathnew nvarchar(max),@update_by varchar(100)
as
set nocount on
--*/

--begin tran
--rollback
/*
declare @id_Document bigint ,
@rpathold nvarchar(max),
@rpathnew nvarchar(max),
@update_by varchar(100)

select @id_Document=-999999658
select @rpathold=N'share/rogerroan/clouddata/local/202108190/abcd'
select @rpathnew=N'share/rogerroan/clouddata/FA0/專案進度/20210722/abcd'
select @update_by='rogerroan'
*/

if object_id('tempdb.dbo.#parent')is not null drop table #parent      
      
select  b.id_Document,b.relativepath,[level]=0      
into #parent      
from Document b      
where b.id_Document=@id_Document      
and b.stat_void=0      
     
if object_id('tempdb.dbo.#doc') is not null drop table #doc      
      
 begin                                 
                  
    WITH WUSE_CTE (id_Document,relativepath) AS(         
    --根         
    select       
    distinct a.id_Document,a.relativepath    
    from #parent a, Document b      
    where a.relativepath like b.relativepath+'%'      
    and b.stat_void=0      
    union all      
    --以根為基底往下重複做一件事      
    select b.id_Document,b.relativepath      
    from Document b,WUSE_CTE c      
    where b.stat_void=0      
    and b.relativepath like c.relativepath+'%'      
    and isnull(b.relativepath,'')<>isnull(c.relativepath,'')      
    --and c.level<10      
    )      
          
    select distinct id_Document,relativepath     
    into #doc      
    from WUSE_CTE       
          OPTION(MAXRECURSION 99);      
  end     
      
	update b
	set 
	relativepath=replace(b.relativepath,@rpathold,@rpathnew),
	dt_update=getdate(),
	update_by=@update_by
	--select replace(b.relativepath,@rpathold,@rpathnew),b.relativepath
	from #doc a,Document b
	where a.id_Document=b.id_Document
	and stat_void=0
	and a.relativepath like @rpathold+'%'
	
    update b
	set 
	relativepath=replace(b.relativepath,@rpathold,@rpathnew),
    dt_update=getdate(),
	update_by=@update_by
	--select replace(b.relativepath,@rpathold,@rpathnew),b.relativepath
	from #doc a,[File] b
	where a.id_Document=b.id_Document
	and b.stat_void=0
	and a.relativepath like @rpathold+'%'


select result='OK'

set nocount off
return

GO
/****** Object:  StoredProcedure [dbo].[zp_rename_directory]    Script Date: 2022/2/20 下午10:10:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--/*
CREATE proc [dbo].[zp_rename_directory]  @rpathold nvarchar(max),@rpathnew nvarchar(max),
@newname nvarchar(max),@update_by varchar(100)
as
set nocount on
--*/
/*
declare @rpathold nvarchar(max)
declare @rpathnew nvarchar(max)
declare @newname nvarchar(max)

select @rpathold='share/rogerroan/clouddata/local/20210819/temp'
select @rpathnew='share/rogerroan/clouddata/local/20210819/tempabcde'
select @newname='tempabcde'
*/
--current
update a
set relativepath=replace(relativepath,@rpathold,@rpathnew),
docname=@newname,dt_update=getdate(),update_by=@update_by
--select relativepath,replace(relativepath,@rpathold,@rpathnew)
--select *
from [Document] a
where stat_void=0
and relativepath= @rpathold

--child
--select relativepath,replace(relativepath,@rpathold,@rpathnew)
update a
set relativepath=replace(relativepath,@rpathold,@rpathnew),
docname=@newname,
dt_update=getdate(),
update_by=@update_by
from [Document] a
where stat_void=0
and relativepath like @rpathold+'/%'

--file
--select relativepath,replace(relativepath,@rpathold,@rpathnew)
update a
set  relativepath=replace(relativepath,@rpathold,@rpathnew),
     dt_update=getdate(),
	 update_by=@update_by
from [File] a
where relativepath like @rpathold+'/%'
and stat_void=0

select result='OK'

set nocount off
return
GO
/****** Object:  StoredProcedure [dbo].[zp_share_to_other_list]    Script Date: 2022/2/20 下午10:10:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--/*  
  CREATE proc [dbo].[zp_share_to_other_list] @empname varchar(100)  
  as   
  set nocount on  
--*/  
/*  
declare @empname varchar(10)  
select @empname='rogerroan'  
*/  
declare @deptno varchar(100)  
   
   
 select @deptno=b.dept_no  
 from [vw_AppEmp] b  
 where emp_ename=@empname   



 if object_id('tempdb.dbo.#deptno') is not null drop table #deptno  
  
 select a.dept_no,child_dept_no=b.dept_no  
 into #deptno  
 from DocumentDept a cross apply [fn_get_level](a.dept_no) b  
 where b.dept_no=@deptno  
  
 if object_id('tempdb.dbo.#bydept') is not null drop table #bydept  
  
 --select  d.id_Document,empname=c.emp_ename,a.dept_no  
 --into #bydept  
 select b.id_Document,empname=@empname,dept_no=a.child_dept_no,create_by=b.create_by  
 into #bydept  
 from #deptno a,DocumentDept b  
 where a.dept_no=b.dept_no  
 and b.stat_void=0  
  
 if object_id('tempdb.dbo.#result') is not null drop table #result  
  
 select distinct b.id_Document,b.docname,b.relativepath,b.absolutepath  
 into #result  
 from #bydept a,Document b  
 where a.id_Document=b.id_Document  
 and b.stat_void=0  
 union  
 select distinct d.id_Document,c.docname,c.relativepath,c.absolutepath  
 from AccountDocument a,AccountDocument b,Document c,DocumentAccount d  
   where a.empname=@empname  
   and a.stat_void=0  
   and a.id_Document=b.id_Document  
   and b.stat_void=0  
   and b.id_Document=c.id_Document  
   and c.stat_void=0  
   and c.id_Document=d.id_Document  
   and d.stat_void=0  
  
   select distinct  a.id_Document,a.docname,a.relativepath,a.absolutepath  ,
   docname2=replace
   (substring(a.relativepath,charindex('clouddata',a.relativepath),len(
   a.relativepath)),'clouddata/','')
   from #result a,AccountDocument b  
   where a.id_Document=b.id_Document  
   and b.empname=@empname  
   and b.stat_void=0      
     order by a.id_Document desc  
  
  set nocount off  
  return
GO

/***************SP***************/
GO

USE [clouddb]
GO
SET IDENTITY_INSERT [dbo].[UserAccount] ON 
GO
INSERT [dbo].[UserAccount] ([id_UserAccount], [empname], [dt_create], [dt_update], [create_by], [update_by], [stat_void], [is_manager], [is_superuser]) VALUES (-999999999, N'lin', CAST(N'2022-02-20T14:32:53.420' AS DateTime), NULL, N'lin', NULL, 0, 1, 1)
GO
SET IDENTITY_INSERT [dbo].[UserAccount] OFF
GO



USE [master]
GO
ALTER DATABASE [clouddb] SET  READ_WRITE 
GO




