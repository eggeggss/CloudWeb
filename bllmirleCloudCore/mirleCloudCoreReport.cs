﻿using dalmirleCloudCore;
using dalmirleCloudCore.Common;
using dalmirleCloudCore.Meta;
using dalmirleCloudCore.Models;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bllmirleCloudCore
{
    public class mirleCloudCoreReport
    {
        private mirleCloudCoredal _dalmirleCloudCore;
        private IOptionsSnapshot<Profile> _profile;
        public string Root { get; set; }

        private mirleCloudCorequery _querymirleCloudCore;

        public mirleCloudCoreReport(mirleCloudCoredal dalmirleCloudCoredal,
            IOptionsSnapshot<Profile> profile,
            mirleCloudCorequery querymirleCloudCore)
        {
            _dalmirleCloudCore = dalmirleCloudCoredal;
            _profile = profile;
            var data = _profile.Value;
            Root = data.Root;
            _querymirleCloudCore = querymirleCloudCore;
        }

        public async Task<IEnumerable<zp_inital_user_Result>> InitialUser(string empname)
        {
            return await _dalmirleCloudCore.InitialUser(empname);
        }

        public async Task<IEnumerable<VwDept>> GetDepts()
        {
            return await _querymirleCloudCore.GetDepts();
        }

        public async Task<IEnumerable<VwMember>> GetMembers()
        {
            return await _querymirleCloudCore.GetMembers();
        }

        public async Task<IEnumerable<zp_get_share_folder_user_list_Result>> GetShareFolderMember(string abpath)
        {
            var rpath = Common.AbsoluteToRelative(Root, abpath);
            return await _querymirleCloudCore.GetShareFolderMember(rpath);
        }

        public async Task<IEnumerable<zp_get_share_folder_dept_list_Result>> GetShareFolderDept(string abpath)
        {
            var rpath = Common.AbsoluteToRelative(Root, abpath);
            return await _querymirleCloudCore.GetShareFolderDept(rpath);
        }


        public async Task<IEnumerable<zp_share_to_other_list_Result>> GetShareToOtherTree(string empname)
        {
            return await _querymirleCloudCore.GetShareToOtherTree(empname);
        }

        public async Task<IEnumerable<zp_get_sharetome_list_Result>> GetShareToMeTree(string empname)
        {
            return await _querymirleCloudCore.GetShareToMeTree(empname);
        }

        public async Task<IEnumerable<zp_get_sharetome_list_for_dashboard_Result>> GetShareToMeTreeForDashBoard(string empname,int top)
        {
            return await _querymirleCloudCore.GetShareToMeTreeForDashBoard(empname,top);
        }

        public async Task<IEnumerable<zp_get_sharetome_list_manager_for_dashboard_Result>> GetShareToMeTreeForManagerDashBoard(string empname, int top)
        {
            return await _querymirleCloudCore.GetShareToMeTreeForManagerDashBoard(empname, top);
        }


        public async Task<IEnumerable<zp_get_file_datatable_Result>> GetFileDataTable(string empname)
        {
            return await _querymirleCloudCore.GetFileDataTable(empname);
        }

        public async Task<VwAppEmp> GetAppEmpsInfoByName(string ename)
        {
            return await _querymirleCloudCore.GetAppEmpsInfoByName(ename);
        }

        public async Task<IEnumerable<VwDept>> GetDeptNos()
        {
            return await _querymirleCloudCore.GetDeptNos();
        }

        public async Task<IEnumerable<zp_get_doc_size_report_Result>> Getdocsize_report(string empname)
        {
            return await _querymirleCloudCore.Getdocsize_report(empname);
        }

        public async Task<IEnumerable<zp_get_disk_space_report_Result>> GetSpaceReport(string empname)
        {
            return await _querymirleCloudCore.GetSpaceReport(empname);
        }

        public async Task<IEnumerable<zp_get_sharetometop10_report_Result>> GetshareTop10Report(string empname)

        {
            return await _querymirleCloudCore.GetshareTop10Report(empname);
        }

        public async Task<IEnumerable<zp_get_top10_file_size_report_Result>> GetTop10FileSizeReport(string empname)

        {
            return await _querymirleCloudCore.GetTop10FileSizeReport(empname);

        }

        public async Task<IEnumerable<zp_get_recent_upload_report_Result>> GetRecentUploadReport(string empname)
        {
            return await _querymirleCloudCore.GetRecentUploadReport(empname);
        }

        //取得使用者權限
        public async Task<IEnumerable<zp_get_member_auth_detail_Result>> GetMemberAuthDetail(string empname,
            string abpath)
        {
            string rpath = Common.AbsoluteToRelative(Root,abpath);
            return await _querymirleCloudCore.GetMemberAuthDetail(empname,rpath);
        }

        public async Task<IEnumerable<zp_get_dashboard_get_files_Result>> GetFileDashboardDetail(
           string abpath)
        {
            string rpath = Common.AbsoluteToRelative(Root, abpath);
            return await _querymirleCloudCore.GetFileDashboardDetail(rpath);
        }

        public async Task<IEnumerable<zp_check_autoversion_Result>> CheckAutoVersion(
         string abpath)
        {
            string rpath = Common.AbsoluteToRelative(Root, abpath);
            return await _querymirleCloudCore.CheckAutoVersion(rpath);
        }

        public async Task<IEnumerable<zp_checkversion_give_filename_Result>> GiveFilename(
        string abpath, string filename)
        {
            string rpath = Common.AbsoluteToRelative(Root, abpath);
            return await _querymirleCloudCore.GiveFilename(rpath, filename);
        }

        public async Task<IEnumerable<zp_get_mangers_list_Result>> GetManagerList()
        {
            return await _querymirleCloudCore.GetManagerList();
        }


    }
}
