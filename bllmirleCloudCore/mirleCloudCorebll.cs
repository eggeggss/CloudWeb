﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using dalmirleCloudCore;
using dalmirleCloudCore.Common;
using dalmirleCloudCore.Meta;
using dalmirleCloudCore.Models;
using Microsoft.Extensions.Options;

namespace bllmirleCloudCore
{
    public class mirleCloudCorebll
    {

        private mirleCloudCoredal _dalmirleCloudCore;
        private IOptionsSnapshot<Profile> _profile;
        public string Root { get; set; }

        private mirleCloudCorequery _querymirleCloudCore;

        public mirleCloudCorebll(mirleCloudCoredal dalmirleCloudCoredal,
            IOptionsSnapshot<Profile> profile,
            mirleCloudCorequery querymirleCloudCore)
        {
            _dalmirleCloudCore = dalmirleCloudCoredal;
            _profile = profile;
            var data = _profile.Value;
            Root = data.Root;
            _querymirleCloudCore = querymirleCloudCore;
        }

        #region Document
        /* Document*/
        public async Task<Document> FindDocument(string abpath)
        {
            var rpath = Common.AbsoluteToRelative(Root, abpath);

            return await _dalmirleCloudCore.FindDocument(rpath);
        }


        public async Task<Document> CreateADocument(string abpath, string docname, string who, string empname)
        {
            var rpath = Common.AbsoluteToRelative(Root, abpath);

            var doc = await this.FindDocument(abpath);

            if (doc != null)
            {
                doc.UpdateBy = who;
                return await _dalmirleCloudCore.UpdateDocument(doc);
            }
            else
            {
                doc = new Document();
                doc.Docname = docname;
                doc.Relativepath = rpath;
                doc.Absolutepath = abpath;
                doc.CreateBy = who;
                var result = await _dalmirleCloudCore.CreateDocument(doc);

                return doc;
            }

        }

        public async Task<bool> DeleteADocument(string abpath, string who, string empname)
        {

            var rpth = Common.AbsoluteToRelative(Root, abpath);
            //get idDocument

            var doc = await this.FindDocument(abpath);

            if (doc != null)
            {
                var iddocument = doc.IdDocument;
                //delete File

                await this.DeleteFiles(iddocument, who);

                //delete AccoutDocument
                await this.DeleteAccountDocument(empname, iddocument, who);

                //delete DocumentAccount
                await this.DelDocumentAccount(empname, iddocument, who);

                await this.DelDocumentDepts(iddocument, who);

                //delete Document
                await this._dalmirleCloudCore.DelDocument(doc);
                //await this.DeleteADocument(abpath, who, empname);
            }

            return true;
        }


        public async Task<bool> UpdateDocumentAutoday(long iddoc, int autoday, string who)
        {
            return await _dalmirleCloudCore.UpdateDocumentAutoday(iddoc, autoday, who);
        }

        public async Task<bool> UpdateDocumentAutoversion(long iddoc, int autoversion, string who)
        {
            return await _dalmirleCloudCore.UpdateDocumentAutoversion(iddoc, autoversion, who);
        }


        /* Document*/
        #endregion


        #region DocumentAccount
        /*DocumentAccount*/
        public async Task<IEnumerable<DocumentAccount>> FindDocumentAccount(long idDocument)
        {
            var doct = await _dalmirleCloudCore.FindDocumentAccount(idDocument);

            return doct;
        }
        public async Task<IEnumerable<DocumentAccount>> FindDocumentAccounts(long idDocument, string ename)
        {
            var doct = await _dalmirleCloudCore.FindDocumentAccount(idDocument);

            return doct;
        }

        public async Task<IEnumerable<DocumentAccount>> FindDocumentAccount(long idDocument, string ename)
        {
            var doct = await _dalmirleCloudCore.FindDocumentAccount(ename, idDocument);
            //var doct = await _dalmirleCloudCore.FindDocumentAccount(idDocument);

            return doct;
        }

        public async Task<IEnumerable<DocumentAccount>> FindDocumentAccount(string ename)
        {
            var doct = await _dalmirleCloudCore.FindDocumentAccount(ename);

            return doct;
        }

        public async Task<bool> CreateDocumentAccounts(List<member> members, long id_document, string who)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required,
              TransactionScopeAsyncFlowOption.Enabled))
            {


                foreach (var item in members)
                {
                    await this.CreateDocumentAccount(item.name, id_document, who);
                }

                scope.Complete();

            }

            return true;
        }

        //share to
        public async Task<bool> CreateDocumentAccount(string ename, long id_document, string who)
        {
            var doc = await this.FindDocumentAccount(id_document, ename);
            var docnew = doc.FirstOrDefault();

            if (docnew == null)
            {
                DocumentAccount doct = new DocumentAccount()
                {
                    IdDocument = id_document,
                    Empname = ename,
                    CreateBy = who,
                    StatVoid = 0,
                };

                await _dalmirleCloudCore.CreateDocumentAccount1(doct);
            }

            return true;
        }

        //刪除分享
        public async Task<bool> DelDocumentAccount(string ename, long id_document, string who)
        {
            var result = await this.FindDocumentAccounts(id_document, ename);

            foreach (var item in result)
            {
                item.UpdateBy = who;
                await _dalmirleCloudCore.DelDocumentAccount(item);
            }
            return true;
        }


        public async Task<bool> DelDocumentAccount(long id_documentaccount, string who)
        {
            var result = await _dalmirleCloudCore.FindDocumentAccountBypk(id_documentaccount);

            await _dalmirleCloudCore.DelDocumentAccount(result);

            return true;
        }


        public async Task<bool> UpdateMemberAllowUpload(long iddocacc, int allow_update,string who)
        {
            return await _dalmirleCloudCore.UpdateMemberAllowUpload(iddocacc,allow_update,who);
        }

        public async Task<bool> UpdateMemberAllowDownload(long iddocacc, int allow_download,string who)
        {
            return await _dalmirleCloudCore.UpdateMemberAllowDownload(iddocacc,allow_download,who);
        }

        public async Task<bool> UpdateMemberAllowFolder(long iddocacc, int allow_folder,string who)
        {
            return await _dalmirleCloudCore.UpdateMemberAllowFolder(iddocacc,allow_folder,who);
        }

        public async Task<bool> UpdateMemberAllowDelFolder(long iddocacc, int allow_delfolder,string who)
        {
            return await _dalmirleCloudCore.UpdateMemberAllowDelFolder(iddocacc,allow_delfolder,who);
        }

        public async Task<bool> UpdateMemberAllowDelFile(long iddocacc, int allow_delfile,string who)
        {
            return await _dalmirleCloudCore.UpdateMemberAllowDelFile(iddocacc, allow_delfile,who);
        }

        public async Task<bool> UpdateMemberAllowShare(long iddocacc, int allow_share,string who)
        {
            return await _dalmirleCloudCore.UpdateMemberAllowShare(iddocacc,allow_share,who);
        }


        /*DocumentAccount*/
        #endregion



        #region AccountDocument
        /*AcountDocument*/
        public async Task<AccountDocument> FindAccountDocument(long idDocument, string ename)
        {
            var doct = await _dalmirleCloudCore.FindAccountDocument(idDocument, ename);

            return doct;
        }

        public async Task<AccountDocument> CreateAccountDocument(string ename, long ldDocument, string who)
        {
            var obj = await _dalmirleCloudCore.FindAccountDocument(ldDocument, ename);

            if (obj == null)
            {
                AccountDocument doct = new AccountDocument
                {
                    Empname = ename,
                    IdDocument = ldDocument,
                    CreateBy = who
                };

                await _dalmirleCloudCore.CreateAccountDocument(doct);
            }
            else
            {
                obj.UpdateBy = who;

                await _dalmirleCloudCore.UpdateAccountDocument(obj);

            }


            return obj;
        }

        public async Task<bool> DeleteAccountDocument(string ename, long ldDocument, string who)
        {
            var doct = await _dalmirleCloudCore.FindAccountDocument(ldDocument, ename);

            if (doct != null)
            {
                doct.UpdateBy = who;
                await _dalmirleCloudCore.DelAccountDocument(doct);

            }
            return true;
        }

        /*AcountDocument*/
        #endregion


        #region File
        /*File*/
        public async Task<File> FindFile(string abpath)
        {
            var rpath = Common.AbsoluteToRelative(Root, abpath);
            return await _dalmirleCloudCore.FindFile(rpath);
        }

        public async Task<File> FindFile(long idfile)
        {
            return await _dalmirleCloudCore.FindFile(idfile);
        }

        public async Task<IEnumerable<File>> FindFiles(long idDocument)
        {
            return await _dalmirleCloudCore.FindFiles(idDocument);
        }

        public async Task<long> CreateFile(
            long idDocument,
            string filename, string abpath,
            string filesize,
            string who,string keyword)
        {
            var rpath = Common.AbsoluteToRelative(Root, abpath);

            var file = await _dalmirleCloudCore.FindFile(rpath);

            if (file == null)
            {
                file = new File
                {
                    IdDocument = idDocument,
                    Filename = filename,
                    Absolutepath = abpath,
                    Relativepath = rpath,
                    Filesize = filesize,
                    CreateBy = who,
                    Comment=keyword,
                };

                return await _dalmirleCloudCore.CreateFile(file);
            }
            else
            {
                file.UpdateBy = who;

                if (!String.IsNullOrEmpty(keyword))
                {
                    file.Comment = keyword;
                }
                return await _dalmirleCloudCore.UpdateFile(file);
            }

        }

        public async Task<long> UpdateFile(File file)
        {
            return await _dalmirleCloudCore.UpdateFile(file);
        }


        public async Task<bool> DeleteFile(string abpath, string who)
        {
            var rpath = Common.AbsoluteToRelative(Root, abpath);
            var file = await _dalmirleCloudCore.FindFile(rpath);
            if (file != null)
            {
                file.UpdateBy = who;
                await _dalmirleCloudCore.DelFile(file);
            }

            return true;
        }

        public async Task<bool> DeleteFiles(long idDocument, string who)
        {
            var files = await _dalmirleCloudCore.FindFiles(idDocument);

            foreach (var file in files)
            {
                file.UpdateBy = who;
                await _dalmirleCloudCore.DelFile(file);
            }
            return true;
        }

        /*File*/
        #endregion



        /******DocumentDept********/

        public async Task<DocumentDept> FindDocumentDept(long iddocumentdept)
        {
            return await _dalmirleCloudCore.FindDocumentDept(iddocumentdept);
        }

        public async Task<IEnumerable<DocumentDept>> FindDocumentDepts(long iddocument)
        {
            return await _dalmirleCloudCore.FindDocumentDepts(iddocument);

        }

        //share to deptno
        public async Task<bool> CreateDocumentDept(string deptno, long id_document, string who)
        {
            var doc = await _dalmirleCloudCore.FindDocumentDept(deptno,id_document);

            if (doc == null)
            {
                DocumentDept doct = new DocumentDept()
                {
                    IdDocument=id_document,
                    DeptNo= deptno,
                    StatVoid=0,
                    CreateBy=who,
                    
                };

                return await _dalmirleCloudCore.CreateDocumentDept(doct);
            }

            return true;

        }


        public async Task<bool> CreateDocumentDepts(List<deptno> deptnos, long id_document, string who)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required,
              TransactionScopeAsyncFlowOption.Enabled))
            {


                foreach (var item in deptnos)
                {
                    await this.CreateDocumentDept(item.name, id_document, who);
                }

                scope.Complete();

            }

            return true;
        }


        public async Task<bool> UpdateDocumentDept(DocumentDept doct)
        {
            return await _dalmirleCloudCore.UpdateDocumentDept(doct);
        }

        //刪除分享
        public async Task<bool> DelDocumentDept(long id_docmentdept,string ename)
        {
            var doct = await _dalmirleCloudCore.FindDocumentDept(id_docmentdept);

            if (doct != null)
            {
                return await _dalmirleCloudCore.DelDocumentDept(doct);

            }

            return true;
        }

        //刪除分享s
        public async Task<bool> DelDocumentDepts(long id_document, string who)
        {
            var result = await this.FindDocumentDepts(id_document);

            foreach (var item in result)
            {
                item.UpdateBy = who;
                await _dalmirleCloudCore.DelDocumentDept(item);
            }
            return true;
        }



        public async Task<bool> UpdateDeptAllowUpload(long iddocdept, int allow_upload, string who)
        {
            return await _dalmirleCloudCore.UpdateDeptAllowUpload(iddocdept, allow_upload, who);
        }


        public async Task<bool> UpdateDeptAllowDownload(long iddocdept, int allow_download, string who)
        {
            return await _dalmirleCloudCore.UpdateDeptAllowDownload(iddocdept, allow_download, who);
        }

        public async Task<bool> UpdateDeptAllowFolder(long iddocdept, int allow_folder, string who)
        {
            return await _dalmirleCloudCore.UpdateDeptAllowFolder(iddocdept, allow_folder, who);
        }

        public async Task<bool> UpdateDeptAllowDelFolder(long iddocdept, int allow_delfolder, string who)
        {
            return await _dalmirleCloudCore.UpdateDeptAllowDelFolder(iddocdept, allow_delfolder, who);
        }

        public async Task<bool> UpdateDeptAllowDelFile(long iddocdept, int allow_delfile, string who)
        {
            return await _dalmirleCloudCore.UpdateDeptAllowDelFile(iddocdept, allow_delfile, who);
        }


        public async Task<bool> UpdateDeptAllowShare(long iddocdept, int allow_share, string who)
        {
            return await _dalmirleCloudCore.UpdateDeptAllowShare(iddocdept, allow_share, who);
        }

        public async Task<IEnumerable<zp_rename_directory_Result>> RenameDirectory(
           string rpathold,
           string rpathnew,
           string newname,
           string update_by)
        {
            return await _dalmirleCloudCore.RenameDirectory(rpathold, rpathnew, newname, update_by);
        }

        public async Task<IEnumerable<zp_remove_directory_Result>> RemoveDirectory(
            long id_Document,
            string abpathold,
            string abpathnew,
            string update_by)
        {

            var rpathold= Common.AbsoluteToRelative(Root, abpathold);

            var rpathnew = Common.AbsoluteToRelative(Root, abpathnew);

            return await _dalmirleCloudCore.RemoveDirectory(id_Document, rpathold, rpathnew, update_by);
        }


        public async Task<UserAccount> FindUserAccount(string empname)
        {
            return await _dalmirleCloudCore.FindUserAccount(empname);
        }


        public async Task<bool> UpdateIsManagers(IList<member> members, string update_by)
        {
            foreach (var item in members)
            {
                var member = await this.FindUserAccount(item.name);

                if (member != null)
                {
                    if (member.IsManager == 1)
                    {
                        continue;
                    }

                    member.UpdateBy = update_by;
                    member.IsManager = 1;

                    await _dalmirleCloudCore.UpdateUserAccount(member);
                }
                else
                {
                    member = new UserAccount
                    {
                        Empname = item.name,
                        IsManager = 1,
                    };

                    await _dalmirleCloudCore.InsertUserAccount(member);
                }

            }
            return true;
        }

        public async Task<bool> DeleteManager(long id_UserAccount, string update_by)
        {
            var user = await _dalmirleCloudCore.FindUserAccount(id_UserAccount);

            if (user != null)
            {
                user.IsManager = 0;
                user.UpdateBy = update_by;

                await _dalmirleCloudCore.UpdateUserAccount(user);
            }
            return true;
        }

    }
}
