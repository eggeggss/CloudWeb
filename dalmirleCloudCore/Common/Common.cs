﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net.Http;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace dalmirleCloudCore.Common
{
    public class Common
    {
//\\Users\\rogerroan\\share\\cloudweb\\Cloudweb\\mirleCloudCore\\wwwroot\\"
//\\Users\\rogerroan\\share\\cloudweb\\CloudWeb\\mirleCloudCore\\wwwroot\\share\\lin\\clouddata\\abcde"
        public static string AbsoluteToRelative(string Root, string absolute)
        {
            Root = Root.Replace("/", "\\");
            absolute = absolute.Replace("/", "\\");
            var result= absolute.Replace(Root, "").Replace("\\","/");
            return result;
        }

        public static string RelativeToAbsolute(string Root, string relative)
        {
            return System.IO.Path.Combine(Root, relative);
        }

        public static void SendLine(string mymessage)
        {

            string _notifyUrl = "https://notify-api.line.me/api/notify";
            string token = "89woZCO67LWGmutJqckCJ2VxViBv9EsTSCtcW2jR17A";

            string message = mymessage;


            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(_notifyUrl);
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);

                var form = new FormUrlEncodedContent(new[]
                {
                           new KeyValuePair<string, string>("message", message)
                    });

                var result = client.PostAsync("", form).Result;

                Console.WriteLine(result.StatusCode);
            }

        }

        public static DateTime GetNow()
        {
#if DEBUG
            var today = DateTime.Now;
#else
            var today=DateTime.UtcNow.AddHours(8);
#endif
            return today;
        }

        public static string Compress(string text)
        {
            if (string.IsNullOrEmpty(text)) { return text; }

            byte[] buffer = Encoding.UTF8.GetBytes(text);

            using (var outStream = new MemoryStream())
            using (var zip = new GZipStream(outStream, CompressionMode.Compress))
            {
                zip.Write(buffer, 0, buffer.Length);
                zip.Close();

                string compressedBase64 = Convert.ToBase64String(outStream.ToArray());
                return compressedBase64;
            }
        }


        /*解壓縮*/
        public static string Decompress(string compressed)
        {
            if (string.IsNullOrEmpty(compressed)) { return compressed; }

            byte[] buffer = Convert.FromBase64String(compressed);

            using (var inStream = new MemoryStream(buffer))
            using (var outStream = new MemoryStream())
            using (var zip = new GZipStream(inStream, CompressionMode.Decompress))
            {
                zip.CopyTo(outStream);
                zip.Close();

                string text = Encoding.UTF8.GetString(outStream.ToArray());
                return text;
            }
        }

        public static byte[] ObjectToByteArray(object obj)
        {
            var binaryFormatter = new BinaryFormatter();
            using (var memoryStream = new MemoryStream())
            {
                binaryFormatter.Serialize(memoryStream, obj);
                return memoryStream.ToArray();
            }
        }

        public static T ByteArrayToObject<T>(byte[] bytes)
        {
            using (var memoryStream = new MemoryStream())
            {
                var binaryFormatter = new BinaryFormatter();
                memoryStream.Write(bytes, 0, bytes.Length);
                memoryStream.Seek(0, SeekOrigin.Begin);

                var obj = binaryFormatter.Deserialize(memoryStream);


                return (T)obj;
            }
        }

    }
}
