﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using dalmirleCloudCore.Meta;
using dalmirleCloudCore.Models;
using Microsoft.EntityFrameworkCore;

namespace dalmirleCloudCore.Common
{
    public static class DbSPContextExtention
    {

        public async static Task<IEnumerable<zp_remove_directory_Result>>
            zp_remove_directory_Result(this clouddbContext context,
            long id_Document,
            string rpathold,
            string rpathnew,
            string update_by)
        {

            var result = await
              context.zp_remove_directory_Result
              .FromSqlInterpolated($@"execute dbo.zp_remove_directory {id_Document},{rpathold},{rpathnew},{update_by}
               ").ToListAsync();

            return result;
        }

        public async static Task<IEnumerable<zp_rename_directory_Result>>
            zp_rename_directory_Result(this clouddbContext context,
            string rpathold,
            string rpathnew,
            string newname,
            string update_by)
        {

            var result = await
              context.zp_rename_directory_Result
              .FromSqlInterpolated($@"execute dbo.zp_rename_directory {rpathold},{rpathnew},{newname},{update_by}
               ").ToListAsync();

            return result;
        }


        public async static Task<IEnumerable<zp_get_mangers_list_Result>> 
            zp_get_mangers_list_Result(this clouddbContext context)
        {

            var result = await
              context.zp_get_mangers_list_Result
              .FromSqlInterpolated($@"execute dbo.zp_get_mangers_list
               ").ToListAsync();

            return result;
        }


        public async static Task<IEnumerable<zp_checkversion_give_filename_Result>> zp_checkversion_give_filename_Result(this clouddbContext context,
          string rpath,string filename)
        {

            var result = await
              context.zp_checkversion_give_filename_Result
              .FromSqlInterpolated($@"execute dbo.zp_checkversion_give_filename {rpath},{filename}
               ").ToListAsync();

            return result;
        }


        public async static Task<IEnumerable<zp_check_autoversion_Result>> zp_check_autoversion_Result(this clouddbContext context,
          string rpath)
        {

            var result = await
              context.zp_check_autoversion_Result
              .FromSqlInterpolated($@"execute dbo.zp_check_autoversion {rpath}
               ").ToListAsync();

            return result;
        }


        public async static Task<IEnumerable<zp_get_dashboard_get_files_Result>> zp_get_dashboard_get_files_Result(this clouddbContext context,
          string rpath)
        {

            var result = await
              context.zp_get_dashboard_get_files_Result
              .FromSqlInterpolated($@"execute dbo.zp_get_dashboard_get_files {rpath}
               ").ToListAsync();

            return result;
        }



        public async static Task<IEnumerable<zp_get_sharetome_list_for_dashboard_Result>>  zp_get_sharetome_list_for_dashboard_Result(this clouddbContext context,
          string empname, int top)
        {

            var result = await
              context.zp_get_sharetome_list_for_dashboard_Result
              .FromSqlInterpolated($@"execute dbo.zp_get_sharetome_list_for_dashboard {empname},{top}
               ").ToListAsync();

            return result;
        }



        public async static Task<IEnumerable<zp_get_sharetome_list_manager_for_dashboard_Result>> zp_get_sharetome_list_manager_for_dashboard_Result(this clouddbContext context,
          string empname, int top)
        {

            var result = await
              context.zp_get_sharetome_list_manager_for_dashboard_Result
              .FromSqlInterpolated($@"execute dbo.zp_get_sharetome_list_manager_for_dashboard {empname},{top}
               ").ToListAsync();

            return result;
        }


        public async static Task<IEnumerable<zp_get_member_auth_detail_Result>> zp_get_member_auth_detail_Result(this clouddbContext context,
           string empname,string rpath)
        {

            var result = await
              context.zp_get_member_auth_detail_Result
              .FromSqlInterpolated($@"execute dbo.zp_get_member_auth_detail {empname},{rpath}
               ").ToListAsync();

            return result;
        }


        public async static Task<IEnumerable<zp_get_recent_upload_report_Result>> zp_get_recent_upload_report_Result(this clouddbContext context,
           string empname)
        {

            var result = await
              context.zp_get_recent_upload_report_Result
              .FromSqlInterpolated($@"execute dbo.zp_get_recent_upload_report {empname}
               ").ToListAsync();

            return result;
        }


        public async static Task<IEnumerable<zp_get_top10_file_size_report_Result>> zp_get_top10_file_size_report_Result(this clouddbContext context,
           string empname)
        {

            var result = await
              context.zp_get_top10_file_size_report_Result
              .FromSqlInterpolated($@"execute dbo.zp_get_top10_file_size_report {empname}
               ").ToListAsync();

            return result;
        }


        public async static Task<IEnumerable<zp_get_sharetometop10_report_Result>> zp_get_sharetometop10_report_Result(this clouddbContext context,
           string empname)
        {

            var result = await
              context.zp_get_sharetometop10_report_Result
              .FromSqlInterpolated($@"execute dbo.zp_get_sharetometop10_report {empname}
               ").ToListAsync();

            return result;
        }


        public async static Task<IEnumerable<zp_get_disk_space_report_Result>> zp_get_disk_space_report_Result(this clouddbContext context,
           string empname)
        {

            var result = await
              context.zp_get_disk_space_report_Result
              .FromSqlInterpolated($@"execute dbo.zp_get_disk_space_report {empname}
               ").ToListAsync();

            return result;
        }

        public async static Task<IEnumerable<zp_get_file_datatable_Result>> zp_get_file_datatable_Result(this clouddbContext context,
           string empname)
        {

            var result = await
              context.zp_get_file_datatable_Result
              .FromSqlInterpolated($@"execute dbo.zp_get_file_datatable {empname}
               ").ToListAsync();

            return result;
        }



        public async static Task<IEnumerable<zp_get_doc_size_report_Result>> zp_get_doc_size_report_Result(this clouddbContext context,
           string empname)
        {

            var result = await
              context.zp_get_doc_size_report_Result
              .FromSqlInterpolated($@"execute dbo.zp_get_doc_size_report {empname}
               ").ToListAsync();

            return result;
        }

        public async static Task<IEnumerable<zp_get_sharetome_list_Result>> zp_get_sharetome_list_Result(this clouddbContext context,
           string empname)
        {

            var result = await
              context.zp_get_sharetome_list_Result
              .FromSqlInterpolated($@"execute dbo.zp_get_sharetome_list {empname}
               ").ToListAsync();

            return result;
        }

        public async static Task<IEnumerable<zp_share_to_other_list_Result>> zp_share_to_other_list_Result(this clouddbContext context,
            string empname)
        {

            var result = await
              context.zp_share_to_other_list_Result
              .FromSqlInterpolated($@"execute dbo.zp_share_to_other_list {empname}
               ").ToListAsync();

            return result;
        }


        public async static Task<IEnumerable<zp_inital_user_Result>> zp_inital_user_Result(this clouddbContext context,
            string empname)
        {

            var result = await
              context.zp_inital_user_Result
              .FromSqlInterpolated($@"execute dbo.zp_inital_user {empname}
               ").ToListAsync();

            return result;
        }

        public async static Task<IEnumerable<zp_get_share_folder_user_list_Result>> zp_get_share_folder_user_list_Result(this clouddbContext context,
            string rpath)
        {

            var result = await
              context.zp_get_share_folder_user_list_Result
              .FromSqlInterpolated($@"execute dbo.zp_get_share_folder_user_list {rpath}
               ").ToListAsync();

            return result;
        }


        public async static Task<IEnumerable<zp_get_share_folder_dept_list_Result>> zp_get_share_folder_dept_list_Result(this clouddbContext context,
            string rpath)
        {

            var result = await
              context.zp_get_share_folder_dept_list_Result
              .FromSqlInterpolated($@"execute dbo.zp_get_share_folder_dept_list {rpath}
               ").ToListAsync();

            return result;
        }


    }
}
