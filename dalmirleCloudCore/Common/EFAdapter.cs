﻿using dalmirleCloudCore.Meta;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dalmirleCloudCore.Common
{
    public class EFAdapter
    {
        public string Location;

        private ILogger<EFAdapter> _logger;
        public String DbString
        {
            get
            {
                if (Location.Equals("Test"))
                {
                    return Setting.TestSsl;
                }
                else
                {

                    return Setting.ProductSql;
                }
            }
        }

        public Profile Profile { get; set; }
        public EFAdapter(ILogger<EFAdapter> logger, IOptionsSnapshot<Profile> profile)
        {
            //_location = Location;
            _logger = logger;
            Profile = profile.Value;
            this.Location = Profile.Site;

        }

        public T Catch<T>(Func<T> func)
        {
            try
            {
                return func.Invoke();
            }
            catch (Exception ex)
            {
                this._logger.LogError("EF adapter catch Message:{ex.Message}", ex.Message);
                //System.Diagnostics.EventLog.WriteEntry("DBDev", $"EF adapter catch Message:{ex.Message}", System.Diagnostics.EventLogEntryType.Error);

                throw new Exception("db fail : " + ex.Message);
            }
        }


        public async Task<T> CatchAsync<T>(Func<T> func)
        {
            try
            {
                var result=await Task.Run(() =>
                {
                    return func.Invoke();

                });

                return result;
                
            }
            catch (Exception ex)
            {
                this._logger.LogError("EF adapter catch async Message:{ex.Message}", ex.Message);
                //System.Diagnostics.EventLog.WriteEntry("DBDev", $"EF adapter catch Message:{ex.Message}", System.Diagnostics.EventLogEntryType.Error);

                throw new Exception("db fail : " + ex.Message);
            }
        }
    }
}
