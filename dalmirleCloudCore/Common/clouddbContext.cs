﻿using dalmirleCloudCore.Meta;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dalmirleCloudCore.Models
{
    public partial class clouddbContext : DbContext
    {
        //public virtual DbSet<zp_get_playlist_Result> zp_get_playlist_Result { get; set; }

        public virtual DbSet<zp_inital_user_Result> zp_inital_user_Result { get; set; }

        public virtual DbSet<zp_get_share_folder_user_list_Result> zp_get_share_folder_user_list_Result { get; set; }

        public virtual DbSet<zp_share_to_other_list_Result> zp_share_to_other_list_Result { get; set; }

        public virtual DbSet<zp_get_sharetome_list_Result> zp_get_sharetome_list_Result { get; set; }

        public virtual DbSet<zp_get_doc_size_report_Result> zp_get_doc_size_report_Result { get; set; }

        public virtual DbSet<zp_get_file_datatable_Result> zp_get_file_datatable_Result { get; set; }

        public virtual DbSet<zp_get_share_folder_dept_list_Result> zp_get_share_folder_dept_list_Result { get; set; }

        public virtual DbSet<zp_get_disk_space_report_Result> zp_get_disk_space_report_Result { get; set; }

        public virtual DbSet<zp_get_sharetometop10_report_Result> zp_get_sharetometop10_report_Result { get; set; }

        public virtual DbSet<zp_get_top10_file_size_report_Result> zp_get_top10_file_size_report_Result { get; set; }

        public virtual DbSet<zp_get_recent_upload_report_Result> zp_get_recent_upload_report_Result { get; set; }

        public virtual DbSet<zp_get_member_auth_detail_Result> zp_get_member_auth_detail_Result { get; set; }

        public virtual DbSet<zp_get_sharetome_list_for_dashboard_Result> zp_get_sharetome_list_for_dashboard_Result { get; set; }

        public virtual DbSet<zp_get_dashboard_get_files_Result> zp_get_dashboard_get_files_Result { get; set; }

        public virtual DbSet<zp_get_sharetome_list_manager_for_dashboard_Result> zp_get_sharetome_list_manager_for_dashboard_Result { get; set; }

        public virtual DbSet<zp_check_autoversion_Result> zp_check_autoversion_Result { get; set; }

        public virtual DbSet<zp_checkversion_give_filename_Result> zp_checkversion_give_filename_Result { get; set; }

        public virtual DbSet<zp_get_mangers_list_Result> zp_get_mangers_list_Result { get; set; }

        public virtual DbSet<zp_rename_directory_Result> zp_rename_directory_Result { get; set; }


        public virtual DbSet<zp_remove_directory_Result> zp_remove_directory_Result { get; set; }


        //zp_remove_directory_Result
        //zp_rename_directory_Result
        //zp_check_autoversion_Result
        //zp_get_sharetome_list_manager_for_dashboard_Result
        //zp_get_dashboard_get_files_Result
        //zp_get_sharetome_list_for_dashboard_Result
        //zp_get_member_auth_detail_Result
        //zp_get_recent_upload_report_Result
        //zp_get_top10_file_size_report_Result
        //zp_get_sharetometop10_report_Result
        //zp_get_disk_space_report_Result

        private void OnModelCreatingExtent(ModelBuilder modelBuilder)
        {

            //stored procedure
            //modelBuilder.Entity<zp_get_playlist_Result>().HasNoKey();
            modelBuilder.Entity<zp_inital_user_Result>().HasNoKey();

            modelBuilder.Entity<zp_get_share_folder_user_list_Result>().HasNoKey();

            modelBuilder.Entity<zp_share_to_other_list_Result>().HasNoKey();

            modelBuilder.Entity<zp_get_sharetome_list_Result>().HasNoKey();

            modelBuilder.Entity<zp_get_doc_size_report_Result>().HasNoKey();

            modelBuilder.Entity<zp_get_file_datatable_Result>().HasNoKey();

            modelBuilder.Entity<zp_get_share_folder_dept_list_Result>().HasNoKey();

            modelBuilder.Entity<zp_get_disk_space_report_Result>().HasNoKey();

            modelBuilder.Entity<zp_get_sharetometop10_report_Result>().HasNoKey();

            modelBuilder.Entity<zp_get_top10_file_size_report_Result>().HasNoKey();

            modelBuilder.Entity<zp_get_recent_upload_report_Result>().HasNoKey();

            modelBuilder.Entity<zp_get_member_auth_detail_Result>().HasNoKey();

            modelBuilder.Entity<zp_get_sharetome_list_for_dashboard_Result>().HasNoKey();

            modelBuilder.Entity<zp_get_dashboard_get_files_Result>().HasNoKey();

            modelBuilder.Entity<zp_get_sharetome_list_manager_for_dashboard_Result>().HasNoKey();

            modelBuilder.Entity<zp_check_autoversion_Result>().HasNoKey();

            modelBuilder.Entity<zp_checkversion_give_filename_Result>().HasNoKey();

            modelBuilder.Entity<zp_get_mangers_list_Result>().HasNoKey();

            modelBuilder.Entity<zp_rename_directory_Result>().HasNoKey();

            modelBuilder.Entity<zp_remove_directory_Result>().HasNoKey();

            //zp_remove_directory_Result
            //zp_check_autoversion_Result
            //zp_get_sharetome_list_manager_for_dashboard_Result
            //zp_get_dashboard_get_files_Result
            //zp_get_sharetome_list_for_dashboard_Result
            //zp_get_member_auth_detail_Result
            //zp_get_recent_upload_report_Result
            //zp_get_top10_file_size_report_Result
            //zp_get_sharetometop10_report_Result
            //zp_get_disk_space_report_Result
            //zp_get_share_folder_dept_list_Result
            //zp_get_file_datatable_Result
            //zp_get_doc_size_report_Result
            //zp_copy_project_Result

        }
    }
}
