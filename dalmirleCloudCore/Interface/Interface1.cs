﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dalmirleCloudCore.Interface
{
    public interface IODirectoryInterface
    {
        Task<bool> CreateDiretory(string path,string who);
        Task<bool> DeleteDirectory(string path,string who);
        Task<bool> MoveDirectory(string source, string dest,string who);

        Task<bool> CopyDirectory(string source, string dest,string who);
    }

    public interface IOFileInterface
    {
        Task<bool> DeleteFile(string path,string who);
        Task<bool> Movefile(string source, string dest,string who);
        Task<bool> Copyfile(string source, string dest,string who);

        Task<bool> CreateFile(string abpath, string who,string keyword);

    }

}
