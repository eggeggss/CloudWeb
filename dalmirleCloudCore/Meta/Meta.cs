﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dalmirleCloudCore.Meta
{

    public class zp_remove_directory_Result
    {
        public string result { get; set; }
    }


    public class zp_rename_directory_Result
    {
        public string result { get; set; }
    }

    public class zp_get_mangers_list_Result
    {
        public string emp_cname { get; set; }
        public string emp_ename { get; set; }
        public string emp_no { get; set; }

        public DateTime dt_create { get; set; }

        public long id_UserAccount { get; set; }
    }
    public class zp_checkversion_give_filename_Result
    {
        public string filename { get; set; }
    }


    public class zp_check_autoversion_Result
    {
        public int result { get; set; }
    }


    public class zp_get_dashboard_get_files_Result
    {
        public string filename { get; set; }
        public string create_by { get; set; }

        public DateTime dt_create { get; set; }

        public string absolutepath { get; set; }
    }




     public class zp_get_member_auth_detail_Result
    {
        public long id_Document { get; set; }
        public string empname { get; set; }

        public string dept_no { get; set; }
        public string create_by { get; set; }
        public string docname { get; set; }

        public string relativepath { get; set; }

        public int allow_upload { get; set; }

        public int allow_download  { get; set; }

        public int allow_createfolder { get; set; }

        public int allow_delfolder { get; set; }

        public int allow_delfile { get; set; }

        public int allow_share { get; set; }

        public DateTime dt_create { get; set; }

    }


    public class zp_get_recent_upload_report_Result
    {
        public string docname { get; set; }
        public string filename { get; set; }
        public string filesize { get; set; }
        public string absolutepath { get; set; }
        public string relativepath { get; set; }
        public DateTime dt_create { get; set; }
    }

    public class zp_get_top10_file_size_report_Result
    {
        public string docname { get; set; }
        public string filename { get; set; }
        public string filesize { get; set; }
        public string absolutepath { get; set; }
        public string relativepath { get; set; }
        public DateTime dt_create { get; set; }

    }

           

    public class zp_get_sharetometop10_report_Result
    {
        public string docname { get; set; }

        public string filename { get; set; }

        public string filesize { get; set; }

        public DateTime dt_create { get; set; }

        public string create_by { get; set; }

        public string absolutepath { get; set; }

        public string relativepath { get; set; }
        
    }

    public class zp_get_disk_space_report_Result
    {
        public string used { get; set; }
        public string percen { get; set; }
        public int maxlimit { get; set; }
    }

    public class zp_get_share_folder_dept_list_Result
    {
        public long id_DocumentDept { get; set; }
        public long id_Document { get; set; }

        public string dept_no { get; set; }

        public string dept_name { get; set; }

        public int allow_upload { get; set; }

        public int allow_download { get; set; }

        public int allow_createfolder { get; set; }
        public int allow_delfolder { get; set; }
        public int allow_delfile { get; set; }

        public int allow_share { get; set; }

    }
    public class aduserResult
    {
        public string empno { get; set; }
        public string empname { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public string osver { get; set; }
        public float appver { get; set; }
        public string device { get; set; }
        public int haslogined { get; set; }
        public string token { get; set; }
        public int company { get; set; }
    }

    public class zp_get_file_datatable_Result
    {
        public string filename { get; set; }
        public string docname { get; set; }
        public decimal filesize { get; set; }
        public string relativepath { get; set; }
        public string absolutepath { get; set; }
        public string create_by { get; set; }
        public string empname { get; set; }
        public DateTime dt_create { get; set; }

        public string category  { get; set; }

        public string keyword { get; set; }
    }


    public class zp_get_doc_size_report_Result
    {
        public string docname { get; set; }
        public string relativepath { get; set; }
        public long filesize { get; set; }
        public long percen { get; set; }
    }


    public class zp_get_sharetome_list_Result
    {
        public long id_Document { get; set; }

        public string empname { get; set; }
        public string dept_no { get; set; }

        public string docname { get; set; }
        public string relativepath { get; set; }

        public string absolutepath { get; set; }

        public string create_by { get; set; }

        public string docname2 { get; set; }
    }



    public class zp_get_sharetome_list_for_dashboard_Result
    {
        public long id_Document { get; set; }

        public string empname { get; set; }
        public string dept_no { get; set; }



        public string docname { get; set; }
        public string relativepath { get; set; }

        public string absolutepath { get; set; }

        public string create_by { get; set; }

        public int limit { get; set; }

        public DateTime dt_create { get; set; }

        public string docname2 { get; set; }

        public string abpath { get; set; }
    }




    public class zp_get_sharetome_list_manager_for_dashboard_Result
    {
        public long id_Document { get; set; }

        public string empname { get; set; }
        public string dept_no { get; set; }



        public string docname { get; set; }
        public string relativepath { get; set; }

        public string absolutepath { get; set; }

        public string create_by { get; set; }

        public int limit { get; set; }

        public DateTime dt_create { get; set; }

        public string docname2 { get; set; }

        public string abpath { get; set; }

        public int autoversion { get; set; }
    }


    public class zp_share_to_other_list_Result
    {
        public long id_Document { get; set; }

        public string docname { get; set; }
        public string relativepath { get; set; }

        public string absolutepath { get; set; }

        public string docname2 { get; set; }
    }

    public class member
    {
        public string name { get; set; }
    }


    public class deptno
    {
        public string name { get; set; }
    }

    public class setting
    {

    }

    public class zp_get_share_folder_user_list_Result
    {
        public long id_DocumentAccount { get; set; }

        public long id_Document { get; set; }

        public string emp_cname { get; set; }

        public string emp_ename { get; set; }

        public string emp_no { get; set; }

        public int allow_upload { get; set; }

        public int allow_download { get; set; }

        public int allow_createfolder { get; set; }
        public int allow_delfolder { get; set; }
        public int allow_delfile { get; set; }

        public int allow_share { get; set; }

    }
    public class zp_inital_user_Result
    {
        public string result { get; set; }
    }

    public class aduser
    {
        public string password { get; set; }

        public string empname { get; set; }

        public string cname { get; set; }
        public string empno { get; set; }

        public string email { get; set; }

    }
    public class LoginRole
    {
        public string role { get; set; }
        public string username { get; set; }
        public string password { get; set; }

        public bool isremember { get; set; }

        public string hint { get; set; }
    }

    public enum Filetype
    {
        Directory,
        File
    }
    public class DirectoryFile
    {
        public string Name { get; set; }
        public string Path { get; set; }

        public string Size { set; get; }

        public DateTime CreateTime { set; get; }

        public DateTime UpdateTime { set; get; }

        public Filetype Filetype { set; get; }

        public string  Createby { get; set; }
        public string Extension { get; set; }


    }

    public class FileData
    {
        public string id { get; set; }
        public string name { get; set; }

        public icon icons { get; set; }

        public List<FileData> children { set; get; }

        public List<FileData> parent { set; get; }
        public string path { get; set; }

        public string drivetype { set; get; }
    }

    public class icon
    {
        [JsonProperty("default")]
        public List<string> defaults { get; set; }
    }
}
