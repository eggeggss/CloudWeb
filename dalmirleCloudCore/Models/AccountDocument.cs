﻿using System;
using System.Collections.Generic;

#nullable disable

namespace dalmirleCloudCore.Models
{
    public partial class AccountDocument
    {
        public long IdAccountDocument { get; set; }
        public string Empname { get; set; }
        public long? IdDocument { get; set; }
        public DateTime? DtCreate { get; set; }
        public DateTime? DtUpdate { get; set; }
        public string UpdateBy { get; set; }
        public string CreateBy { get; set; }
        public int? StatVoid { get; set; }
    }
}
