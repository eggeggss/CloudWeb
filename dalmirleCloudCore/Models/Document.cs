﻿using System;
using System.Collections.Generic;

#nullable disable

namespace dalmirleCloudCore.Models
{
    public partial class Document
    {
        public long IdDocument { get; set; }
        public string Relativepath { get; set; }
        public string Absolutepath { get; set; }
        public string Docname { get; set; }
        public DateTime? DtCreate { get; set; }
        public DateTime? DtUpdate { get; set; }
        public string UpdateBy { get; set; }
        public string CreateBy { get; set; }
        public int? StatVoid { get; set; }
        public int? Autoday { get; set; }
        public int? Autoversion { get; set; }
    }
}
