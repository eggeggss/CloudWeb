﻿using System;
using System.Collections.Generic;

#nullable disable

namespace dalmirleCloudCore.Models
{
    public partial class DocumentAccount
    {
        public long IdDocumentAccount { get; set; }
        public string Empname { get; set; }
        public long? IdDocument { get; set; }
        public DateTime? DtCreate { get; set; }
        public string CreateBy { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? DtUpdate { get; set; }
        public int? StatVoid { get; set; }
        public int? AllowUpload { get; set; }
        public int? AllowDownload { get; set; }
        public int? AllowCreatefolder { get; set; }
        public int? AllowDelfolder { get; set; }
        public int? AllowDelfile { get; set; }
        public int? AllowShare { get; set; }
    }
}
