﻿using System;
using System.Collections.Generic;

#nullable disable

namespace dalmirleCloudCore.Models
{
    public partial class DocumentFileVersion
    {
        public long IdDocumentFileVersion { get; set; }
        public long? IdDocument { get; set; }
        public string Filename { get; set; }
        public int? Version { get; set; }
        public DateTime? DtCreate { get; set; }
        public DateTime? DtUpdate { get; set; }
        public string UpdateBy { get; set; }
        public string CreateBy { get; set; }
        public int? StatVoid { get; set; }
    }
}
