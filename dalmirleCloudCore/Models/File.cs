﻿using System;
using System.Collections.Generic;

#nullable disable

namespace dalmirleCloudCore.Models
{
    public partial class File
    {
        public long IdFile { get; set; }
        public long? IdDocument { get; set; }
        public string Filename { get; set; }
        public string Relativepath { get; set; }
        public string Absolutepath { get; set; }
        public string Filesize { get; set; }
        public DateTime? DtCreate { get; set; }
        public DateTime? DtUpdate { get; set; }
        public string UpdateBy { get; set; }
        public string CreateBy { get; set; }
        public int? StatVoid { get; set; }
        public string Comment { get; set; }
        public string Preview { get; set; }
    }
}
