﻿using System;
using System.Collections.Generic;

#nullable disable

namespace dalmirleCloudCore.Models
{
    public partial class UserAccount
    {
        public long IdUserAccount { get; set; }
        public string Empname { get; set; }
        public DateTime? DtCreate { get; set; }
        public DateTime? DtUpdate { get; set; }
        public string CreateBy { get; set; }
        public string UpdateBy { get; set; }
        public int? StatVoid { get; set; }
        public int? IsManager { get; set; }
        public int? IsSuperuser { get; set; }
    }
}
