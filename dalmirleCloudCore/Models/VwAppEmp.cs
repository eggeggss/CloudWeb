﻿using System;
using System.Collections.Generic;

#nullable disable

namespace dalmirleCloudCore.Models
{
    public partial class VwAppEmp
    {
        public string EmpNo { get; set; }
        public string EmpEname { get; set; }
        public string EmpCname { get; set; }
        public string Email { get; set; }
    }
}
