﻿using System;
using System.Collections.Generic;

#nullable disable

namespace dalmirleCloudCore.Models
{
    public partial class VwDept
    {
        public string DeptNo { get; set; }
        public string DeptName { get; set; }
        public string Key { get; set; }
    }
}
