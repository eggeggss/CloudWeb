﻿using System;
using System.Collections.Generic;

#nullable disable

namespace dalmirleCloudCore.Models
{
    public partial class VwMember
    {
        public string EmpCname { get; set; }
        public string EmpEname { get; set; }
        public string EmpNo { get; set; }
        public string Company { get; set; }
        public string DeptNo { get; set; }
        public string Key { get; set; }
    }
}
