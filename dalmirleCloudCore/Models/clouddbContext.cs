﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace dalmirleCloudCore.Models
{
    public partial class clouddbContext : DbContext
    {
        public clouddbContext()
        {
        }

        public clouddbContext(DbContextOptions<clouddbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AccountDocument> AccountDocuments { get; set; }
        public virtual DbSet<Document> Documents { get; set; }
        public virtual DbSet<DocumentAccount> DocumentAccounts { get; set; }
        public virtual DbSet<DocumentDept> DocumentDepts { get; set; }
        public virtual DbSet<DocumentFileVersion> DocumentFileVersions { get; set; }
        public virtual DbSet<File> Files { get; set; }
        public virtual DbSet<UserAccount> UserAccounts { get; set; }
        public virtual DbSet<VwAppEmp> VwAppEmps { get; set; }
        public virtual DbSet<VwDept> VwDepts { get; set; }
        public virtual DbSet<VwMember> VwMembers { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=eggeggss.ddns.me,8101;Database=clouddb;User ID=sa;Password=Password!");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            OnModelCreatingExtent(modelBuilder);

            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<AccountDocument>(entity =>
            {
                entity.HasKey(e => e.IdAccountDocument);

                entity.ToTable("AccountDocument");

                entity.HasIndex(e => new { e.Empname, e.IdDocument }, "IDX_name_id")
                    .IsUnique();

                entity.Property(e => e.IdAccountDocument).HasColumnName("id_AccountDocument");

                entity.Property(e => e.CreateBy)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("create_by");

                entity.Property(e => e.DtCreate)
                    .HasColumnType("datetime")
                    .HasColumnName("dt_create")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DtUpdate)
                    .HasColumnType("datetime")
                    .HasColumnName("dt_update");

                entity.Property(e => e.Empname)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("empname");

                entity.Property(e => e.IdDocument).HasColumnName("id_Document");

                entity.Property(e => e.StatVoid)
                    .HasColumnName("stat_void")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UpdateBy)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("update_by");
            });

            modelBuilder.Entity<Document>(entity =>
            {
                entity.HasKey(e => e.IdDocument);

                entity.ToTable("Document");

                entity.Property(e => e.IdDocument).HasColumnName("id_Document");

                entity.Property(e => e.Absolutepath).HasColumnName("absolutepath");

                entity.Property(e => e.Autoday)
                    .HasColumnName("autoday")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.Autoversion)
                    .HasColumnName("autoversion")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.CreateBy)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("create_by");

                entity.Property(e => e.Docname).HasColumnName("docname");

                entity.Property(e => e.DtCreate)
                    .HasColumnType("datetime")
                    .HasColumnName("dt_create")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DtUpdate)
                    .HasColumnType("datetime")
                    .HasColumnName("dt_update");

                entity.Property(e => e.Relativepath).HasColumnName("relativepath");

                entity.Property(e => e.StatVoid)
                    .HasColumnName("stat_void")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UpdateBy)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("update_by");
            });

            modelBuilder.Entity<DocumentAccount>(entity =>
            {
                entity.HasKey(e => e.IdDocumentAccount);

                entity.ToTable("DocumentAccount");

                entity.HasIndex(e => new { e.Empname, e.IdDocument }, "IDX_name_id")
                    .IsUnique();

                entity.Property(e => e.IdDocumentAccount).HasColumnName("id_DocumentAccount");

                entity.Property(e => e.AllowCreatefolder)
                    .HasColumnName("allow_createfolder")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.AllowDelfile)
                    .HasColumnName("allow_delfile")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.AllowDelfolder)
                    .HasColumnName("allow_delfolder")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.AllowDownload)
                    .HasColumnName("allow_download")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.AllowShare)
                    .HasColumnName("allow_share")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.AllowUpload)
                    .HasColumnName("allow_upload")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.CreateBy)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("create_by");

                entity.Property(e => e.DtCreate)
                    .HasColumnType("datetime")
                    .HasColumnName("dt_create")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DtUpdate)
                    .HasColumnType("datetime")
                    .HasColumnName("dt_update");

                entity.Property(e => e.Empname)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("empname");

                entity.Property(e => e.IdDocument).HasColumnName("id_Document");

                entity.Property(e => e.StatVoid)
                    .HasColumnName("stat_void")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UpdateBy)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("update_by");
            });

            modelBuilder.Entity<DocumentDept>(entity =>
            {
                entity.HasKey(e => e.IdDocumentDept);

                entity.ToTable("DocumentDept");

                entity.Property(e => e.IdDocumentDept).HasColumnName("id_DocumentDept");

                entity.Property(e => e.AllowCreatefolder)
                    .HasColumnName("allow_createfolder")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.AllowDelfile)
                    .HasColumnName("allow_delfile")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.AllowDelfolder)
                    .HasColumnName("allow_delfolder")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.AllowDownload)
                    .HasColumnName("allow_download")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.AllowShare)
                    .HasColumnName("allow_share")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.AllowUpload)
                    .HasColumnName("allow_upload")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.CreateBy)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("create_by");

                entity.Property(e => e.DeptNo)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("dept_no");

                entity.Property(e => e.DtCreate)
                    .HasColumnType("datetime")
                    .HasColumnName("dt_create")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DtUpdate)
                    .HasColumnType("datetime")
                    .HasColumnName("dt_update");

                entity.Property(e => e.IdDocument).HasColumnName("id_Document");

                entity.Property(e => e.StatVoid)
                    .HasColumnName("stat_void")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UpdateBy)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("update_by");
            });

            modelBuilder.Entity<DocumentFileVersion>(entity =>
            {
                entity.HasKey(e => e.IdDocumentFileVersion);

                entity.ToTable("DocumentFileVersion");

                entity.Property(e => e.IdDocumentFileVersion).HasColumnName("id_DocumentFileVersion");

                entity.Property(e => e.CreateBy)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("create_by");

                entity.Property(e => e.DtCreate)
                    .HasColumnType("datetime")
                    .HasColumnName("dt_create")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DtUpdate)
                    .HasColumnType("datetime")
                    .HasColumnName("dt_update");

                entity.Property(e => e.Filename).HasColumnName("filename");

                entity.Property(e => e.IdDocument).HasColumnName("id_Document");

                entity.Property(e => e.StatVoid)
                    .HasColumnName("stat_void")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UpdateBy)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("update_by");

                entity.Property(e => e.Version)
                    .HasColumnName("version")
                    .HasDefaultValueSql("((1))");
            });

            modelBuilder.Entity<File>(entity =>
            {
                entity.HasKey(e => e.IdFile);

                entity.ToTable("File");

                entity.Property(e => e.IdFile).HasColumnName("id_File");

                entity.Property(e => e.Absolutepath).HasColumnName("absolutepath");

                entity.Property(e => e.Comment)
                    .HasColumnName("comment")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.CreateBy)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("create_by");

                entity.Property(e => e.DtCreate)
                    .HasColumnType("datetime")
                    .HasColumnName("dt_create")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DtUpdate)
                    .HasColumnType("datetime")
                    .HasColumnName("dt_update");

                entity.Property(e => e.Filename).HasColumnName("filename");

                entity.Property(e => e.Filesize).HasColumnName("filesize");

                entity.Property(e => e.IdDocument).HasColumnName("id_Document");

                entity.Property(e => e.Preview)
                    .IsUnicode(false)
                    .HasColumnName("preview")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Relativepath).HasColumnName("relativepath");

                entity.Property(e => e.StatVoid)
                    .HasColumnName("stat_void")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UpdateBy)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("update_by");
            });

            modelBuilder.Entity<UserAccount>(entity =>
            {
                entity.HasKey(e => e.IdUserAccount);

                entity.ToTable("UserAccount");

                entity.Property(e => e.IdUserAccount).HasColumnName("id_UserAccount");

                entity.Property(e => e.CreateBy)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("create_by");

                entity.Property(e => e.DtCreate)
                    .HasColumnType("datetime")
                    .HasColumnName("dt_create")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DtUpdate)
                    .HasColumnType("datetime")
                    .HasColumnName("dt_update");

                entity.Property(e => e.Empname)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("empname");

                entity.Property(e => e.IsManager)
                    .HasColumnName("is_manager")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.IsSuperuser)
                    .HasColumnName("is_superuser")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.StatVoid)
                    .HasColumnName("stat_void")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UpdateBy)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("update_by");
            });

            modelBuilder.Entity<VwAppEmp>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_AppEmp");

                entity.Property(e => e.Email)
                    .HasMaxLength(1024)
                    .IsUnicode(false)
                    .HasColumnName("email");

                entity.Property(e => e.EmpCname)
                    .HasMaxLength(100)
                    .HasColumnName("emp_cname");

                entity.Property(e => e.EmpEname)
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasColumnName("emp_ename");

                entity.Property(e => e.EmpNo)
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasColumnName("emp_no");
            });

            modelBuilder.Entity<VwDept>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_dept");

                entity.Property(e => e.DeptName).HasColumnName("dept_name");

                entity.Property(e => e.DeptNo)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("dept_no");

                entity.Property(e => e.Key).HasColumnName("key");
            });

            modelBuilder.Entity<VwMember>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_member");

                entity.Property(e => e.Company)
                    .IsRequired()
                    .HasMaxLength(3)
                    .HasColumnName("company");

                entity.Property(e => e.DeptNo)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("dept_no");

                entity.Property(e => e.EmpCname)
                    .HasMaxLength(100)
                    .HasColumnName("emp_cname");

                entity.Property(e => e.EmpEname)
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasColumnName("emp_ename");

                entity.Property(e => e.EmpNo)
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasColumnName("emp_no");

                entity.Property(e => e.Key)
                    .HasMaxLength(263)
                    .HasColumnName("key");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
