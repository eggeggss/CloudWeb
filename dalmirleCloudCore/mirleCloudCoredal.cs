﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using dalmirleCloudCore.Common;
using dalmirleCloudCore.Meta;
using dalmirleCloudCore.Models;
using Microsoft.EntityFrameworkCore;

namespace dalmirleCloudCore
{
    public class mirleCloudCoredal
    {
        public string Location { set; get; }
        private EFAdapter _adapter;
        private clouddbContext _entity;

        public mirleCloudCoredal(EFAdapter adapter,
            clouddbContext entity)
        {
            Location = adapter.Location;
            _adapter = adapter;
            _entity = entity;
        }

        /******Document********/
        public async Task<Document> FindDocument(string rpath)
        {
            var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
            optionsBuilder.UseSqlServer(_adapter.DbString);
            _entity = new clouddbContext(optionsBuilder.Options);

            var doc = await _entity.Documents.Where(e => e.Relativepath.Equals(rpath) && e.StatVoid==0).FirstOrDefaultAsync();
            
            return doc;
        }

        public async Task<Document> CreateDocument(Document doc)
        {
            var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
            optionsBuilder.UseSqlServer(_adapter.DbString);
            _entity = new clouddbContext(optionsBuilder.Options);

            doc.StatVoid = 0;
            doc.DtCreate = DateTime.Now.AddHours(8);
            _entity.Entry(doc).State = EntityState.Added;

            await _entity.SaveChangesAsync();

            return doc;
        }


        public async Task<Document> UpdateDocument(Document doc)
        {
            var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
            optionsBuilder.UseSqlServer(_adapter.DbString);
            _entity = new clouddbContext(optionsBuilder.Options);

            doc.DtUpdate = DateTime.Now.AddHours(8);

            _entity.Entry(doc).State = EntityState.Modified;

            await _entity.SaveChangesAsync();

            return doc;
        }


        public async Task<bool> DelDocument(Document doc)
        {
            var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
            optionsBuilder.UseSqlServer(_adapter.DbString);
            _entity = new clouddbContext(optionsBuilder.Options);

            doc.StatVoid = 1;
            doc.DtUpdate = DateTime.Now.AddHours(8);

            _entity.Entry(doc).State = EntityState.Modified;

            await _entity.SaveChangesAsync();

            return true;
        }

        public async Task<bool> UpdateDocumentAutoday(long iddoc, int autoday, string who)
        {
            var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
            optionsBuilder.UseSqlServer(_adapter.DbString);
            _entity = new clouddbContext(optionsBuilder.Options);

            var doc = await _entity.Documents.FindAsync(iddoc);

            if (doc != null)
            {
                doc.Autoday = autoday;
                doc.UpdateBy = who;
                doc.DtUpdate = DateTime.Now.AddHours(8);
                _entity.Entry(doc).State = EntityState.Modified;
                await _entity.SaveChangesAsync();
            }

            return true;
        }



        public async Task<bool> UpdateDocumentAutoversion(long iddoc, int autoversion, string who)
        {
            var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
            optionsBuilder.UseSqlServer(_adapter.DbString);
            _entity = new clouddbContext(optionsBuilder.Options);

            var doc = await _entity.Documents.FindAsync(iddoc);

            if (doc != null)
            {
                doc.Autoversion = autoversion;
                doc.UpdateBy = who;
                doc.DtUpdate = DateTime.Now.AddHours(8);
                _entity.Entry(doc).State = EntityState.Modified;
                await _entity.SaveChangesAsync();
            }

            return true;
        }


        /******Document********/

        /******File********/
        public async Task<File> FindFile(string rpath)
        {
            var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
            optionsBuilder.UseSqlServer(_adapter.DbString);
            _entity = new clouddbContext(optionsBuilder.Options);

            var doc = await _entity.Files.Where(e => e.Relativepath.Equals(rpath) && e.StatVoid==0).FirstOrDefaultAsync();

            return doc;
        }

        public async Task<File> FindFile(long id_file)
        {
            var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
            optionsBuilder.UseSqlServer(_adapter.DbString);
            _entity = new clouddbContext(optionsBuilder.Options);

            var doc = await _entity.Files.FindAsync(id_file);

            return doc;
        }

        public async Task<IEnumerable< File>> FindFiles(long  idDocument)
        {
            var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
            optionsBuilder.UseSqlServer(_adapter.DbString);
            _entity = new clouddbContext(optionsBuilder.Options);

            var doc = await _entity.Files.Where(e => e.IdDocument == idDocument).ToListAsync(); ;

            return doc;
        }

        public async Task<long> CreateFile(File file)
        {
            var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
            optionsBuilder.UseSqlServer(_adapter.DbString);
            _entity = new clouddbContext(optionsBuilder.Options);

            file.StatVoid = 0;
            file.DtCreate = DateTime.Now.AddHours(8);

            _entity.Entry(file).State = EntityState.Added;

            await _entity.SaveChangesAsync();

            var idfile = file.IdFile;

            return idfile ;
        }


        public async Task<long> UpdateFile(File doc)
        {
            var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
            optionsBuilder.UseSqlServer(_adapter.DbString);
            _entity = new clouddbContext(optionsBuilder.Options);

            doc.DtUpdate = DateTime.Now.AddHours(8);
            
            _entity.Entry(doc).State = EntityState.Modified;

            await _entity.SaveChangesAsync();

            return doc.IdFile;
        }


        public async Task<bool> DelFile(File doc)
        {
            var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
            optionsBuilder.UseSqlServer(_adapter.DbString);
            _entity = new clouddbContext(optionsBuilder.Options);

            doc.StatVoid = 1;
            doc.DtUpdate = DateTime.Now.AddDays(8);

            _entity.Entry(doc).State = EntityState.Modified;

            await _entity.SaveChangesAsync();

            return true;
        }

        /******File********/

        /******[AccountDocument***********/
        public async Task<AccountDocument> FindAccountDocument(long idDocument)
        {
            var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
            optionsBuilder.UseSqlServer(_adapter.DbString);
            _entity = new clouddbContext(optionsBuilder.Options);

            var result = await _entity.AccountDocuments.Where(e => e.IdDocument==idDocument
            && e.StatVoid==0).FirstOrDefaultAsync();

            return result;
        }

        public async Task<AccountDocument> FindAccountDocument(long idDocument,string ename)
        {
            var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
            optionsBuilder.UseSqlServer(_adapter.DbString);
            _entity = new clouddbContext(optionsBuilder.Options);

            var result = await _entity.AccountDocuments.Where(
            e => e.IdDocument == idDocument
            && e.Empname.Equals(ename) 
            && e.StatVoid==0).FirstOrDefaultAsync();

            return result;
        }

        public async Task<bool> CreateAccountDocument(AccountDocument doct)
        {
            var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
            optionsBuilder.UseSqlServer(_adapter.DbString);
            _entity = new clouddbContext(optionsBuilder.Options);

            doct.StatVoid = 0;
            doct.DtCreate = DateTime.Now.AddHours(8);

            _entity.Entry(doct).State = EntityState.Added;

            await _entity.SaveChangesAsync();

            return true;
        }


        public async Task<bool> UpdateAccountDocument(AccountDocument doct)
        {
            var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
            optionsBuilder.UseSqlServer(_adapter.DbString);
            _entity = new clouddbContext(optionsBuilder.Options);


            doct.DtUpdate = DateTime.Now.AddHours(8);

            _entity.Entry(doct).State = EntityState.Modified;

            await _entity.SaveChangesAsync();

            return true;
        }


        public async Task<bool> DelAccountDocument(AccountDocument doct)
        {
            var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
            optionsBuilder.UseSqlServer(_adapter.DbString);
            _entity = new clouddbContext(optionsBuilder.Options);

            doct.StatVoid = 1;
            doct.DtUpdate = DateTime.Now.AddHours(8);

            //_entity.Entry(doct).State = EntityState.Modified;
            _entity.Entry(doct).State = EntityState.Deleted;

            await _entity.SaveChangesAsync();

            return true;
        }

        /******[AccountDocument***********/



        /******DocumentAccount***********/

        public async Task<DocumentAccount> FindDocumentAccountBypk(long iddocumentaccount)
        {
            var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
            optionsBuilder.UseSqlServer(_adapter.DbString);
            _entity = new clouddbContext(optionsBuilder.Options);

            var result = await _entity.DocumentAccounts.FindAsync(iddocumentaccount);

            return result;
        }


        public async Task<IEnumerable<DocumentAccount>> FindDocumentAccount(long idDocument)
        {
            var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
            optionsBuilder.UseSqlServer(_adapter.DbString);
            _entity = new clouddbContext(optionsBuilder.Options);

            var result = await _entity.DocumentAccounts.Where(e => ((long)e.IdDocument).Equals(idDocument)
            && e.StatVoid == 0).ToListAsync();
               

            return result;
        }


        public async Task<IEnumerable<DocumentAccount>> FindDocumentAccount(string ename)
        {
            var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
            optionsBuilder.UseSqlServer(_adapter.DbString);
            _entity = new clouddbContext(optionsBuilder.Options);

            var result = await _entity.DocumentAccounts.Where(e => e.Empname.Equals(ename) && e.StatVoid==0)
                .ToListAsync();

            return result;
        }


        public async Task<IEnumerable<DocumentAccount>> FindDocumentAccount(string ename,long idDocument)
        {
            var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
            optionsBuilder.UseSqlServer(_adapter.DbString);
            _entity = new clouddbContext(optionsBuilder.Options);

            var result = await _entity
                .DocumentAccounts.Where(e => e.Empname.Equals(ename) && 
                e.IdDocument==idDocument &&
                e.StatVoid == 0)
                .ToListAsync();

            return result;
        }


        public async Task<bool> CreateDocumentAccount1(DocumentAccount doct)
        {
            var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
            optionsBuilder.UseSqlServer(_adapter.DbString);
            _entity = new clouddbContext(optionsBuilder.Options);

            doct.DtCreate = DateTime.Now.AddHours(8);
            doct.StatVoid = 0;
            _entity.Entry(doct).State = EntityState.Added;

            await _entity.SaveChangesAsync();

            return true;
        }


        public async Task<bool> UpdateDocumentAccount(DocumentAccount doct)
        {
            var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
            optionsBuilder.UseSqlServer(_adapter.DbString);
            _entity = new clouddbContext(optionsBuilder.Options);

            _entity.Entry(doct).State = EntityState.Modified;

            doct.DtCreate = DateTime.Now.AddHours(8);
            doct.StatVoid = 0;

            await _entity.SaveChangesAsync();

            return true;
        }


        public async Task<bool> DelDocumentAccount(DocumentAccount doct)
        {
            var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
            optionsBuilder.UseSqlServer(_adapter.DbString);
            _entity = new clouddbContext(optionsBuilder.Options);

            doct.StatVoid = 1;
            doct.DtUpdate = DateTime.Now.AddDays(8);

            //_entity.Entry(doct).State = EntityState.Modified;
            _entity.Entry(doct).State = EntityState.Deleted;

            await _entity.SaveChangesAsync();

            return true;
        }


        public async Task<bool> UpdateMemberAllowUpload(long iddocacc,int allow_upload,string who)
        {
            var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
            optionsBuilder.UseSqlServer(_adapter.DbString);
            _entity = new clouddbContext(optionsBuilder.Options);

            var doc=await _entity.DocumentAccounts.FindAsync(iddocacc);

            if (doc != null)
            {
                doc.AllowUpload = allow_upload;
                doc.UpdateBy = who;
                doc.DtUpdate = DateTime.Now.AddHours(8);
                _entity.Entry(doc).State = EntityState.Modified;
                await _entity.SaveChangesAsync();
            }



            return true;
        }

        public async Task<bool> UpdateMemberAllowDownload(long iddocacc, int allow_download,string who)
        {
            var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
            optionsBuilder.UseSqlServer(_adapter.DbString);
            _entity = new clouddbContext(optionsBuilder.Options);

            var doc = await _entity.DocumentAccounts.FindAsync(iddocacc);

            if (doc != null)
            {
                doc.AllowDownload = allow_download;
                doc.UpdateBy = who;
                doc.DtUpdate = DateTime.Now.AddHours(8);
                _entity.Entry(doc).State = EntityState.Modified;
                await _entity.SaveChangesAsync();
            }

            return true;
        }

        public async Task<bool> UpdateMemberAllowFolder(long iddocacc, int allow_folder,string who)
        {
            var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
            optionsBuilder.UseSqlServer(_adapter.DbString);
            _entity = new clouddbContext(optionsBuilder.Options);

            var doc = await _entity.DocumentAccounts.FindAsync(iddocacc);

            if (doc != null)
            {
                doc.AllowCreatefolder = allow_folder;
                doc.UpdateBy = who;
                doc.DtUpdate = DateTime.Now.AddHours(8);
                _entity.Entry(doc).State = EntityState.Modified;
                await _entity.SaveChangesAsync();
            }

            return true;
        }

        public async Task<bool> UpdateMemberAllowDelFolder(long iddocacc, int allow_delfolder,string who)
        {
            var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
            optionsBuilder.UseSqlServer(_adapter.DbString);
            _entity = new clouddbContext(optionsBuilder.Options);

            var doc = await _entity.DocumentAccounts.FindAsync(iddocacc);

            if (doc != null)
            {
                doc.AllowDelfolder = allow_delfolder;
                doc.UpdateBy = who;
                doc.DtUpdate = DateTime.Now.AddHours(8);
                _entity.Entry(doc).State = EntityState.Modified;
                await _entity.SaveChangesAsync();
            }

            return true;
        }

        public async Task<bool> UpdateMemberAllowDelFile(long iddocacc, int allow_delfile,string who)
        {
            var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
            optionsBuilder.UseSqlServer(_adapter.DbString);
            _entity = new clouddbContext(optionsBuilder.Options);

            var doc = await _entity.DocumentAccounts.FindAsync(iddocacc);

            if (doc != null)
            {
                doc.AllowDelfile = allow_delfile;
                doc.UpdateBy = who;
                doc.DtUpdate = DateTime.Now.AddHours(8);
                _entity.Entry(doc).State = EntityState.Modified;
                await _entity.SaveChangesAsync();
            }

            return true;
        }


        public async Task<bool> UpdateMemberAllowShare(long iddocacc, int allow_share,string who)
        {
            var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
            optionsBuilder.UseSqlServer(_adapter.DbString);
            _entity = new clouddbContext(optionsBuilder.Options);

            var doc = await _entity.DocumentAccounts.FindAsync(iddocacc);

            if (doc != null)
            {
                doc.AllowShare = allow_share;
                doc.UpdateBy = who;
                doc.DtUpdate = DateTime.Now.AddHours(8);
                _entity.Entry(doc).State = EntityState.Modified;
                await _entity.SaveChangesAsync();
            }

            return true;
        }



        /******DocumentAccount***********/



        /*******AccountDocument************************/


        public async Task<IEnumerable<zp_inital_user_Result>> InitialUser(string empname)
        {
            var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
            optionsBuilder.UseSqlServer(_adapter.DbString);
            _entity = new clouddbContext(optionsBuilder.Options);

            var result = await _entity.zp_inital_user_Result(empname);

            return result;
        }


        /*******AccountDocument************************/



        /******DocumentDept********/

        public async Task<DocumentDept> FindDocumentDept(string dept_no,long id_document)
        {
            var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
            optionsBuilder.UseSqlServer(_adapter.DbString);
            _entity = new clouddbContext(optionsBuilder.Options);

            var result = await _entity.DocumentDepts
                .Where(e => e.DeptNo.Equals(dept_no) && e.IdDocument == id_document && e.StatVoid==0).FirstOrDefaultAsync();

            return result;
        }

        public async Task<DocumentDept> FindDocumentDept(long iddocumentdept)
        {
            var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
            optionsBuilder.UseSqlServer(_adapter.DbString);
            _entity = new clouddbContext(optionsBuilder.Options);

            var result = await _entity.DocumentDepts.FindAsync(iddocumentdept);

            return result;
        }

        public async Task<IEnumerable<DocumentDept>> FindDocumentDepts(long iddocument)
        {
            var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
            optionsBuilder.UseSqlServer(_adapter.DbString);
            _entity = new clouddbContext(optionsBuilder.Options);

            var result = await _entity.DocumentDepts.Where(e => e.IdDocument == iddocument && e.StatVoid==0).ToListAsync();

            return result;
        }




        public async Task<bool> CreateDocumentDept(DocumentDept doct)
        {
            var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
            optionsBuilder.UseSqlServer(_adapter.DbString);
            _entity = new clouddbContext(optionsBuilder.Options);

            doct.DtCreate = DateTime.Now.AddHours(8);
            doct.StatVoid = 0;
            _entity.Entry(doct).State = EntityState.Added;

            await _entity.SaveChangesAsync();

            return true;
        }


        public async Task<bool> UpdateDocumentDept(DocumentDept doct)
        {
            var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
            optionsBuilder.UseSqlServer(_adapter.DbString);
            _entity = new clouddbContext(optionsBuilder.Options);

            _entity.Entry(doct).State = EntityState.Modified;

            doct.DtCreate = DateTime.Now.AddHours(8);
            doct.StatVoid = 0;

            await _entity.SaveChangesAsync();

            return true;
        }


        public async Task<bool> DelDocumentDept(DocumentDept doct)
        {
            var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
            optionsBuilder.UseSqlServer(_adapter.DbString);
            _entity = new clouddbContext(optionsBuilder.Options);

            doct.StatVoid = 1;
            doct.DtUpdate = DateTime.Now.AddDays(8);

            //_entity.Entry(doct).State = EntityState.Modified;
            _entity.Entry(doct).State = EntityState.Deleted;

            await _entity.SaveChangesAsync();

            return true;
        }



        public async Task<bool> UpdateDeptAllowUpload(long iddocdept, int allow_upload, string who)
        {
            var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
            optionsBuilder.UseSqlServer(_adapter.DbString);
            _entity = new clouddbContext(optionsBuilder.Options);

            var doc = await _entity.DocumentDepts.FindAsync(iddocdept);

            if (doc != null)
            {
                doc.AllowUpload = allow_upload;
                doc.UpdateBy = who;
                doc.DtUpdate = DateTime.Now.AddHours(8);
                _entity.Entry(doc).State = EntityState.Modified;
                await _entity.SaveChangesAsync();
            }



            return true;
        }


        public async Task<bool> UpdateDeptAllowDownload(long iddocdept, int allow_download, string who)
        {
            var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
            optionsBuilder.UseSqlServer(_adapter.DbString);
            _entity = new clouddbContext(optionsBuilder.Options);

            var doc = await _entity.DocumentDepts.FindAsync(iddocdept);

            if (doc != null)
            {
                doc.AllowDownload = allow_download;
                doc.UpdateBy = who;
                doc.DtUpdate = DateTime.Now.AddHours(8);
                _entity.Entry(doc).State = EntityState.Modified;
                await _entity.SaveChangesAsync();
            }

            return true;
        }

        public async Task<bool> UpdateDeptAllowFolder(long iddocdept, int allow_folder, string who)
        {
            var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
            optionsBuilder.UseSqlServer(_adapter.DbString);
            _entity = new clouddbContext(optionsBuilder.Options);

            var doc = await _entity.DocumentDepts.FindAsync(iddocdept);

            if (doc != null)
            {
                doc.AllowCreatefolder = allow_folder;
                doc.UpdateBy = who;
                doc.DtUpdate = DateTime.Now.AddHours(8);
                _entity.Entry(doc).State = EntityState.Modified;
                await _entity.SaveChangesAsync();
            }

            return true;
        }

        public async Task<bool> UpdateDeptAllowDelFolder(long iddocdept, int allow_delfolder, string who)
        {
            var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
            optionsBuilder.UseSqlServer(_adapter.DbString);
            _entity = new clouddbContext(optionsBuilder.Options);

            var doc = await _entity.DocumentDepts.FindAsync(iddocdept);

            if (doc != null)
            {
                doc.AllowDelfolder = allow_delfolder;
                doc.UpdateBy = who;
                doc.DtUpdate = DateTime.Now.AddHours(8);
                _entity.Entry(doc).State = EntityState.Modified;
                await _entity.SaveChangesAsync();
            }

            return true;
        }

        public async Task<bool> UpdateDeptAllowDelFile(long iddocdept, int allow_delfile, string who)
        {
            var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
            optionsBuilder.UseSqlServer(_adapter.DbString);
            _entity = new clouddbContext(optionsBuilder.Options);

            var doc = await _entity.DocumentDepts.FindAsync(iddocdept);

            if (doc != null)
            {
                doc.AllowDelfile = allow_delfile;
                doc.UpdateBy = who;
                doc.DtUpdate = DateTime.Now.AddHours(8);
                _entity.Entry(doc).State = EntityState.Modified;
                await _entity.SaveChangesAsync();
            }

            return true;
        }


        public async Task<bool> UpdateDeptAllowShare(long iddocdept, int allow_share, string who)
        {
            var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
            optionsBuilder.UseSqlServer(_adapter.DbString);
            _entity = new clouddbContext(optionsBuilder.Options);

            var doc = await _entity.DocumentDepts.FindAsync(iddocdept);

            if (doc != null)
            {
                doc.AllowShare = allow_share;
                doc.UpdateBy = who;
                doc.DtUpdate = DateTime.Now.AddHours(8);
                _entity.Entry(doc).State = EntityState.Modified;
                await _entity.SaveChangesAsync();
            }

            return true;
        }

        public async Task<IEnumerable<zp_rename_directory_Result>> RenameDirectory(
            string rpathold,
            string rpathnew,
            string newname,
            string update_by)
        {
            var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
            optionsBuilder.UseSqlServer(_adapter.DbString);
            _entity = new clouddbContext(optionsBuilder.Options);

            var result = await _entity.zp_rename_directory_Result(rpathold, rpathnew, newname, update_by);

            return result;
        }

        public async Task<IEnumerable<zp_remove_directory_Result>> RemoveDirectory(
            long id_Document,
            string rpathold,
            string rpathnew,
            string update_by)
        {
            var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
            optionsBuilder.UseSqlServer(_adapter.DbString);
            _entity = new clouddbContext(optionsBuilder.Options);

            var result = await _entity.zp_remove_directory_Result(id_Document,rpathold, rpathnew, update_by);

            return result;
        }


        /******DocumentDept********/


        /******AccountUser********/

        public async Task<UserAccount> FindUserAccount(string empname)
        {
            var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
            optionsBuilder.UseSqlServer(_adapter.DbString);
            _entity = new clouddbContext(optionsBuilder.Options);

            var result = await _entity.UserAccounts
                .Where(e => e.Empname.Equals(empname) && e.StatVoid == 0).FirstOrDefaultAsync();

            return result;
        }


        public async Task<UserAccount> FindUserAccount(long id_UserAccount)
        {
            var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
            optionsBuilder.UseSqlServer(_adapter.DbString);
            _entity = new clouddbContext(optionsBuilder.Options);

            var result = await _entity.UserAccounts.FindAsync(id_UserAccount);

            return result;
        }



        public async Task<bool> UpdateUserAccount(UserAccount user)
        {
            var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
            optionsBuilder.UseSqlServer(_adapter.DbString);
            _entity = new clouddbContext(optionsBuilder.Options);
            /*
            var thisuser = await _entity.UserAccounts.
                Where(e => e.IdUserAccount == user.IdUserAccount && e.StatVoid == 0)
                .FirstOrDefaultAsync();
            */
            user.DtUpdate = DateTime.Now.AddHours(8);

            _entity.Entry(user).State = EntityState.Modified;
            await _entity.SaveChangesAsync();

            return true;
        }

        public async Task<bool> InsertUserAccount(UserAccount user)
        {
            var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
            optionsBuilder.UseSqlServer(_adapter.DbString);
            _entity = new clouddbContext(optionsBuilder.Options);

            user.StatVoid = 0;
            user.DtCreate = DateTime.Now.AddHours(8);

            _entity.Entry(user).State = EntityState.Added;
            await _entity.SaveChangesAsync();

            return true;
        }

        /******AccountUser********/



    }
}
