﻿using dalmirleCloudCore.Common;
using dalmirleCloudCore.Meta;
using dalmirleCloudCore.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dalmirleCloudCore
{
    public class mirleCloudCorequery
    {
        public string Location { set; get; }
        private EFAdapter _adapter;
        private clouddbContext _entity;

        public mirleCloudCorequery(EFAdapter adapter,
            clouddbContext entity)
        {
            Location = adapter.Location;
            _adapter = adapter;
            _entity = entity;
        }

        public async Task<IEnumerable<VwDept>> GetDepts()
        {
            try
            {
                var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
                optionsBuilder.UseSqlServer(_adapter.DbString);
                _entity = new clouddbContext(optionsBuilder.Options);

                var result = await _entity.VwDepts.ToListAsync();
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception($"GetDepts fail {ex.Message}");
            }
        }

        public async Task<IEnumerable<VwMember>> GetMembers()
        {
            var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
            optionsBuilder.UseSqlServer(_adapter.DbString);
            _entity = new clouddbContext(optionsBuilder.Options);
            IEnumerable<VwMember> result = null;

            result = await _entity.VwMembers.ToListAsync();

            return result;
        }

        public async Task<IEnumerable<zp_get_share_folder_user_list_Result>> GetShareFolderMember(string rpath)
        {
            var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
            optionsBuilder.UseSqlServer(_adapter.DbString);
            _entity = new clouddbContext(optionsBuilder.Options);

            try
            {
                var result = await _entity.zp_get_share_folder_user_list_Result(rpath);
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception($"GetShareFolderMember fail {ex.Message}");
            }
        }

        public async Task<IEnumerable<zp_get_share_folder_dept_list_Result>> GetShareFolderDept(string rpath)
        {
            var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
            optionsBuilder.UseSqlServer(_adapter.DbString);
            _entity = new clouddbContext(optionsBuilder.Options);

            try
            {
                var result = await _entity.zp_get_share_folder_dept_list_Result(rpath);
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception($"GetShareFolderDept fail:{ex.Message}");
            }
        }





        public async Task<IEnumerable<zp_share_to_other_list_Result>> GetShareToOtherTree(string empname)
        {
            var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
            optionsBuilder.UseSqlServer(_adapter.DbString);
            _entity = new clouddbContext(optionsBuilder.Options);

            try
            {
                var result = await _entity.zp_share_to_other_list_Result(empname);
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception($"GetShareToOtherTree fail:{ex.Message}");
            }
        }


        public async Task<IEnumerable<zp_get_sharetome_list_Result>> GetShareToMeTree(string empname)
        {
            var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
            optionsBuilder.UseSqlServer(_adapter.DbString);
            _entity = new clouddbContext(optionsBuilder.Options);

            try
            {
                var result = await _entity.zp_get_sharetome_list_Result(empname);
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception($"GetShareToMeTree fail:{ex.Message}");
            }
        }


        public async Task<IEnumerable<zp_get_sharetome_list_for_dashboard_Result>> GetShareToMeTreeForDashBoard(string empname, int top)
        {
            var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
            optionsBuilder.UseSqlServer(_adapter.DbString);
            _entity = new clouddbContext(optionsBuilder.Options);

            try
            {
                var result = await _entity.zp_get_sharetome_list_for_dashboard_Result(empname, top);
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception($"GetShareToMeTreeForDashBoard fail:{ex.Message}");
            }
        }


        public async Task<IEnumerable<zp_get_sharetome_list_manager_for_dashboard_Result>> GetShareToMeTreeForManagerDashBoard(string empname, int top)
        {
            var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
            optionsBuilder.UseSqlServer(_adapter.DbString);
            _entity = new clouddbContext(optionsBuilder.Options);

            try
            {
                var result = await _entity.zp_get_sharetome_list_manager_for_dashboard_Result(empname, top);
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception($"GetShareToMeTreeForManagerDashBoard fail:{ex.Message}");
            }
        }


        public async Task<IEnumerable<zp_get_file_datatable_Result>> GetFileDataTable(string empname)
        {
            var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
            optionsBuilder.UseSqlServer(_adapter.DbString);
            _entity = new clouddbContext(optionsBuilder.Options);

            try
            {
                var result = await _entity.zp_get_file_datatable_Result(empname);
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception($"GetFileDataTable fail:{ex.Message}");
            }
        }


        public async Task<VwAppEmp> GetAppEmpsInfoByName(string ename)
        {
            try
            {
                var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
                optionsBuilder.UseSqlServer(_adapter.DbString);
                _entity = new clouddbContext(optionsBuilder.Options);

                var result = await _entity.VwAppEmps.Where(e => e.EmpEname.Equals(ename)).FirstOrDefaultAsync();

                return result;
            }
            catch (Exception ex)
            {
                throw new Exception($"GetAppEmpsInfoByName fail:{ex.Message}");
            }
        }

        public async Task<IEnumerable<VwDept>> GetDeptNos()
        {
            try
            {
                var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
                optionsBuilder.UseSqlServer(_adapter.DbString);
                _entity = new clouddbContext(optionsBuilder.Options);

                var result = await _entity.VwDepts.ToListAsync();

                return result;
            }
            catch (Exception ex)
            {
                throw new Exception($"GetDeptNos fail:{ex.Message}");
            }

        }

        public async Task<IEnumerable<zp_get_doc_size_report_Result>> Getdocsize_report(string empname)
        {
            try
            {
                var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
                optionsBuilder.UseSqlServer(_adapter.DbString);
                _entity = new clouddbContext(optionsBuilder.Options);

                var result = await _entity.zp_get_doc_size_report_Result(empname);

                return result;
            }
            catch (Exception ex)
            {
                throw new Exception($"Getdocsize_report fail:{ex.Message}");
            }
        }

        public async Task<IEnumerable<zp_get_disk_space_report_Result>> GetSpaceReport(string empname)
        {
            try
            {
                var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
                optionsBuilder.UseSqlServer(_adapter.DbString);
                _entity = new clouddbContext(optionsBuilder.Options);

                var result = await _entity.zp_get_disk_space_report_Result(empname);

                return result;
            }
            catch (Exception ex)
            {
                throw new Exception($"GetSpaceReport fail:{ex.Message}");
            }
        }


        public async Task<IEnumerable<zp_get_sharetometop10_report_Result>> GetshareTop10Report(string empname)
        {
            try
            {
                var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
                optionsBuilder.UseSqlServer(_adapter.DbString);
                _entity = new clouddbContext(optionsBuilder.Options);

                var result = await _entity.zp_get_sharetometop10_report_Result(empname);

                return result;
            }
            catch (Exception ex)
            {

                throw new Exception($"GetshareTop10Report fail:{ex.Message}");
            }
        }

        public async Task<IEnumerable<zp_get_top10_file_size_report_Result>> GetTop10FileSizeReport(string empname)
        {
            try
            {
                var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
                optionsBuilder.UseSqlServer(_adapter.DbString);
                _entity = new clouddbContext(optionsBuilder.Options);

                var result = await _entity.zp_get_top10_file_size_report_Result(empname);

                return result;
            }
            catch (Exception ex)
            {
                throw new Exception($"GetTop10FileSizeReport fail:{ex.Message}");
            }
        }

        public async Task<IEnumerable<zp_get_recent_upload_report_Result>> GetRecentUploadReport(string empname)
        {
            try
            {
                var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
                optionsBuilder.UseSqlServer(_adapter.DbString);
                _entity = new clouddbContext(optionsBuilder.Options);

                var result = await _entity.zp_get_recent_upload_report_Result(empname);

                return result;
            }
            catch (Exception ex)
            {
                throw new Exception($"GetRecentUploadReport fail:{ex.Message}");
            }
        }

        //取得使用者權限
        public async Task<IEnumerable<zp_get_member_auth_detail_Result>> GetMemberAuthDetail(string empname,
            string rpath)
        {
            try
            {
                var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
                optionsBuilder.UseSqlServer(_adapter.DbString);
                _entity = new clouddbContext(optionsBuilder.Options);

                var result = await _entity.zp_get_member_auth_detail_Result(empname, rpath);

                return result;
            }
            catch (Exception ex)
            {
                throw new Exception($"GetMemberAuthDetail fail:{ex.Message}");
            }
        }


        //取得檔案detail 在dashboard
        public async Task<IEnumerable<zp_get_dashboard_get_files_Result>> GetFileDashboardDetail(
           string rpath)
        {
            try
            {
                var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
                optionsBuilder.UseSqlServer(_adapter.DbString);
                _entity = new clouddbContext(optionsBuilder.Options);

                var result = await _entity.zp_get_dashboard_get_files_Result(rpath);

                return result;
            }
            catch (Exception ex)
            {
                throw new Exception($"GetFileDashboardDetail fail:{ex.Message}");
            }
        }

        //check autoversion
        public async Task<IEnumerable<zp_check_autoversion_Result>> CheckAutoVersion(
         string rpath)
        {
            try
            {
                var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
                optionsBuilder.UseSqlServer(_adapter.DbString);
                _entity = new clouddbContext(optionsBuilder.Options);

                var result = await _entity.zp_check_autoversion_Result(rpath);

                return result;
            }
            catch (Exception ex)
            {

                throw new Exception($"CheckAutoVersion fail:{ex.Message}");
            }
        }


        public async Task<IEnumerable<zp_checkversion_give_filename_Result>> GiveFilename(
         string rpath, string filename)
        {
            try
            {
                var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
                optionsBuilder.UseSqlServer(_adapter.DbString);
                _entity = new clouddbContext(optionsBuilder.Options);

                var result = await _entity.zp_checkversion_give_filename_Result(rpath, filename);

                return result;
            }
            catch (Exception ex)
            {
                throw new Exception($"GiveFilename fail:{ex.Message}");
            }
        }



        public async Task<IEnumerable<zp_get_mangers_list_Result>> GetManagerList()
        {
            try
            {
                var optionsBuilder = new DbContextOptionsBuilder<clouddbContext>();
                optionsBuilder.UseSqlServer(_adapter.DbString);
                _entity = new clouddbContext(optionsBuilder.Options);

                var result = await _entity.zp_get_mangers_list_Result();

                return result;
            }
            catch (Exception ex)
            {
                throw new Exception($"GetManagerList fail:{ex.Message}");
            }
        }
        //zp_checkversion_give_filename_Result

    }
}
