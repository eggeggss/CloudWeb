﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using mirleCloudCore;


namespace mirleCloudCore.Common
{

    public enum LoginType
    {
        None,
        Assitant,
        Engineer,
        Manager,
    }

    public class CommonFunction
    {
       
        public CommonFunction()
        {
        }
        /// <summary>
        /// 判斷角色
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static Claim[] LoadClaims(string ename,string empno, LoginType logintype)
        {

            Claim[] claims;


            if (logintype == LoginType.Assitant)
            {
               
                claims = new[]
               {
                new Claim(ClaimTypes.Name, ename),
                new Claim(ClaimTypes.Actor,empno),
                new Claim(ClaimTypes.Role, "Assist"),
                };

                return claims;


            }
            else if (logintype == LoginType.Engineer)
            {

                claims = new[]
                {
                new Claim(ClaimTypes.Name, ename),
                new Claim(ClaimTypes.Actor,empno),
                new Claim(ClaimTypes.Role, "Users"),
                };
                return claims;
            }

            return null;

        }

    }

}
