﻿using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace mirleCloudCore.Common
{
    public class EmailService
    {
        private IWebHostEnvironment _Env;
        public EmailService(IWebHostEnvironment Env)
        {
            _Env = Env;
        }

        public Task SendInventUser(List<string> whos,string sender,string url)
        {
            var result = Task.Run(() =>
            {
                using (MailMessage emailMessage = new MailMessage())
                {

                    var path1 = $"{this._Env.WebRootPath}/pic/guild1.png";
                    var path2 = $"{this._Env.WebRootPath}/pic/guild2.png";
                    var path3 = $"{this._Env.WebRootPath}/pic/guild3.png";

                    var pathi1 = $"{this._Env.WebRootPath}/pic/iguild1.PNG";
                    var pathi2 = $"{this._Env.WebRootPath}/pic/iguild2.PNG";
                    var pathi3 = $"{this._Env.WebRootPath}/pic/iguild3.PNG";
                    var pathi4 = $"{this._Env.WebRootPath}/pic/iguild4.PNG";
                    var pathi5 = $"{this._Env.WebRootPath}/pic/iguild5.PNG";
                    var pathi6 = $"{this._Env.WebRootPath}/pic/iguild6.PNG";

                    string imagth = path1;
                    LinkedResource objLinkedRes = new LinkedResource(imagth,"image/png");
                    objLinkedRes.ContentId = "fuzzydev-logo";

                    string imagth2 = path2;
                    LinkedResource objLinkedRes2 = new LinkedResource(imagth2, "image/png");
                    objLinkedRes2.ContentId = "fuzzydev-logo2";

                    string imagth3 = path3;
                    LinkedResource objLinkedRes3 = new LinkedResource(imagth3, "image/png");
                    objLinkedRes3.ContentId = "fuzzydev-logo3";


                    string imagthi1 = pathi1;
                    LinkedResource objLinkedResi1 = new LinkedResource(imagthi1, "image/png");
                    objLinkedResi1.ContentId = "fuzzydev-logoi1";
                   
                    string imagthi2 = pathi2;
                    LinkedResource objLinkedResi2 = new LinkedResource(imagthi2, "image/png");
                    objLinkedResi2.ContentId = "fuzzydev-logoi2";

                    string imagthi3 = pathi3;
                    LinkedResource objLinkedResi3 = new LinkedResource(imagthi3, "image/png");
                    objLinkedResi3.ContentId = "fuzzydev-logoi3";

                    string imagthi4 = pathi4;
                    LinkedResource objLinkedResi4 = new LinkedResource(imagthi4, "image/png");
                    objLinkedResi4.ContentId = "fuzzydev-logoi4";

                    string imagthi5 = pathi5;
                    LinkedResource objLinkedResi5 = new LinkedResource(imagthi5, "image/png");
                    objLinkedResi5.ContentId = "fuzzydev-logoi5";

                    string imagthi6 = pathi6;
                    LinkedResource objLinkedResi6 = new LinkedResource(imagthi6, "image/png");
                    objLinkedResi6.ContentId = "fuzzydev-logoi6";



                    emailMessage.From = new MailAddress("mirlehq@mirle.com.tw", "mirlehq");


                    foreach (string who in whos)
                    {
                        emailMessage.To.Add(new MailAddress(who));
                    }


                    emailMessage.Subject = $"{sender} 邀請您加入工時系統";

                    
                    AlternateView objHTLMAltView = AlternateView.CreateAlternateViewFromString(
                               $@"<strong>Dear Sir</strong> <br> <br>
                                    <br/>

                                <table style='border: 3px #cccccc solid;width:30%;font-size:16px;' cellpadding='10' border='1'>
                                  <tr>
                                     <td colspan='2' align='center' >
                                         <div style='font-weight:bold;font-size:18px;'>工時系統邀請信</div>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                        <div style='font-size:14px;'>加入連結 </div>
                                    </td>
                                     <td>
                                       <a href='{url}' style='font-size:14px;'>工時系統</a>
                                       <span> (請用<strong>chrome,firefox,Edge</strong>開啟)</span>
                                    </td>
                                  </tr>
                                  <tr>
                                     <td> 
                                         <div style='font-size:14px;'> 注意事項 </div>
                                     </td>
                                     <td>
                                          <div style='font-size:14px;'>
                                          1.請用<strong>mirlehq</strong>帳號登入 <br/>
                                          2.請用<strong>直接人員</strong>身份登入 <br/>
                                          3.請用<strong>chrome,firefox,Edge</strong>開啟 (<strong style='color:red;'>ie不支援</strong>)
                                          </div>
                                     </td>
                                  </tr>
                                  <tr>
                                      <td>
                                           <div style='font-size:14px;'>信件來源</div>
                                      </td>
                                      <td>

                                           <div style='font-size:14px;'>經營管理中心資管處工時系統</div>

                                      </td>
                                  </tr>

                                </table>
                                <br/>
                                <br/>
                                <br/>
                                <br/>
                                <h3 style='color:red;'> 操作方式如下:</h3>
                                <br/>
                                <h3>1.使用mirlehq登入</h3>
                                <br/>
                                <img height='500' width='700' src='cid:fuzzydev-logo3' />
                                <br/>
                                <br/>
                                <h3>2.填工時</h3>
                                <br/>
                                <img height='500' width='900' src='cid:fuzzydev-logo' />
                                <br/>
                                <br/>

                                <h3>3.設定</h3>
                                <br/>
                                <img height='500' width='900' src='cid:fuzzydev-logo2' />
                                <br/>
                                <h3>4.手機版登入</h3>
                                <img height='800' width='400' src='cid:fuzzydev-logoi1' />
                                <br/>
                                <h3>5.填寫工時</h3>
                                <img height='800' width='400' src='cid:fuzzydev-logoi2' />
                                <br/>
                                <h3>6.選擇日期後填寫日班或加班工時</h3>
                                <img height='800' width='400' src='cid:fuzzydev-logoi3' />
                                <br/>
                                <h3>7.綠色表示工時填寫正確</h3>
                                <img height='800' width='400' src='cid:fuzzydev-logoi4' />
                                <br/>
                                <h3>8.送出</h3>
                                <img height='800' width='400' src='cid:fuzzydev-logoi5' />
                                <br/>
                                 ",
                                Encoding.GetEncoding(950), "text/html");

                    objHTLMAltView.LinkedResources.Add(objLinkedRes);

                    objHTLMAltView.LinkedResources.Add(objLinkedRes2);

                    objHTLMAltView.LinkedResources.Add(objLinkedRes3);

                    objHTLMAltView.LinkedResources.Add(objLinkedRes3);

                    objHTLMAltView.LinkedResources.Add(objLinkedResi1);
                    objHTLMAltView.LinkedResources.Add(objLinkedResi2);
                    objHTLMAltView.LinkedResources.Add(objLinkedResi3);
                    objHTLMAltView.LinkedResources.Add(objLinkedResi4);
                    objHTLMAltView.LinkedResources.Add(objLinkedResi5);
                    objHTLMAltView.LinkedResources.Add(objLinkedResi6);


                    emailMessage.AlternateViews.Add(objHTLMAltView);

                    emailMessage.BodyEncoding = Encoding.GetEncoding(950);

                    emailMessage.Priority = MailPriority.Normal;
                    emailMessage.IsBodyHtml = true;
                    using (SmtpClient MailClient = new SmtpClient("192.168.2.7", 25))
                    //using (SmtpClient MailClient = new SmtpClient("smtp.gmail.com", 587))
                    {
                        MailClient.EnableSsl = false;
                        MailClient.Send(emailMessage);
                    }
                }

            });

            return result;

        }


//        public Task SendForgetHours(IEnumerable<zp_get_member_forget_maintaint_hours_Result> hours,
//            string whoemail,string sender,string senderemail,string url )
//        {
//            var result = Task.Run(() =>
//            {
//                using (MailMessage emailMessage = new MailMessage())
//                {

//                    emailMessage.From = new MailAddress("mirlehq@mirle.com.tw", "mirlehq");

                    
//                    emailMessage.To.Add(new MailAddress(whoemail));

//                    if (!String.IsNullOrEmpty(senderemail))
//                    {
//                        emailMessage.CC.Add(new MailAddress(senderemail));
//                    }

//                    //for test
//                    emailMessage.Bcc.Add(new MailAddress("rogerroan@mirle.com.tw"));

//                    emailMessage.Subject = $"工時系統 - 提醒您維護工時";

//                    StringBuilder mailformat = new StringBuilder();

//                    mailformat.Append(@"
//                         <table style='border: 3px #cccccc solid;width:50%;font-size:16px;' cellpadding='10' border='1'>
//                         <tr style='background-color:#AED6F1;'>
//                              <td colspan='6' align='center' >
//                                      <div style='font-weight:bold;font-size:18px;'>以下日期請填寫工時</div>
//                              </td>
//                         </tr>
//                         <tr>
//                               <td>年月</td>
//                               <td>週</td>
//                               <td>星期</td>
//                               <td>日期</td>
//                               <td>工時</td>
//                               <td>請假</td>
//                        </tr>
//                     ");

//                    foreach (var item in hours)
//                    {
//                        mailformat.Append(
//                        @$"<tr>
//                                <td>{item.yymm}</td>
//                                <td>{item.week}</td>
//                                <td>{item.weekday1}</td>
//                                <td>{item.date1}</td>
//                                <td>{item.workhour}</td>
//                                <td>{item.holiayhour}</td>
//                        </tr>");
//                    }

//                    mailformat.Append(@"
//                    <tr>
//                       <td colspan='6' align='center'>此信件由經營管理中心資管處工時系統寄出</td>
//                    </tr>
//                    </table>");

//                    AlternateView objHTLMAltView = AlternateView.CreateAlternateViewFromString(
//                               $@"<strong>Dear Sir</strong> <br> <br>
//<a href='{url}'> 工時系統連結 (請用chrome/firefox/edge開啟)</a>   

//<br/>
//<br/>
//                                    {mailformat.ToString()}

//                                 ",
//                                Encoding.GetEncoding(950), "text/html");


//                    emailMessage.AlternateViews.Add(objHTLMAltView);

//                    emailMessage.BodyEncoding = Encoding.GetEncoding(950);

//                    emailMessage.Priority = MailPriority.Normal;
//                    emailMessage.IsBodyHtml = true;
//                    using (SmtpClient MailClient = new SmtpClient("192.168.2.7", 25))
//                    //using (SmtpClient MailClient = new SmtpClient("smtp.gmail.com", 587))
//                    {
//                        MailClient.EnableSsl = false;
//                        MailClient.Send(emailMessage);
//                    }
//                }

//            });

//            return result;

//        }


//        public Task SendForgetHoursGroup(IEnumerable<zp_get_member_forget_maintaint_hours_by_week_for_group_Result> hours,
//           string whoemail, string sender, string senderemail, string url,string groupname,string assitantsemail)
//        {
//            var result = Task.Run(() =>
//            {
//                using (MailMessage emailMessage = new MailMessage())
//                {

//                    emailMessage.From = new MailAddress("mirlehq@mirle.com.tw", "mirlehq");

//                    //emailMessage.To.Add(new MailAddress("rogerroan@mirle.com.tw"));
                    
//                    foreach (var user in whoemail.Split(","))
//                    {
//                        if (!String.IsNullOrEmpty(user) || user!="" )
//                        {
//                            emailMessage.To.Add(new MailAddress(user));
//                        }
                           
//                    }

//                    if (!String.IsNullOrEmpty(assitantsemail))
//                    {
//                        emailMessage.CC.Add(new MailAddress(assitantsemail));
//                    }

//                    //for test
//                    emailMessage.Bcc.Add("rogerroan@mirle.com.tw");

//                    //emailMessage.Subject = $"工時系統 - 上週未維護工時 ({groupname})";
//                    emailMessage.Subject = $"工時系統 - 上週未維護工時 ({groupname})";


//                    StringBuilder mailformat = new StringBuilder();

//                    mailformat.Append(@"
//                         <table style='border: 3px #cccccc solid;width:50%;font-size:16px;' cellpadding='10' border='1'>
//                         <tr style='background-color:#0674BE;color:white;'>
//                              <td colspan='4' align='center' >
//                                      <div style='font-weight:bold;font-size:18px;letter-space:5px;'>尚未維護工時清單</div>
//                              </td>
//                         </tr>
//                         <tr>
//                               <td>姓名</td>
//                               <td>年月</td>
//                               <td>週</td>                              
//                               <td>工時</td>
//                        </tr>
//                     ");

//                    foreach (var item in hours)
//                    {
//                        mailformat.Append(
//                        @$"<tr>
//                                <td>{item.empcname}</td>
//                                <td>{item.yymm}</td>
//                                <td>{item.week}</td>
//                                <td>{item.wtot}</td>
//                        </tr>");
//                    }

//                    mailformat.Append(@"
//                    <tr>
//                       <td colspan='4' align='center'>此信件由經營管理中心資管處工時系統寄出</td>
//                    </tr>
//                    </table>");

//                    AlternateView objHTLMAltView = AlternateView.CreateAlternateViewFromString(
//                               $@"<strong>Dear Sir</strong> <br> <br>
//<a href='{url}'> 工時系統連結 (請用chrome/firefox/edge開啟)</a>   

//<br/>
//<br/>
//                                    {mailformat.ToString()}

//                                 ",
//                                Encoding.GetEncoding(950), "text/html");


//                    emailMessage.AlternateViews.Add(objHTLMAltView);

//                    emailMessage.BodyEncoding = Encoding.GetEncoding(950);

//                    emailMessage.Priority = MailPriority.Normal;
//                    emailMessage.IsBodyHtml = true;
//                    using (SmtpClient MailClient = new SmtpClient("192.168.2.7", 25))
//                    //using (SmtpClient MailClient = new SmtpClient("smtp.gmail.com", 587))
//                    {
//                        MailClient.EnableSsl = false;
//                        MailClient.Send(emailMessage);
//                    }
//                }

//            });

//            return result;

//        }


        public Task Send(string who, string url)
        {
            var result = Task.Run(() =>
            {
                //who = "eggeggss@gmail.com";
                using (MailMessage emailMessage = new MailMessage())
                {
                    //var mailacc = System.Configuration.ConfigurationManager.AppSettings["mailacc"];
                    //var mailpass = System.Configuration.ConfigurationManager.AppSettings["mailpass"];

                    emailMessage.From = new MailAddress("mirlehq@mirle.com.tw", "mirlehq");
                    emailMessage.To.Add(new MailAddress(who));
                    emailMessage.Subject = "MIRLEHQ重設密碼";

                    //string imagth = System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/guideline.png");
                    //LinkedResource objLinkedRes = new LinkedResource(imagth);
                    //objLinkedRes.ContentId = "fuzzydev-logo";

                    //string imagth2 = System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/guideline2.png");
                    //LinkedResource objLinkedRes2 = new LinkedResource(imagth2);
                    //objLinkedRes2.ContentId = "fuzzydev-logo2";

                    AlternateView objHTLMAltView = AlternateView.CreateAlternateViewFromString(
                               $@"<strong>Hi {who.Replace("@mirle.com.tw", ":")}</strong> <br> <br>
                                 請變更您的密碼:  {url} <br/><br/>
                                 <span style='color:red;font-weight:bold;'>此連結5分鐘內有效</span>
                                 ",
                                Encoding.GetEncoding(950), "text/html");


                    //objHTLMAltView.LinkedResources.Add(objLinkedRes);
                    //objHTLMAltView.LinkedResources.Add(objLinkedRes2);

                    emailMessage.AlternateViews.Add(objHTLMAltView);

                    emailMessage.BodyEncoding = Encoding.GetEncoding(950);

                    emailMessage.Priority = MailPriority.Normal;
                    emailMessage.IsBodyHtml = true;
                    using (SmtpClient MailClient = new SmtpClient("192.168.2.7", 25))
                    //using (SmtpClient MailClient = new SmtpClient("smtp.gmail.com", 587))
                    {
                        MailClient.EnableSsl = false;
                        MailClient.Send(emailMessage);
                    }
                }

            });

            return result;

        }


        public Task SendExpiredNotify(string who, string who2, string url, int remain)
        {
            var result = Task.Run(() =>
            {
                //who = "eggeggss@gmail.com";
                using (MailMessage emailMessage = new MailMessage())
                {
                    //var mailacc = System.Configuration.ConfigurationManager.AppSettings["mailacc"];
                    //var mailpass = System.Configuration.ConfigurationManager.AppSettings["mailpass"];

                    emailMessage.From = new MailAddress("mirlehq@mirle.com.tw", "mirlehq");
                    emailMessage.To.Add(new MailAddress(who));
                    emailMessage.Subject = $"MIRLEHQ密碼即將過期 剩{remain}天";

                    AlternateView objHTLMAltView = AlternateView.CreateAlternateViewFromString(
                               $@"<strong>Hi {who2}</strong>
                                  <br/>
                                  <strong style='color:red;'>技術中心通知</strong>
                                  <br/>
                                  <strong style='color:red;'>★★再次提醒★★為落實資訊安全政策，請定期變更mirlehq網域密碼。</strong>
                                  <br/> <br/>
                                 MIRLEHQ密碼還剩 {remain} 天過期
                                 <br/> <br/>
                                 <a href='{url}'>請變更您的密碼</a> <br/><br/>
                                 <br/> <br/>

                                 ",
                                Encoding.GetEncoding(950), "text/html");


                    //objHTLMAltView.LinkedResources.Add(objLinkedRes);
                    //objHTLMAltView.LinkedResources.Add(objLinkedRes2);

                    emailMessage.AlternateViews.Add(objHTLMAltView);

                    emailMessage.BodyEncoding = Encoding.GetEncoding(950);

                    emailMessage.Priority = MailPriority.Normal;
                    emailMessage.IsBodyHtml = true;
                    using (SmtpClient MailClient = new SmtpClient("192.168.2.7", 25))
                    //using (SmtpClient MailClient = new SmtpClient("smtp.gmail.com", 587))
                    {
                        MailClient.EnableSsl = false;
                        MailClient.Send(emailMessage);
                    }
                }

            });

            return result;

        }


        public Task SendToPublicPassword(List<string> whos, string month, string password)
        {
            var result = Task.Run(() =>
            {
                using (MailMessage emailMessage = new MailMessage())
                {
                    
                    emailMessage.From = new MailAddress("mirlehq@mirle.com.tw", "mirlehq");


                    foreach (string who in whos)
                    {
                        emailMessage.To.Add(new MailAddress(who));
                    }


                    emailMessage.Subject = $"本月雲端與EF部門公用帳號密碼為:{password}";


                    AlternateView objHTLMAltView = AlternateView.CreateAlternateViewFromString(
                               $@"<strong>Dear All</strong> <br> <br>
                                  <br/>
                                  <strong style='color:red;'>技術中心通知<strong><br/> 
                                  <strong style='color:red;'>★★提醒★★為落實資訊安全政策，本月({month}月)雲端與EF部門公用帳號密碼為:<span style='color:blue;'>{password}</span>。</strong>

                                 ",
                                Encoding.GetEncoding(950), "text/html");


                    emailMessage.AlternateViews.Add(objHTLMAltView);

                    emailMessage.BodyEncoding = Encoding.GetEncoding(950);

                    emailMessage.Priority = MailPriority.Normal;
                    emailMessage.IsBodyHtml = true;
                    using (SmtpClient MailClient = new SmtpClient("192.168.2.7", 25))
                    //using (SmtpClient MailClient = new SmtpClient("smtp.gmail.com", 587))
                    {
                        MailClient.EnableSsl = false;
                        MailClient.Send(emailMessage);
                    }
                }

            });

            return result;

        }

    }

}
