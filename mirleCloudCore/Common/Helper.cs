﻿using dalmirleCloudCore.Meta;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace mirleCloudCore.Common
{

    public class Helper
    {
        public static void GetDirectory(string sDir, FileData parent,string drivetype)
        {
            try
            {
                int id = 1;

                foreach (string d in Directory.GetDirectories(sDir))
                {
                    id++;
                    DirectoryInfo d1 = new DirectoryInfo(d);
              
                    FileData filedata = null;

                    filedata = new FileData
                    {
                        id = "ok12",
                        name = d1.Name,
                        icons = new icon
                        {
                            defaults = new List<string> { "<i class='fa fa-folder'></i>", "text-orange-d1" }
                        },
                        children = new List<FileData>(),
                        path = d,
                        drivetype= drivetype,
                    };

                    parent.children.Add(filedata);

                    GetDirectory(d, filedata, drivetype);

                }
            }
            catch (System.Exception excpt)
            {
                Console.WriteLine(excpt.Message);
            }
        }

        public static List<DirectoryFile> GetDirectoryWithFile(string sDir)
        {
            List<DirectoryFile> ds = new List<DirectoryFile>();
            try
            {

                foreach (string d in Directory.GetDirectories(sDir))
                {

                    DirectoryInfo d1 = new DirectoryInfo(d);

                    var size = d1.EnumerateFiles("*.*", SearchOption.AllDirectories).Sum(fi => fi.Length);
                    var newsize = (size == 0) ? "0" : Math.Round((size / 1024 / 1024m),2).ToString();
                    
                    ds.Add(new DirectoryFile
                    {
                        Name = d1.Name,
                        Path = d,
                        Filetype = Filetype.Directory,
                        Size = newsize,//newsize.ToString(),
                        CreateTime = d1.CreationTime,
                        UpdateTime = d1.LastWriteTime,
                        Extension=d1.Extension,
                    });
                }

                foreach (string f in Directory.GetFiles(sDir))
                {
                    FileInfo f1 = new FileInfo(f);
                    ds.Add(new DirectoryFile
                    {
                        Name = f1.Name,
                        Path = f,
                        Filetype = Filetype.File,
                        Size = Convert.ToString(f1.Length / 1024m / 1024m).Substring(0, 5),
                        CreateTime = f1.CreationTime,
                        UpdateTime = f1.LastWriteTime,
                        Extension=f1.Extension,
                    });
                }


            }
            catch (System.Exception excpt)
            {
                Console.WriteLine(excpt.Message);
            }
            return ds;
        }

        public static void GetDirectoryParent(string sDir,ref List<FileData> breadcrumb,int level)
        {
            DirectoryInfo d1 = new DirectoryInfo(sDir);

            if (d1.Parent != null)
            {
                var d2 = (DirectoryInfo)d1.Parent;

                breadcrumb.Add(new FileData
                {
                    name = d2.Name,
                    path = d2.FullName,
                    parent=new List<FileData>(),
                    id=level.ToString(),
                });


                if (d2.Name.Equals("clouddata"))
                {
                    return;
                }

                GetDirectoryParent(d2.FullName,ref breadcrumb,level+1);

            }

        }

        public static void DeleteDirectory(DirectoryInfo baseDir)
        {
            if (!baseDir.Exists)
                return;

            foreach (var dir in baseDir.EnumerateDirectories())
            {
                DeleteDirectory(dir);
            }

            var files = baseDir.GetFiles();
            foreach (var file in files)
            {
                file.IsReadOnly = false;
                file.Delete();
            }

            baseDir.Delete(true);

        }
        public static string CheckWebroot(IWebHostEnvironment _env, string empname)
        {
            var root = _env.WebRootPath;
            var share = $@"{root}/share";
            var memberpath = $"{share}/{empname}/clouddata";
if (!Directory.Exists(memberpath))
            {
                Directory.CreateDirectory(memberpath);
            }
            
            //Helper.GetDirectory(memberpath);

            return memberpath;
        }




    }
}
