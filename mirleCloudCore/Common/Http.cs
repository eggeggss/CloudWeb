﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using dalmirleCloudCore.Meta;
using Newtonsoft.Json;


namespace mirleCloudCore
{
    public class Http
    {

        public async static Task<aduserResult> AdLogin(aduser user)
        {
            string url = "https://eip.mirle.com.tw:82/MirleAPI/app/login/ad";
            //string url = "http://eggeggss.ddns.me:8102/MirleAPI/app/login/ad";

            HttpClient client = new HttpClient();

            string json = JsonConvert.SerializeObject(user);

            StringContent content = new StringContent(json, Encoding.UTF8, "application/json");

            var response = await client.PostAsync(url, content);

            /*
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
                throw new Exception("Status Code Error");
            */
            var result = await response.Content.ReadAsStringAsync();
            /*
            if (result.Equals("null"))
                throw new Exception("Login Fail");
            */
            var aduserresult = JsonConvert.DeserializeObject<aduserResult>(result);

            System.Diagnostics.Debug.WriteLine(result, "debug");

            return aduserresult;
        }
    }
}
