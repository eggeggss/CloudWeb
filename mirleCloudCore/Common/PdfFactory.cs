﻿using bllmirleCloudCore;
using dalmirleCloudCore.Interface;
using dalmirleCloudCore.Meta;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Aspose.Cells;
using System.IO;
using Hangfire;
using System.ComponentModel;
using Hangfire.Server;
using System.Net.Http;

namespace mirleCloudCore.Common
{
    public enum PdfType
    {
        pptx,
        xlsx,
        docx
    }

    public class PdfFactory
    {

        private IWebHostEnvironment _env;
        private IOptionsSnapshot<Profile> _profile;
        private IOFileInterface _iofile;
        private IODirectoryInterface _iodirectory;
        private mirleCloudCorebll _bllmirleCloudCore;
        private mirleCloudCoreReport _reportmirleCloudCore;
        private IBackgroundJobClient _jobClient;

        public PdfFactory(
            IWebHostEnvironment env,
            IOptionsSnapshot<Profile> profile,
            IOFileInterface iofile,
            IODirectoryInterface iodirectory,
            mirleCloudCorebll bllmirleCloudCore,
            mirleCloudCoreReport reportmirleCloudCore,
            IBackgroundJobClient jobClient)
        {
            _env = env;
            _profile = profile;
            _iofile = iofile;
            _iodirectory = iodirectory;
            _bllmirleCloudCore = bllmirleCloudCore;
            _reportmirleCloudCore = reportmirleCloudCore;
            _jobClient = jobClient;
        }

        public async Task GeneratePDF(string abpath, PdfType type)
        {
            await Task.Delay(3000);

            if (type==PdfType.pptx)
               _jobClient.Enqueue(() => this.HandlePPTx(null, JobCancellationToken.Null,abpath));
        }

        [Queue("default")]
        [DisplayName("產生PPTX預覽檔案")]
        [Description("產生PPTX預覽檔案")]
        [AutomaticRetry(Attempts = 10)]   //自動重試
        //[DisableConcurrentExecution(90)] //禁止使用並行
        public async Task HandlePPTx(PerformContext context,
            IJobCancellationToken cancellationToken,string abpath)
        {
            try
            {

               await  PdfFactory.GeneratePDF(_env, _bllmirleCloudCore, abpath);

            }
            catch(Exception ex)
            {
                throw new Exception($"HandlePPTx Fail : {ex.Message}");
            }
            
        }

        public static async Task GeneratePDF(IWebHostEnvironment _env,
          mirleCloudCorebll _bllmirleCloudCore,
          string rpath)
        {
            var root = _env.WebRootPath;
            var temproot = $@"{root}/share/temp";
            var temppath = temproot;

            string guid = Guid.NewGuid().ToString();

            string aabpath = dalmirleCloudCore.Common.Common.RelativeToAbsolute(root, rpath);

            FileInfo f = new FileInfo(aabpath);

            if (f.Exists == true)
            {
                if (f.Extension.Equals(".ppt") || f.Extension.Equals(".pptx"))
                {
                    Aspose.Slides.Presentation presentation = new Aspose.Slides.Presentation(aabpath);

                    string preview = $"{guid}.pdf";

                    string target = Path.Combine(temppath, preview);

                    presentation.Save(target, Aspose.Slides.Export.SaveFormat.Pdf);

                    var doc = await _bllmirleCloudCore.FindFile(rpath);

                    if (doc != null)
                    {
                        doc.Preview = preview;

                        await _bllmirleCloudCore.UpdateFile(doc);

                    }
                }
                else if (f.Extension.Equals(".doc") || f.Extension.Equals(".docx"))
                {
                    var doc = await _bllmirleCloudCore.FindFile(rpath);
                    var idfile = doc.IdFile;
                    if (doc != null)
                    {
                        HttpClient client = new HttpClient();

                        var url = $"http://192.168.3.156:8088/PatentWeb01/emeeting/ConvertToPdf/{idfile}";

                        await client.GetAsync(url);
                    }
                }


            }
            else
            {
                throw new Exception("File not found");
            }
        }


    }
}
