﻿using bllmirleCloudCore;
using dalmirleCloudCore.Interface;
using dalmirleCloudCore.Meta;
using Hangfire;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using mirleCloudCore.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace mirleCloudCore.Controllers
{
    [Route("Data")]
    [Authorize(Roles = "Assist,Users")]
    public class DataController : Controller
    {
        protected IWebHostEnvironment _env;

        private IOptionsSnapshot<Profile> _profile;
        private readonly IOFileInterface _iofile;
        private readonly IODirectoryInterface _iodirectory;

        public string empname { get; set; }

        public string empno { get; set; }

        private mirleCloudCorebll _bllmirleCloudCore;
        private mirleCloudCoreReport _reportmirleCloudCore;
        private PdfFactory _pdffacetory;
        private IBackgroundJobClient _backgroundJobs;
        public string  Root { get; set; }

        public DataController(IWebHostEnvironment env,
            IOptionsSnapshot<Profile> profile,
            IOFileInterface iofile,
            IODirectoryInterface iodirectory,
            mirleCloudCorebll bllmirleCloudCore,
            mirleCloudCoreReport reportmirleCloudCore, 
            PdfFactory pdffactory,
            IBackgroundJobClient backgroundJobs
            )
        {
            _env = env;
            _profile = profile;
            _iofile = iofile;
            _iodirectory = iodirectory;
            //empname = "rogerroan";
            _bllmirleCloudCore = bllmirleCloudCore;
            _reportmirleCloudCore = reportmirleCloudCore;
            Root = _profile.Value.Root;
            _pdffacetory = pdffactory;
            _backgroundJobs = backgroundJobs;
        }
        [HttpGet("Index")]
        public IActionResult Index()
        {
            if (User.Claims.Count() > 0)
            {
                empname = User?.Claims?.FirstOrDefault(e => e.Type == ClaimTypes.Name).Value?.ToString();
                empno = User?.Claims?.FirstOrDefault(e => e.Type == ClaimTypes.Actor).Value?.ToString();
            }


            ViewBag.empname = empname;
            return View();
        }

        [HttpGet("GetTreeData")]
        public IActionResult GetTreeData()
        {

            if (User.Claims.Count() > 0)
            {
                empname = User?.Claims?.FirstOrDefault(e => e.Type == ClaimTypes.Name).Value?.ToString();
                empno = User?.Claims?.FirstOrDefault(e => e.Type == ClaimTypes.Actor).Value?.ToString();
            }


            var memberpath = Helper.CheckWebroot(_env, empname);
            string drivetype = "我的雲端硬碟";
            var data0 = new FileData()
            {
                id = "abc",
                name = "MyDrive",
                icons = new icon
                {
                    defaults = new List<string> { "<i class='fas fa-cloud'></i>", "text-blue-m1" }
                },
                children = new List<FileData> { },
                path = memberpath,
                drivetype= drivetype

            };

            Helper.GetDirectory(memberpath, data0, drivetype);

            var datas = JsonConvert.SerializeObject(data0);

            return Json(datas);
        }

        [HttpGet("GetTreeData2")]
        public async Task<IActionResult> GetShareToOtherTreeData()
        {

            if (User.Claims.Count() > 0)
            {
                empname = User?.Claims?.FirstOrDefault(e => e.Type == ClaimTypes.Name).Value?.ToString();
                empno = User?.Claims?.FirstOrDefault(e => e.Type == ClaimTypes.Actor).Value?.ToString();
            }

            var sharelist = await _reportmirleCloudCore.GetShareToOtherTree(empname);
            string abpath = "";
            List<FileData> treelist = new List<FileData>();
            string drivetype = "我與別人分享";
            FileData data0;
            foreach (var item in sharelist)
            {
                abpath= dalmirleCloudCore.Common.Common.RelativeToAbsolute(Root,item.relativepath);

                data0 = new FileData()
                {
                    id = "abc",
                    name = item.docname,
                    icons = new icon
                    {
                        defaults = new List<string> { "<i class='fa fa-folder'></i>", "text-orange-d1" }
                    },
                    children = new List<FileData> { },
                    path = abpath,
                    drivetype = drivetype,

                };

                Helper.GetDirectory(abpath, data0, drivetype);
                treelist.Add(data0);
            }


            FileData dataroot = new FileData()
            {
                id = "abc",
                name = "ShareWithOthers",
                icons = new icon
                {
                    defaults = new List<string> { "<i class='fas fa-cloud'></i>", "text-blue-m1" }
                },
                children = treelist,
                path = "",
                drivetype=drivetype,
            };


            var datas = JsonConvert.SerializeObject(dataroot);


            return Json(datas);
        }

        [HttpGet("GetTreeData3")]
        public async Task<IActionResult> GetShareToMeTreeData()
        {

            if (User.Claims.Count() > 0)
            {
                empname = User?.Claims?.FirstOrDefault(e => e.Type == ClaimTypes.Name).Value?.ToString();
                empno = User?.Claims?.FirstOrDefault(e => e.Type == ClaimTypes.Actor).Value?.ToString();
            }

            var sharelist = await _reportmirleCloudCore.GetShareToMeTree(empname);
            string abpath = "";
            List<FileData> treelist = new List<FileData>();
            string drivetype = "別人與我分享";
            FileData data0;
            foreach (var item in sharelist)
            {
                abpath = dalmirleCloudCore.Common.Common.RelativeToAbsolute(Root, item.relativepath);

                data0 = new FileData()
                {
                    id = "abc",
                    name = item.docname2,
                    icons = new icon
                    {
                        defaults = new List<string> { "<i class='fa fa-folder'></i>", "text-orange-d1" }
                    },
                    children = new List<FileData> { },
                    path = abpath,
                    drivetype = drivetype,

                };

                Helper.GetDirectory(abpath, data0, drivetype);
                treelist.Add(data0);
            }


            FileData dataroot = new FileData()
            {
                id = "abc",
                name = "MyDrive",
                icons = new icon
                {
                    defaults = new List<string> { "<i class='fas fa-cloud'></i>", "text-blue-m1" }
                },
                children = treelist,
                path = "",
                drivetype = drivetype,
            };


            var datas = JsonConvert.SerializeObject(dataroot);


            return Json(datas);
        }

        [HttpGet("GetTreeForDashboard/{top}")]
        public async Task<IActionResult> GetTreeForDashboard(int? top)
        {

            if (User.Claims.Count() > 0)
            {
                empname = User?.Claims?.FirstOrDefault(e => e.Type == ClaimTypes.Name).Value?.ToString();
                empno = User?.Claims?.FirstOrDefault(e => e.Type == ClaimTypes.Actor).Value?.ToString();
            }

            int topnew = 0;

            if (top == null)
                topnew = 0;

            topnew = (int)top;

            var sharelist = await _reportmirleCloudCore.GetShareToMeTreeForDashBoard(empname,topnew);

            ViewBag.top = topnew;

            return PartialView("_GetTreeForDashboard", sharelist);
        }

        [HttpGet("GetTreeForDashboardForManager/{top}")]
        public async Task<IActionResult> GetTreeForDashboardForManager(int? top)
        {

            if (User.Claims.Count() > 0)
            {
                empname = User?.Claims?.FirstOrDefault(e => e.Type == ClaimTypes.Name).Value?.ToString();
                empno = User?.Claims?.FirstOrDefault(e => e.Type == ClaimTypes.Actor).Value?.ToString();
            }

            int topnew = 0;

            if (top == null)
                topnew = 0;

            topnew = (int)top;

            var sharelist = await _reportmirleCloudCore.GetShareToMeTreeForManagerDashBoard(empname, topnew);

            //var sharelist = await _reportmirleCloudCore.GetShareToMeTreeForDashBoard(empname, topnew);

            ViewBag.top = topnew;

            return PartialView("_GetTreeForDashboardForManager", sharelist);
        }

        /// <summary>
        /// tree view在用的
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        [HttpPost("GetFolderDetail")]
        [HttpPost("GetFolderDetail/{type}")]
        public async  Task<IActionResult> GetFolderDetail(postfolderobj obj,int? type)
        {
            int type_new = 0;
            if (type != null)
            {
                type_new = (int)type;
            }

            if (User.Claims.Count() > 0)
            {
                empname = User?.Claims?.FirstOrDefault(e => e.Type == ClaimTypes.Name).Value?.ToString();
                empno = User?.Claims?.FirstOrDefault(e => e.Type == ClaimTypes.Actor).Value?.ToString();
            }

            ViewBag.path = obj.Path;
            DirectoryInfo d1 = new DirectoryInfo(obj.Path);
            var f1 = new FileData
            {
                name = d1.Name,
                path = obj.Path,
                parent = new List<FileData>(),
            };
            List<FileData> breadcrumb = new List<FileData>();

            breadcrumb.Add(f1);

            //取得麵包屑
            Helper.GetDirectoryParent(obj.Path, ref breadcrumb, 0);

            var newbreadcrumb = breadcrumb.OrderByDescending(e => e.id).ToList();

            ViewBag.breadcrumb = newbreadcrumb;

            var model = Helper.GetDirectoryWithFile(obj.Path);

            var data = _profile.Value;
            ViewBag.rootpath = data.Root;

            ViewBag.empname = empname;

            if (type_new == 0)
            {
                return PartialView("_GetFolderDetail", model);
            }
            else
            {
                //取得該成員的目錄權限
                var memberAuth = await _reportmirleCloudCore.GetMemberAuthDetail(empname, obj.Path);
                ViewBag.memberAuth = memberAuth.FirstOrDefault();

                return PartialView("_GetFolderDetailAuth", model);
            }
                
        }


        [HttpPost("GetFoderDetailByDashboard")]
        public async Task<IActionResult> GetFoderDetailByDashboard(postfolderobj obj)
        {

            if (User.Claims.Count() > 0)
            {
                empname = User?.Claims?.FirstOrDefault(e => e.Type == ClaimTypes.Name).Value?.ToString();
                empno = User?.Claims?.FirstOrDefault(e => e.Type == ClaimTypes.Actor).Value?.ToString();
            }

            var model= await _reportmirleCloudCore.GetFileDashboardDetail(obj.Path);

            return PartialView("_GetFoderDetailByDashboard",model);

        }


        [HttpPost("uploadfile")]
        public async Task<IActionResult> UploadFile(IFormFile[] uploadImage, string path,
            string keyword,string autoversion)
        {
            try
            {
                if (User.Claims.Count() > 0)
                {
                    empname = User?.Claims?.FirstOrDefault(e => e.Type == ClaimTypes.Name).Value?.ToString();
                    empno = User?.Claims?.FirstOrDefault(e => e.Type == ClaimTypes.Actor).Value?.ToString();
                }

                await _iodirectory.CreateDiretory(path, empname);

                string filePath = "";
                foreach (var item in uploadImage)
                {
                    if (item.Length > 0)
                    {
                        var fileobj=await _reportmirleCloudCore.GiveFilename(path, item.FileName);

                        if (fileobj.Count() ==0)
                        {
                            throw new Exception("filename 無法取得");
                        }

                        var filename = fileobj.FirstOrDefault().filename;

                        filePath = Path.Combine(path, filename);
                        //filePath = Path.Combine(path, item.FileName);

                        using (var stream = System.IO.File.Create(filePath))
                        {
                            await item.CopyToAsync(stream);
                        }
                       
                        await _iofile.CreateFile(filePath, empname,keyword);

                        FileInfo f = new FileInfo(filePath);

                        //call hanefire
                        if (f.Extension.ToLower().Equals(".pptx") || f.Extension.ToLower().Equals(".ppt"))
                        {
                            await _pdffacetory.GeneratePDF(filePath, PdfType.pptx);
                        }
                        
                    }
                }

                return Content("File upload completed!");
            }
            catch (Exception ex)
            {
                return StatusCode(400, ex.Message);
            }
        }

        [HttpPost("deletefile")]
        public IActionResult DeleteFile(postfolderobj obj)
        {
            try
            {
                if (User.Claims.Count() > 0)
                {
                    empname = User?.Claims?.FirstOrDefault(e => e.Type == ClaimTypes.Name).Value?.ToString();
                    empno = User?.Claims?.FirstOrDefault(e => e.Type == ClaimTypes.Actor).Value?.ToString();
                }

                var path = System.IO.Path.Combine(obj.Path, obj.Name);
                _iofile.DeleteFile(path, empname);

                return StatusCode(204);
            }
            catch (Exception ex)
            {
                return StatusCode(400, ex.Message);
            }
        }

        [HttpPost("deletedirectory")]
        public async Task<IActionResult> DeleteDirectory(postfolderobj obj)
        {
            try
            {
                if (User.Claims.Count() > 0)
                {
                    empname = User?.Claims?.FirstOrDefault(e => e.Type == ClaimTypes.Name).Value?.ToString();
                    empno = User?.Claims?.FirstOrDefault(e => e.Type == ClaimTypes.Actor).Value?.ToString();
                }

                var path = System.IO.Path.Combine(obj.Path, obj.Name);
                await _iodirectory.DeleteDirectory(path, empname);

                return StatusCode(204);
            }
            catch (Exception ex)
            {
                return StatusCode(400, ex.Message);
            }
        }

        [HttpPost("createdirectory")]
        public async Task<IActionResult> CreateDirectory(postfolderobj obj)
        {
            try
            {
                if (User.Claims.Count() > 0)
                {
                    empname = User?.Claims?.FirstOrDefault(e => e.Type == ClaimTypes.Name).Value?.ToString();
                    empno = User?.Claims?.FirstOrDefault(e => e.Type == ClaimTypes.Actor).Value?.ToString();
                }

                var path = System.IO.Path.Combine(obj.Path, obj.Name);
                await _iodirectory.CreateDiretory(path, empname);
                return StatusCode(204);
            }
            catch (Exception ex)
            {
                return StatusCode(400, ex.Message);
            }
        }

        [HttpGet("GetPartialMemberOption")]

        public async Task<IActionResult> GetPartialMemberOption()
        {
            //get all member
            var model = await _reportmirleCloudCore.GetMembers();

            return PartialView("_GetPartialMemberOption", model);
        }

        [HttpGet("GetPartialDeptOption")]
        public async Task<IActionResult> GetPartialDeptOption()
        {
            var model = await _reportmirleCloudCore.GetDeptNos();

            return PartialView("_GetPartialDeptOption", model);
        }

        [HttpPost("GetPartialShareMember")]
        public async Task<IActionResult> GetPartialShareMember(postfolderobj obj)
        {

            var model = await _reportmirleCloudCore.GetShareFolderMember(obj.Path);

            var doc = await _bllmirleCloudCore.FindDocument(obj.Path);

            ViewBag.Document = doc;

            return PartialView("_GetPartialShareMember", model);
        }

        [HttpPost("GetPartialShareDept")]
        public async Task<IActionResult> GetPartialShareDept(postfolderobj obj)
        {
            var model = await _reportmirleCloudCore.GetShareFolderDept(obj.Path);

            var doc = await _bllmirleCloudCore.FindDocument(obj.Path);

            ViewBag.Document = doc;

            return PartialView("_GetPartialShareDept", model);
        }

        [HttpPost("UpdateShareMemberAuth")]
        public async Task<IActionResult> UpdateShareMemberAuth(updatememberauth obj)
        {
            try
            {
                if (User.Claims.Count() > 0)
                {
                    empname = User?.Claims?.FirstOrDefault(e => e.Type == ClaimTypes.Name).Value?.ToString();
                    empno = User?.Claims?.FirstOrDefault(e => e.Type == ClaimTypes.Actor).Value?.ToString();
                }
                switch (obj.type)
                {
                    case "upload":
                        await _bllmirleCloudCore.UpdateMemberAllowUpload(obj.id_doc_acc, obj.check,empname);
                        break;
                    case "download":
                        await _bllmirleCloudCore.UpdateMemberAllowDownload(obj.id_doc_acc, obj.check, empname);
                        break;
                    case "folder":
                        await _bllmirleCloudCore.UpdateMemberAllowFolder(obj.id_doc_acc, obj.check, empname);
                        break;
                    case "delfolder":
                        await _bllmirleCloudCore.UpdateMemberAllowDelFolder(obj.id_doc_acc, obj.check, empname);
                        break;
                    case "delfile":
                        await _bllmirleCloudCore.UpdateMemberAllowDelFile(obj.id_doc_acc, obj.check, empname);
                        break;
                    case "share":
                        await _bllmirleCloudCore.UpdateMemberAllowShare(obj.id_doc_acc, obj.check, empname);
                        break;
                    default:
                        break;
                }

                return StatusCode(204);
            }catch(Exception ex)
            {
                return StatusCode(400, ex.Message);
            }
        }

        [HttpPost("UpdateShareDeptAuth")]
        public async Task<IActionResult> UpdateShareDeptAuth(updatedeptauth obj)
        {
            try
            {
                if (User.Claims.Count() > 0)
                {
                    empname = User?.Claims?.FirstOrDefault(e => e.Type == ClaimTypes.Name).Value?.ToString();
                    empno = User?.Claims?.FirstOrDefault(e => e.Type == ClaimTypes.Actor).Value?.ToString();
                }
                switch (obj.type)
                {
                    case "upload":
                        await _bllmirleCloudCore.UpdateDeptAllowUpload(obj.id_doc_dept, obj.check, empname);
                        break;
                    case "download":
                        await _bllmirleCloudCore.UpdateDeptAllowDownload(obj.id_doc_dept, obj.check, empname);
                        break;
                    case "folder":
                        await _bllmirleCloudCore.UpdateDeptAllowFolder(obj.id_doc_dept, obj.check, empname);
                        break;
                    case "delfolder":
                        await _bllmirleCloudCore.UpdateDeptAllowDelFolder(obj.id_doc_dept, obj.check, empname);
                        break;
                    case "delfile":
                        await _bllmirleCloudCore.UpdateDeptAllowDelFile(obj.id_doc_dept, obj.check, empname);
                        break;
                    case "share":
                        await _bllmirleCloudCore.UpdateDeptAllowShare(obj.id_doc_dept, obj.check, empname);
                        break;
                    default:
                        break;
                }


                return StatusCode(204);
            }
            catch (Exception ex)
            {
                return StatusCode(400, ex.Message);
            }
        }

        [HttpPost("UpdateShareMember/{iddocument}")]
        public async Task<IActionResult> UpdateShareMember(postjson json, long? iddocument)
        {
            try
            {
                if (User.Claims.Count() > 0)
                {
                    empname = User?.Claims?.FirstOrDefault(e => e.Type == ClaimTypes.Name).Value?.ToString();
                    empno = User?.Claims?.FirstOrDefault(e => e.Type == ClaimTypes.Actor).Value?.ToString();
                }

                var data = JsonConvert.DeserializeObject<List<member>>(json.content);

                await _bllmirleCloudCore.CreateDocumentAccounts(data, (long)iddocument, empname);

                return StatusCode(204);

            }
            catch (Exception ex)
            {
                return StatusCode(400, ex.Message);
            }
        }



        [HttpPost("UpdateShareDept/{iddocument}")]
        public async Task<IActionResult> UpdateShareDept(postjson json, long? iddocument)
        {
            try
            {
                if (User.Claims.Count() > 0)
                {
                    empname = User?.Claims?.FirstOrDefault(e => e.Type == ClaimTypes.Name).Value?.ToString();
                    empno = User?.Claims?.FirstOrDefault(e => e.Type == ClaimTypes.Actor).Value?.ToString();
                }

                var data = JsonConvert.DeserializeObject<List<deptno>>(json.content);

                await _bllmirleCloudCore.CreateDocumentDepts(data, (long)iddocument, empname);

                return StatusCode(204);

            }
            catch (Exception ex)
            {
                return StatusCode(400, ex.Message);
            }
        }


        [HttpGet("DeleteShareMember/{iddocaccount}")]
        public async Task<IActionResult> DeleteShareMember(long? iddocaccount)
        {
            try
            {
                if (User.Claims.Count() > 0)
                {
                    empname = User?.Claims?.FirstOrDefault(e => e.Type == ClaimTypes.Name).Value?.ToString();
                    empno = User?.Claims?.FirstOrDefault(e => e.Type == ClaimTypes.Actor).Value?.ToString();
                }

                await _bllmirleCloudCore.DelDocumentAccount((long)iddocaccount, empname);

                return StatusCode(204);
            }
            catch (Exception ex)
            {
                return StatusCode(400, ex.Message);
            }

        }


        [HttpGet("DeleteShareDept/{iddocdept}")]
        public async Task<IActionResult> DeleteShareDept(long? iddocdept)
        {
            try
            {
                if (User.Claims.Count() > 0)
                {
                    empname = User?.Claims?.FirstOrDefault(e => e.Type == ClaimTypes.Name).Value?.ToString();
                    empno = User?.Claims?.FirstOrDefault(e => e.Type == ClaimTypes.Actor).Value?.ToString();
                }

                await _bllmirleCloudCore.DelDocumentDept((long)iddocdept, empname);

                return StatusCode(204);
            }
            catch (Exception ex)
            {
                return StatusCode(400, ex.Message);
            }

        }

        [HttpGet("GetSearchDatatable")]
        public async Task<IActionResult> GetSearchDatatable()
        {
            if (User.Claims.Count() > 0)
            {
                empname = User?.Claims?.FirstOrDefault(e => e.Type == ClaimTypes.Name).Value?.ToString();
                empno = User?.Claims?.FirstOrDefault(e => e.Type == ClaimTypes.Actor).Value?.ToString();
            }

            var model = await _reportmirleCloudCore.GetFileDataTable(empname);

            ViewBag.Root = this.Root;

            return PartialView("_GetSearchDatatable",model);
        }


        [HttpPost("GetFolderSetting")]
        public async Task<IActionResult> GetFolderSetting(postfolderobj obj)
        {

            var result=await _bllmirleCloudCore.FindDocument(obj.Path);

            return PartialView("_GetFolderSetting",result);
        }

        [HttpPost("UpdateFolderSetting")]
        public async Task<IActionResult> UpdateFolderSetting(updatesetting obj)
        {
            try
            {
                if (User.Claims.Count() > 0)
                {
                    empname = User?.Claims?.FirstOrDefault(e => e.Type == ClaimTypes.Name).Value?.ToString();
                    empno = User?.Claims?.FirstOrDefault(e => e.Type == ClaimTypes.Actor).Value?.ToString();
                }
                switch (obj.type)
                {
                    case "autoday":
                        await _bllmirleCloudCore.UpdateDocumentAutoday(obj.id_doc, obj.check, empname);
                        break;
                    case "autoversion":
                        await _bllmirleCloudCore.UpdateDocumentAutoversion(obj.id_doc, obj.check, empname);
                        break;
                    default:
                        break;
                }

                return StatusCode(204);
            }
            catch (Exception ex)
            {
                return StatusCode(400, ex.Message);
            }
        }

        [HttpPost("GetPreviewResult")]
        public async Task<IActionResult> GetFileInfo(postfolderobj obj)
        {

            
            var result= await _bllmirleCloudCore.FindFile(obj.Path);

            if (String.IsNullOrEmpty(result.Preview))
            {
               await PdfFactory.GeneratePDF(_env, _bllmirleCloudCore, result.Relativepath);

               result = await _bllmirleCloudCore.FindFile(obj.Path);
            }


           var datas = JsonConvert.SerializeObject(result);

            return Json(datas);
        }

        /// <summary>
        /// for excel
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost("GetFileId")]
        public async Task<IActionResult> GetFileId(postfolderobj obj)
        {
            var result = await _bllmirleCloudCore.FindFile(obj.Path);

            var datas = JsonConvert.SerializeObject(result);

            return Json(datas);
        }

        [HttpPost("MovieFile")]
        public async Task<IActionResult> MoveFile(postmoveobj obj)
        {
            try
            {
                if (User.Claims.Count() > 0)
                {
                    empname = User?.Claims?.FirstOrDefault(e => e.Type == ClaimTypes.Name).Value?.ToString();
                    empno = User?.Claims?.FirstOrDefault(e => e.Type == ClaimTypes.Actor).Value?.ToString();
                }

                var sourcefile=await this._bllmirleCloudCore.FindFile(obj.sourcepath);

                if (sourcefile != null)
                {
                    string newpath = Path.Combine(obj.targetpath, obj.sourcename);
                    string oldpath = obj.sourcepath;
                    string comment = sourcefile.Comment;
                    string preview = sourcefile.Preview;

                    System.IO.File.Move(oldpath, newpath);

                    //新增檔案
                    await _iofile.CreateFile(newpath, empname, sourcefile.Comment);

                    //刪除舊檔
                    await _bllmirleCloudCore.DeleteFile(obj.sourcepath, empno);

                    if (!String.IsNullOrEmpty(preview)) {
                        var newfile = await _bllmirleCloudCore.FindFile(newpath);
                        newfile.Preview = preview;
                        await _bllmirleCloudCore.UpdateFile(newfile);
                    }

                }

                return StatusCode(202,obj);
            }catch(Exception ex)
            {
                return StatusCode(400, ex.Message);
            }
        }

        [HttpPost("MovieDirectory")]
        public async Task<IActionResult> MoveDirectory(postmoveobj obj)
        {
            try
            {
                if (User.Claims.Count() > 0)
                {
                    empname = User?.Claims?.FirstOrDefault(e => e.Type == ClaimTypes.Name).Value?.ToString();
                    empno = User?.Claims?.FirstOrDefault(e => e.Type == ClaimTypes.Actor).Value?.ToString();
                }
                /*
                DirectoryInfo s = new DirectoryInfo(obj.sourcepath);
                DirectoryInfo t = new DirectoryInfo(obj.targetpath);

                DirectoryInfo t1=t.CreateSubdirectory(obj.sourcename);


                await _iodirectory.CopyDirectory(obj.sourcepath, t1.FullName,empname);
                */
                await _iodirectory.MoveDirectory(obj.sourcepath, obj.targetpath,empname);

                return StatusCode(200, obj);
            }catch(Exception ex)
            {
                return StatusCode(400, ex.Message);

            }
        }


        [HttpGet("FindManagers")]
        public async Task<IActionResult> FindManagers()
        {
            var result = await this._reportmirleCloudCore.GetManagerList();
            return PartialView("_GetPartialManagers", result);
        }


        [HttpPost("AddManager")]
        public async Task<IActionResult> AddManager(postjson json)
        {
            try
            {
                if (User.Claims.Count() > 0)
                {
                    empname = User?.Claims?.FirstOrDefault(e => e.Type == ClaimTypes.Name).Value?.ToString();
                    empno = User?.Claims?.FirstOrDefault(e => e.Type == ClaimTypes.Actor).Value?.ToString();
                }

                var data = JsonConvert.DeserializeObject<List<member>>(json.content);

                await _bllmirleCloudCore.UpdateIsManagers(data, empname);

                return StatusCode(200);
            }
            catch (Exception ex)
            {
                return StatusCode(400, ex.Message);
            }

        }

        [HttpGet("DeleteManager/{id_UserAccount}")]
        public async Task<IActionResult> DeleteManager(long? id_UserAccount)
        {
            try
            {
                if (User.Claims.Count() > 0)
                {
                    empname = User?.Claims?.FirstOrDefault(e => e.Type == ClaimTypes.Name).Value?.ToString();
                    empno = User?.Claims?.FirstOrDefault(e => e.Type == ClaimTypes.Actor).Value?.ToString();
                }

                await _bllmirleCloudCore.DeleteManager((long)id_UserAccount, empname);

                return StatusCode(200);
            }
            catch (Exception ex)
            {
                return StatusCode(400, ex.Message);
            }
        }

        [HttpPost("UpdateFileInfo")]
        public async Task<IActionResult> UpdateFileInfo(postrenameobj obj)
        {
            try
            {
                if (User.Claims.Count() > 0)
                {
                    empname = User?.Claims?.FirstOrDefault(e => e.Type == ClaimTypes.Name).Value?.ToString();
                    empno = User?.Claims?.FirstOrDefault(e => e.Type == ClaimTypes.Actor).Value?.ToString();
                }

                var result = await _bllmirleCloudCore.FindFile(obj.id_file);

                if (result!=null)
                {
                    var root = _env.WebRootPath;

                    string aabpath = dalmirleCloudCore.Common.Common.RelativeToAbsolute(root, result.Relativepath);

                    FileInfo f = new FileInfo(aabpath);

                    if (!String.IsNullOrEmpty(obj.file_name))
                    {
                        //改檔名  新舊檔名
                        if (!obj.file_name.Equals(result.Filename))
                        {
                            
                            //change filename
                            string oldpath = aabpath;
                            string newpath = Path.Combine(f.Directory.FullName, obj.file_name);
                            //newpath = newpath.Substring(1, newpath.Length - 1);

                            System.IO.File.Move(oldpath, newpath);

                            result.Filename = obj.file_name;
                            result.UpdateBy = empno;
                            
                            string relativepath= dalmirleCloudCore.Common.Common.AbsoluteToRelative(root, newpath);

                            relativepath= relativepath.Substring(1, relativepath.Length - 1);
                            result.Relativepath = relativepath;

                            await _bllmirleCloudCore.UpdateFile(result);


                        }
                    }

                    if (!String.IsNullOrEmpty(obj.file_keyword))
                    {
                        //改關鍵字
                        if (!obj.file_keyword.Equals(result.Comment))
                        {
                            result.Comment = obj.file_keyword;
                            result.UpdateBy = empno;
                            await _bllmirleCloudCore.UpdateFile(result);

                        }
                    }
                }

                return StatusCode(204);
            }catch(Exception ex)
            {
                return StatusCode(400, ex.Message);
            }
        }

        [HttpPost("UpdateDirectoryInfo")]
        public async Task<IActionResult> UpdateDirectoryInfo(postrenamefolderobj obj)
        {
            try
            {
                if (User.Claims.Count() > 0)
                {
                    empname = User?.Claims?.FirstOrDefault(e => e.Type == ClaimTypes.Name).Value?.ToString();
                    empno = User?.Claims?.FirstOrDefault(e => e.Type == ClaimTypes.Actor).Value?.ToString();
                }

                var root = _env.WebRootPath;

                DirectoryInfo di = new DirectoryInfo(obj.abpath);

                string newnameapapth = "",newnamerpath="";

                if (di.Exists)
                {
                    var oldnameabpath = di.FullName;

                    var parent = di.Parent.FullName;
                    newnameapapth = Path.Combine(parent, obj.foldername);

                    string newrelativepath = dalmirleCloudCore.Common.Common.AbsoluteToRelative(root, newnameapapth);
                    newrelativepath = newrelativepath.Substring(1, newrelativepath.Length - 1);

                    string oldrelativepath = dalmirleCloudCore.Common.Common.AbsoluteToRelative(root, oldnameabpath);
                    oldrelativepath = oldrelativepath.Substring(1, oldrelativepath.Length - 1);

                    await _bllmirleCloudCore.RenameDirectory(oldrelativepath, newrelativepath, obj.foldername, empno);
                    di.MoveTo(newnameapapth);

                    return StatusCode(200, newnameapapth);
                    //di.MoveTo(newname);

                }
                else
                {
                    throw new Exception("folder not found");
                }
                //string aabpath = dalmirleCloudCore.Common.Common.RelativeToAbsolute(root, obj.abpath);


                return StatusCode(200);
            }
            catch(Exception ex)
            {
                return StatusCode(400, ex.Message);
            }
        }

        //[HttpGet("DownloadFile/{idFile}")]
        //public IActionResult DownloadFile(long idFile)
        //{

        //    string path = dalmirleCloudCore.Common.Common.RelativeToAbsolute(Root,rpath);
        //    //找檔案                     
        //    try
        //    {
        //        FileStream stream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read);
        //        return File(stream, "application/octet-stream", "oo"); //MME 格式 可上網查 此為通用設定
        //    }
        //    catch (System.Exception)
        //    {
        //        return Content("<script>alert('查無此檔案');window.close()</script>");
        //    }

        //}

    }


    public class postrenamefolderobj
    {
        public string foldername { get; set; }
        public string abpath { get; set; }
        //id_file/file_name/file_keyword
    }


    public class postrenameobj
    {
        public long id_file { get; set; }
        public string file_name { get; set; }
        public string file_keyword { get; set; }
        //id_file/file_name/file_keyword
    }

    public class postmoveobj
    {
        public string sourcename { set; get; }
        public string  sourcepath { get; set; }


        public string targetname { set; get; }
        public string targetpath { get; set; }

    }

    public class postfolderobj
    {
        public string Name { set; get; }
        public string Path { get; set; }
    }

    public class postjson
    {
        public string content { get; set; }
    }

    public class updatememberauth
    {
        public long id_doc_acc { get; set; }
        public string type { get; set; }

        public int check { set; get; }
    }


    public class updatedeptauth
    {
        public long id_doc_dept { get; set; }
        public string type { get; set; }

        public int check { set; get; }
    }

    public class updatesetting
    {
        public long id_doc { set; get; }
        public string type { set; get; }
        public int check { set; get; }
    }

}
