﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using bllmirleCloudCore;
using dalmirleCloudCore.Interface;
using dalmirleCloudCore.Meta;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using mirleCloudCore.Common;
using mirleCloudCore.Models;
using Newtonsoft.Json;

namespace mirleCloudCore.Controllers
{
    [Authorize(Roles = "Users,Assist")]
    public class HomeController :Controller
    {

        protected IWebHostEnvironment _env;
        private IOptionsSnapshot<Profile> _profile;
        private readonly IOFileInterface _iofile;
        private readonly IODirectoryInterface _iodirectory;

        public string empname { get; set; }
        public string empno { get; set; }

        private mirleCloudCorebll _bllmirleCloudCore;
        private mirleCloudCoreReport _reportmirleCloudCore;
        public string Root { get; set; }
        private int _admin;
        private int _super;


        public HomeController(IWebHostEnvironment env,
            IOptionsSnapshot<Profile> profile,
            IOFileInterface iofile,
            IODirectoryInterface iodirectory,
            mirleCloudCorebll bllmirleCloudCore,
            mirleCloudCoreReport reportmirleCloudCore
            )
        {
            _env = env;
            _profile = profile;
            _iofile = iofile;
            _iodirectory = iodirectory;
            //empname = "rogerroan";
            _bllmirleCloudCore = bllmirleCloudCore;
            _reportmirleCloudCore = reportmirleCloudCore;
            Root = _profile.Value.Root;

        }
  
        public async Task<IActionResult> Index()
        {       
            
            if (User.Claims.Count() > 0)
            {
                empname = User?.Claims?.FirstOrDefault(e => e.Type == ClaimTypes.Name).Value?.ToString();
                empno = User?.Claims?.FirstOrDefault(e => e.Type == ClaimTypes.Actor).Value?.ToString();

            }

            var account=await _bllmirleCloudCore.FindUserAccount(empname);

            if (account != null)
            {
                this._admin = (int)account.IsManager;
                this._super = (int)account.IsSuperuser;
            }

            //document size
            var filesizereport = await _reportmirleCloudCore.Getdocsize_report(empname);

            var label = filesizereport.Select(e => e.docname).ToList();
            var value = filesizereport.Select(e => e.percen).ToList();

            ViewBag.ReportLable = JsonConvert.SerializeObject(label);
            ViewBag.ReportValue = JsonConvert.SerializeObject(value);

            //以使用空間
            var spacereport=await _reportmirleCloudCore.GetSpaceReport(empname);

            var top10sharereport=await _reportmirleCloudCore.GetshareTop10Report(empname);

            var top10filereport = await _reportmirleCloudCore.GetTop10FileSizeReport(empname);

            var recentuploadreport = await _reportmirleCloudCore.GetRecentUploadReport(empname);

            Helper.CheckWebroot(_env,empname);

            ViewBag.empname = empname;
            ViewBag.space = spacereport.FirstOrDefault();
            ViewBag.top10shareme = top10sharereport; //最近別人上船的檔案
            ViewBag.top10filereport = top10filereport; //前10大的檔案
            ViewBag.recentuploadreport = recentuploadreport;//最近上傳
            ViewBag.admin = _admin;//isManager?
            ViewBag.super = _super;

            return View();
        }

       
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
