﻿using bllmirleCloudCore;
using dalmirleCloudCore.Interface;
using dalmirleCloudCore.Meta;
using dalmirleCloudCore.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using mirleCloudCore.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace mirleCloudCore.Controllers
{
    [Route("Login")]
    [Authorize(Roles = "Users")]
    public class LoginController : Controller
    {

        protected IWebHostEnvironment _env;

        private IOptionsSnapshot<Profile> _profile;
        private readonly IOFileInterface _iofile;
        private readonly IODirectoryInterface _iodirectory;

        public string empname { get; set; }
        private mirleCloudCorebll _bllmirleCloudCore;
        private mirleCloudCoreReport _reportmirleCloudCore;
        public string Root { get; set; }
        public LoginController(
            IWebHostEnvironment env,
            IOptionsSnapshot<Profile> profile,
            IOFileInterface iofile,
            IODirectoryInterface iodirectory,
            mirleCloudCorebll bllmirleCloudCore,
            mirleCloudCoreReport reportmirleCloudCore)
        {
            _env = env;
            _profile = profile;
            _iofile = iofile;
            _iodirectory = iodirectory;
            //empname = "rogerroan";
            _bllmirleCloudCore = bllmirleCloudCore;
            _reportmirleCloudCore = reportmirleCloudCore;
            Root = _profile.Value.Root;
        }


        [HttpGet("Dev")]
        [AllowAnonymous]
        public IActionResult Dev(LoginRole login)
        {
            TempData["isdev"] = 1;
            if (User?.Claims?.Count() > 0)
            {
                return RedirectToAction("index", "home");
            }

            return View("Index", login);
        }

        [HttpGet("Info")]
        [AllowAnonymous]
        public IActionResult Index(LoginRole login, int super = 0)
        {
            TempData["isdev"] = 0;
            if (User?.Claims?.Count() > 0)
            {
                return RedirectToAction("index", "home");
            }

            return View("Index", login);
        }


        [HttpPost("Login")]
        [AllowAnonymous]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> PostLogin(LoginRole login)
        {

            var Profile = _profile.Value;
            var Location = Profile.Site;

            Claim[] claims = null;

            aduserResult result = null;

            VwAppEmp member = null;

            string username = login.username.Replace(@"mirlehq\", "").Replace(@"mirle\","");

            int isdev = Convert.ToInt32(TempData["isdev"]);

            if (Location.Equals("Production"))
            {
                //要驗證
                //if (super == 0)
                //{
                //    result = await Http.AdLogin(new aduser
                //    {
                //        empname = username,
                //        password = login.password,
                //    });

                //    if (result == null)
                //    {
                //        login.hint = "帳號或密碼錯誤";
                //        return RedirectToAction("Info", login);//流程不往下執行
                //    }
                //}
                if (isdev==1 && !login.password.Equals("22099478"))
                {
                    login.hint = "帳號或密碼錯誤";
                    return RedirectToAction("Info", login);//流程不往下執行
                }

                member = await _reportmirleCloudCore.GetAppEmpsInfoByName(username);

                if (member == null)
                {
                    login.hint = "帳號不存在";
                    return RedirectToAction("Info", login);//流程不往下執行
                }


                result = new aduserResult
                {
                    empname = username,
                    empno = member.EmpNo,
                    email = "",
                };
            }
            else
            {
                member = await _reportmirleCloudCore.GetAppEmpsInfoByName(username);

                if (member == null)
                {
                    login.hint = "帳號不存在";
                    return RedirectToAction("Info", login);//流程不往下執行
                }

                result = new aduserResult
                {
                    empname = username,
                    empno = member.EmpNo,
                    email = "",
                };

            }

            //member = await _assitantbll.GetAppEmpsInfoByName(username);

            if (member == null)
            {
                login.hint = "帳號不存在";
                return RedirectToAction("Info", login);//流程不往下執行
            }

            result = new aduserResult
            {
                empname = username,
                empno = member.EmpNo,
                email = "",
            };


            if (result != null)
            {
                #region remark
                switch ((LoginType)Convert.ToInt32(login.role))
                {
                    case LoginType.Engineer:

                        claims = CommonFunction.LoadClaims(username, result.empno, LoginType.Engineer);
                        //直接人員清單

                        ClaimsIdentity claimsIdentity2 = new ClaimsIdentity(claims,
                                CookieAuthenticationDefaults.AuthenticationScheme);//Scheme必填

                        ClaimsPrincipal principal2 = new ClaimsPrincipal(claimsIdentity2);

                        //執行登入，相當於以前的FormsAuthentication.SetAuthCookie()

                        //從組態讀取登入逾時設定
                        //double loginExpireMinute2 = _config.GetValue<double>("LoginExpireMinute");

                        DateTime exiredate = DateTime.Now;

                        if (login.isremember == true)
                        {
                            exiredate = exiredate.AddDays(30);
                        }
                        else
                        {
                            exiredate = exiredate.AddDays(7);
                        }

                        await HttpContext.SignInAsync(principal2,
                            new AuthenticationProperties()
                            {

                                IsPersistent = true, //IsPersistent = false，瀏覽器關閉即刻登出
                                //用戶頁面停留太久，逾期時間，在此設定的話會覆蓋Startup.cs裡的逾期設定


                                ExpiresUtc = DateTime.Now.AddDays(7)
                            });


                        //if (Profile.Site.Equals("Production"))
                        {
                            await _reportmirleCloudCore.InitialUser(member.EmpEname);
                        }

                        //return RedirectToAction("Info", "Dashboard");
                        return RedirectToAction("Index", "Home");


                    case LoginType.Assitant:

                        var useraccount = await _bllmirleCloudCore.FindUserAccount(username);


                        if (useraccount != null)
                        {
                            if (useraccount.IsManager == 1)
                            {
                                claims = CommonFunction.LoadClaims(username, result.empno, LoginType.Assitant);
                            }
                            else
                            {
                                login.hint = "不存在管理者清單";
                                return RedirectToAction("Info", login);//流程不往下執行
                            }
                        }
                        else
                        {
                            login.hint = "不存在管理者清單";
                            return RedirectToAction("Info", login);//流程不往下執行
                        }


                        ClaimsIdentity claimsIdentity = new ClaimsIdentity(claims,
                                CookieAuthenticationDefaults.AuthenticationScheme);//Scheme必填


                        ClaimsPrincipal principal = new ClaimsPrincipal(claimsIdentity);

                        //執行登入，相當於以前的FormsAuthentication.SetAuthCookie()

                        //從組態讀取登入逾時設定
                        //double loginExpireMinute = _config.GetValue<double>("LoginExpireMinute");

                        await HttpContext.SignInAsync(principal,
                            new AuthenticationProperties()
                            {
                                //IsPersistent = true, //IsPersistent = false，瀏覽器關閉即刻登出
                                //用戶頁面停留太久，逾期時間，在此設定的話會覆蓋Startup.cs裡的逾期設定
                                ExpiresUtc = DateTime.Now.AddDays(7)
                            });

                        return RedirectToAction("Index", "Manager");


                    default:
                        login.hint = "帳號或密碼輸入錯誤";
                        return RedirectToAction("Info", login);//流程不往下執行

                }
                #endregion

            }
            else
            {
                login.hint = "帳號或密碼輸入錯誤";
                return RedirectToAction("Info", login);//流程不往下執行
            }


        }



        [AllowAnonymous]
        [HttpGet("Logout")]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync();

            return RedirectToAction("Info", "Login");//導至登入頁

        }

        [AllowAnonymous]
        [HttpGet("Denied")]
        public IActionResult Denied()
        {
            return View("Denied");
        }


    }
}
