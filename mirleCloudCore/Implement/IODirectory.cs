﻿using bllmirleCloudCore;
using dalmirleCloudCore;
using dalmirleCloudCore.Interface;
using dalmirleCloudCore.Meta;
using Microsoft.Extensions.Options;
using mirleCloudCore.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace mirleCloudCore.Implement
{

    public class IODirectory : IODirectoryInterface
    {
        private IOptionsSnapshot<Profile> _profile;
        private mirleCloudCorebll _bllmirlecloudcore;
        private IOFileInterface _file;
        public string Root { get; set; }
        public IODirectory(IOptionsSnapshot<Profile> profile,
            mirleCloudCorebll bllmirlecloudcore,
            IOFileInterface file) {

            _bllmirlecloudcore = bllmirlecloudcore;
            _profile = profile;
            var data = _profile.Value;
            Root = data.Root;
            _file = file;
        }
        public async Task<bool> CopyDirectory(string source, string dest,string who)
        {
            DirectoryInfo dir = new DirectoryInfo(source);

            FileSystemInfo[] fileinfo = dir.GetFileSystemInfos();
            string abpth = "";

            foreach (FileSystemInfo i in fileinfo)
            {
                abpth = dest + "\\" + i.Name;

                if (i is DirectoryInfo)
                {                  
                    if (!Directory.Exists(abpth))
                    {
                        Directory.CreateDirectory(abpth);

                        var doc=await _bllmirlecloudcore.CreateADocument(abpth, i.Name, who, who);

                        //AccountDocument
                        var ac = await _bllmirlecloudcore.FindAccountDocument(doc.IdDocument, who);

                        if (ac == null)
                        {
                            ac = await _bllmirlecloudcore.CreateAccountDocument(who, doc.IdDocument, who);
                        }

                    }
                    await CopyDirectory(i.FullName, abpth, who);
                }
                else
                {
                    File.Copy(i.FullName, abpth, true);

                    var doc = await _bllmirlecloudcore.FindFile(abpth);

                    if (doc != null)
                    {
                        FileInfo i2 = new FileInfo(i.FullName); ;
                        await _bllmirlecloudcore.CreateFile((long)doc.IdDocument, i.Name,abpth ,i2.Length.ToString(), who,"");
                    }
                }
            }

            return true;

        }

        public async Task<bool> CreateDiretory(string abpath,string who)
        {
            DirectoryInfo dir = new DirectoryInfo(abpath);

            if (!Directory.Exists(abpath))
            {
                Directory.CreateDirectory(abpath);

                var doc=await _bllmirlecloudcore.CreateADocument(abpath, dir.Name, who,who);

                //AccountDocument
                var ac = await _bllmirlecloudcore.FindAccountDocument(doc.IdDocument, who);

                if (ac == null)
                {
                    ac = await _bllmirlecloudcore.CreateAccountDocument(who, doc.IdDocument, who);
                }

            }
            return true;
        }

        public async Task<bool> DeleteDirectory(string path,string who)
        {
            DirectoryInfo d = new DirectoryInfo(path);
            await DeleteDirectoryRecursi(d,who);
            
            await _bllmirlecloudcore.DeleteADocument(path, who, who);
            return true;
        }

        public async  Task<bool> MoveDirectory(string source, string dest,string who)
        {
            try
            {
                DirectoryInfo destdir = new DirectoryInfo(dest);

                DirectoryInfo sourcedir = new DirectoryInfo(source);

                if (destdir?.GetDirectories()?.Where(e => e.Name == sourcedir.Name).FirstOrDefault() != null)
                {
                    throw new Exception("目錄已存在");
                }
                
                //abpath
                var newsourcepath = sourcedir.FullName;
                var newtargetpath = Path.Combine(dest, sourcedir.Name);

                
                var doc=await _bllmirlecloudcore.FindDocument(newsourcepath);

                if (doc != null)
                {
                    await _bllmirlecloudcore.RemoveDirectory(doc.IdDocument, newsourcepath, newtargetpath, who);

                    System.IO.Directory.Move(newsourcepath, newtargetpath);
                }


                //System.Diagnostics.Debug.WriteLine("test");

            }
            catch(Exception ex)
            {
                throw new Exception($"{ex.Message}");
            }
            return true;
        }


        private async Task<bool> DeleteDirectoryRecursi(DirectoryInfo baseDir,string who)
        {
            if (!baseDir.Exists)
                return true;

            foreach (var dir in baseDir.EnumerateDirectories())
            {
               await DeleteDirectoryRecursi(dir,who);
            }

            var files = baseDir.GetFiles();
            foreach (var file in files)
            {
                file.IsReadOnly = false;
                file.Delete();

                //delete file
                await _bllmirlecloudcore.DeleteFile(file.FullName, who);

            }

            baseDir.Delete(true);
            //delete folder
            await _bllmirlecloudcore.DeleteADocument(baseDir.FullName, who, who);

            return true;
        }

    }

}
