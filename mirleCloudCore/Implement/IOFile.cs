﻿using bllmirleCloudCore;
using dalmirleCloudCore.Interface;
using dalmirleCloudCore.Meta;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using dalmirleCloudCore.Common;


namespace mirleCloudCore.Implement
{
    public class IOFile : IOFileInterface
    {
        private IOptionsSnapshot<Profile> _profile;
        private mirleCloudCorebll _bllmirlecloudcore;
        public string Root { get; set; }

        public IOFile(
            IOptionsSnapshot<Profile> profile,
            mirleCloudCorebll bllmirlecloudcore
            )
        {
            _profile = profile;
            _bllmirlecloudcore = bllmirlecloudcore;
            Root = _profile.Value.Root;
        }

        public async Task<bool> Copyfile(string source, string dest,string who)
        {
            System.IO.File.Copy(source, dest, true);

            FileInfo f1 = new FileInfo(dest);
            var d1=f1.Directory;
            
            if (d1 != null)
            {
                var doc=await _bllmirlecloudcore.FindDocument(d1.FullName);

                if (d1 != null)
                {
                    throw new Exception("not document found");
                }
                else
                {
                    await _bllmirlecloudcore.CreateFile(doc.IdDocument, f1.Name, dest, f1.Length.ToString(), who,"");
                }
            }

            return true;
        }

        
        public async Task<bool> CreateFile(string abpath,string who,string keyword)
        {
            FileInfo f1 = new FileInfo(abpath);
            var d1 = f1.Directory;

            if (d1 != null)
            {
                var doc = await _bllmirlecloudcore.FindDocument(d1.FullName);

                if (doc == null)
                {
                    doc=await _bllmirlecloudcore.CreateADocument(d1.FullName, d1.Name, who, who);

                    //AccountDocument
                    var ac = await _bllmirlecloudcore.FindAccountDocument(doc.IdDocument, who);

                    if (ac == null)
                    {
                        ac = await _bllmirlecloudcore.CreateAccountDocument(who, doc.IdDocument, who);
                    }
                    //throw new Exception("not document found "+ abpath);
                }

                
                await _bllmirlecloudcore.CreateFile(doc.IdDocument, f1.Name, abpath, f1.Length.ToString(), who,keyword);
                
            }
            return true;
        }


        public async Task<bool> Movefile(string source, string dest,string who)
        {
            System.IO.File.Move(source, dest, true);

            //remove old 


            //add new

            return true;
        }


        public async Task<bool> DeleteFile(string abpath,string who)
        {
            if (System.IO.File.Exists(abpath))
            {
                System.IO.FileInfo fi = new System.IO.FileInfo(abpath);

                System.IO.File.SetAttributes(abpath, FileAttributes.Normal);

                System.IO.File.Delete(abpath);

                await _bllmirlecloudcore.DeleteFile(abpath, who);

                var file=await _bllmirlecloudcore.FindFile(abpath);

                if (file != null)
                {
                    if (!String.IsNullOrEmpty(file.Preview))
                    {


                    }
                }
                

            }
            return true;
        }

    }

}
