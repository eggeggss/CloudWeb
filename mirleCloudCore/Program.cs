using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace mirleCloudCore
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.ConfigureKestrel((context, options) =>
                    {
                        // Handle requests up to 100 MB
                        //options.Limits.MaxRequestBodySize = 62428800;
                        options.Limits.MaxRequestBodySize = 100428800;
                        options.Limits.KeepAliveTimeout = TimeSpan.FromMinutes(20);
                        options.Limits.RequestHeadersTimeout = TimeSpan.FromMinutes(20);
                        
                    })
                    .UseStartup<Startup>();
                    //webBuilder.UseStartup<Startup>();
                });
    }
}
