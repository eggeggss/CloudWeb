﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using dalmirleCloudCore.Interface;
using dalmirleCloudCore.Meta;
using dalmirleCloudCore.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using mirleCloudCore.Implement;

using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.DataProtection;
using System.IO;
using dalmirleCloudCore.Common;
using dalmirleCloudCore;
using bllmirleCloudCore;
using mirleCloudCore.Common;
using Hangfire;

namespace mirleCloudCore
{
    public class Startup
    {

        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
            Env = env;
        }

        public IConfiguration Configuration { get; }

        public IWebHostEnvironment Env { get; }


        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<clouddbContext>(options =>
           options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));



            double LoginExpireMinute = this.Configuration.GetValue<double>("LoginExpireMinute");

            //double LoginExpireMinute = 30;//this.Configuration.GetValue<double>("LoginExpireMinute");
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme).AddCookie(option =>
            {
                option.LoginPath = new PathString("/Login/Info");//�n�J��
                //option.LoginPath = new PathString("https://eip.mirle.com.tw:88/Login/Info");
                option.LogoutPath = new PathString("/Login/Logout");//�n�XAction
                option.AccessDeniedPath = new PathString("/Login/Denied");
                option.ExpireTimeSpan = TimeSpan.FromMinutes(LoginExpireMinute);//�S���w�]14��
            });


            var path = Path.Combine(Env.WebRootPath, "keys");


            services.AddDataProtection()
            .PersistKeysToFileSystem(new DirectoryInfo(path))
            .SetApplicationName("SharedCookieApp");

            services.ConfigureApplicationCookie(options =>
            {
                options.Cookie.Name = ".AspNet.SharedCookie";
            });

            services.AddScoped<EFAdapter>();

            services.AddScoped<mirleCloudCorequery>();

            services.AddScoped<mirleCloudCoredal>();

            services.AddScoped<mirleCloudCorebll>();
            services.AddScoped<mirleCloudCoreReport>();

            services.AddScoped<IOFileInterface, IOFile>();

            services.AddScoped<IODirectoryInterface, IODirectory>();

            services.AddScoped<PdfFactory>();

            services.Configure<Profile>(Configuration.GetSection("Profile"));

            services.AddHangfire(configuration => configuration
               .UseSimpleAssemblyNameTypeSerializer()
               .UseRecommendedSerializerSettings()
               .UseSqlServerStorage(Setting.HangfireSql));

            services.AddHangfireServer();

            services.AddControllersWithViews();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                //app.UseHsts();
            }

            app.UseHangfireDashboard();
            //app.UseHttpsRedirection();

            app.UseStaticFiles();

            app.UseRouting();

            //app.UseAuthorization();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
