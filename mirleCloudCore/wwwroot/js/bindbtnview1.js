﻿function binbtnview() {

    $('.btn-view').on('click', function () {

        var extent = $(this).data("extension");

        var path = $(this).data("relativepath");

        if (extent == ".pdf") {

            //$('.pdfview-body').removeClass("watermarked").removeClass("processing");

            var options = {
                pdfOpenParams: {
                    view: 'FitH',
                    //pagemode: 'thumbs',
                    search: 'lorem ipsum',
                    scrollbars: '0', toolbar: '0', statusbar: '0',
                }
            };

            PDFObject.embed(path, "#pdfcontain", options);

            $('#viewpdfModal').modal({
                backdrop: 'static',
                keyboard: false
            })
            //str.toLowerCase
        } else if (extent.toLowerCase() == ".png" ||
            extent.toLowerCase() == ".jpeg" ||
            extent.toLowerCase() == ".bmp" ||
            extent.toLowerCase() == ".jpg") {

            $('#piccontain').attr('src', path);

            $('#picviewpdfModal').modal({
                backdrop: 'static',
                keyboard: false
            })
            //other file
        } else if ((extent.toLowerCase() == ".ppt" || extent.toLowerCase() == ".pptx") ||
            (extent.toLowerCase() == ".doc" || extent.toLowerCase() == ".docx")) {

            var abpath = $(this).data("path");

            var postobj = {
                path: abpath,
            };

            $('.pdfview-body').removeClass("watermarked").addClass("processing");

            $('#viewpdfModal').modal({
                backdrop: 'static',
                keyboard: false
            })

            var url = "../data/GetPreviewResult";

            $.ajaxSetup({
                type: 'POST',
                timeout: 1200000,
            });

            $.post(url, postobj, function (e) {

                var data = jQuery.parseJSON(e);

                var pdfurl = "../share/temp/" + data.Preview;

                if (data.Preview == "") {
                    var msg = "檔案製作中請稍後再試";

                    $.aceToaster.add({
                        placement: 'tr',
                        title: msg,
                        body: '',
                        delay: 10000,
                        icon: '<i class="text-blue ml-2 mr-1 text-130"><i class="far fa-lightbulb text-200"></i></i>',

                        className: 'bgc-white-tp1 brc-secondary-tp4 rounded-sm',
                        headerClass: 'bg-transparent border-0 text-120 text-dark-m3 font-bolder',
                        bodyClass: 'pt-0'
                    });


                } else {

                    var options = {
                        pdfOpenParams: {
                            view: 'FitH',
                            //pagemode: 'thumbs',
                            search: 'lorem ipsum',
                            scrollbars: '0', toolbar: '0', statusbar: '0',
                        }
                    };

                    PDFObject.embed(pdfurl, "#pdfcontain", options);

                    //$('#viewpdfModal').modal({
                    //    backdrop: 'static',
                    //    keyboard: false
                    //})

                    setTimeout(function () {
                        $('.pdfview-body').removeClass("processing").addClass("watermarked");

                    }, 3000);

                };


            }).fail(function (e) {

                console.log(e);
                setTimeout(function () {
                    $('.pdfview-body').removeClass("processing").addClass("watermarked");

                }, 3000);

            });

        } else if (extent.toLowerCase(".xlsx") || (extent.toLowerCase(".xls"))) {

            var url = "../data/GetFileId";

            var abpath = $(this).data("path");

            var postobj = {
                path: abpath,
            };

            $('.excelbody').removeClass("watermarked").addClass("processing");

            $('#excelModal').modal({
                backdrop: 'static',
                keyboard: false
            })

            $.post(url, postobj, function (e) {

                var data = jQuery.parseJSON(e);

                var idfile = data.IdFile;

                var src = "https://eip.mirle.com.tw:89/patentweb01/emeeting/index/" + idfile;

                $('#excelframe').attr('src', src);

                $('#excelframe').on('load', function () {
                    $('.excelbody').removeClass("processing").addClass("watermarked");
                });

                //setTimeout(function () {
                //    $('.excelbody').removeClass("processing").addClass("watermarked");
                //}, 5000);

            }).fail(function () {

                $('.excelbody').removeClass("processing").addClass("watermarked");

            });

        };

    });



}