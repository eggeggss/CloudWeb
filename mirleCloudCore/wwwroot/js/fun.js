﻿

function failMessage(message, wait) {

    $.aceToaster.add({
        placement: 'tr',
        title: 'Fail!',
        body: message,
        delay: wait,
        icon: '<i class="text-danger ml-2 mr-1 text-130"><i class="fas fa-check-circle text-200"></i></i>',

        className: 'bgc-danger-l2 brc-secondary-tp4 rounded-sm',
        headerClass: 'bg-transparent border-0 text-120 text-dark-m3 font-bolder',
        bodyClass: 'pt-0'
    });

}


function okMessage(message,wait) {

    $.aceToaster.add({
        placement: 'tr',
        title: message,
        body: '',
        delay: wait,
        icon: '<i class="text-success ml-2 mr-1 text-130"><i class="fas fa-check-circle text-200"></i></i>',

        className: 'bgc-green-l2 brc-secondary-tp4 rounded-sm',
        headerClass: 'bg-transparent border-0 text-120 text-dark-m3 font-bolder',
        bodyClass: 'pt-0'
    });

}

function loadingPrepare() {
    $("#el").busyLoad("show", {
        maxSize: "350px",
        minSize: "350px",
        fontawesome: "fa fa-spinner fa-spin fa-3x fa-fw",
        text: "Loading...please wait..",
    });

    $('#el').removeClass('none-active');
    $('#el').addClass('load-active');
}

function loadingComplete() {
    $('#el').busyLoad("hide");
    $('#el').removeClass('load-active');
    $('#el').addClass('none-active');
}

function initial() {

    loadingPrepare();

    setTimeout(function () {

        loadingComplete();
    }, 500)
}


function showHintModalMessage(message) {

    $('.hintModalMessage').removeClass("msg-nonactive");
    $('.hintModalMessage').find('.msg-content').text(message);
    setTimeout(function () {

        loadingComplete();

        $('.hintModalMessage').addClass("msg-nonactive");

    }, 5000);
}

function showHintMessage(message) {

    $('.hintMessage').removeClass("msg-nonactive");
    $('.hintMessage').find('.msg-content').text(message);
    setTimeout(function () {

        loadingComplete();

        $('.hintMessage').addClass("msg-nonactive");

    }, 5000);
}

function showSuccessMessage(message) {

    $('.successMessage').removeClass("msg-nonactive");
    $('.successMessage').find('.msg-content').text(message);
    setTimeout(function () {

        loadingComplete();

        $('.successMessage').addClass("msg-nonactive");

        //location.reload();
    }, 5000);
}

function showFailMessage(message) {
    $('.dangerMessage').removeClass("msg-nonactive");
    $('.dangerMessage').find('.msg-content').text(message);
    setTimeout(function () {
        loadingComplete();

        $('.dangerMessage').addClass("msg-nonactive");
        //location.reload();
    }, 2000);

}
