This file describes what folder contains the assemblies you need to use with a particular version of the .NET Framework.


Folder						Description
---------------------------------------------------------------------------------------------------------------------------


net2.0						Contains assemblies to use with .NET Framework 2.0.



net3.5_ClientProfile				Contains assemblies to use with .NET Framework 3.5 Client Profile.



net3.5						Contains assemblies to use with .NET Framework 3.5.



net4.0_ClientProfile				Contains assemblies to use with .NET Framework 4.0 Client Profile.



net4.0						Contains assemblies to use with .NET Framework 4.0.



netstandard2.0						Contains assemblies to use with .NET Standard 2.0.



Xamarin.Android						Contains assemblies to use with Xamarin.Android.